<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    // Register the backend module Web->Forms
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Pallino.T3registration',
        'web',
        't3registrationbuilder',
        '',
        [
            'RegistrationFormBuilder' => 'list, new, edit, delete'
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:t3registration/Resources/Public/Icons/module-t3registration.svg',
            'labels' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_module.xlf',
            'navigationComponentId' => '',
            'inheritNavigationComponentFromMainModule' => false
        ]
    );
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    't3registration',
    'Form',
	'Registration'
);

//Register icon form wizard
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Imaging\IconRegistry::class
);
$iconRegistry->registerIcon(
    'RegistrationFormWizard', // Icon-Identifier, z.B. tx-myext-action-preview
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:t3registration/Resources/Public/Images/RegistrationPluginWizard.svg']
);

// Add "pierror" plugin to new element wizard
$TBE_MODULES_EXT['xMOD_db_new_content_el']['addElClasses']['Pallino\\T3registration\\Backend\\WizardElement'] = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('t3registration') . 'Classes/Backend/WizardElement.php';


$pluginSignature = 't3registration_form';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
    'FILE:EXT:t3registration/Configuration/FlexForms/flexform_form.xml');

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('t3registration/Configuration/TypoScript', 'T3Registration');

$registrationExtensionTable = array(

    'tx_t3registration_user_authentication_token' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_user_authentication_token',
		'excludeFromRegistrationForm' => true,
		'config' => array(
			'type' => 'input',
			'size' => 30,
            'eval' => 'trim'
		),
	),
    'tx_t3registration_admin_moderation_token' => array(
        'exclude' => 1,
        'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_admin_moderation_token',
        'excludeFromRegistrationForm' => true,
        'config' => array(
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        ),
    ),
    'tx_t3registration_privacy' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_privacy',
        'config' => array(
            'type' => 'check'
        ),
    ),
    'tx_t3registration_email_change_process_token' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_email_change_process_token',
        'excludeFromRegistrationForm' => true,
        'config' => array(
            'type' => 'check'
        ),
    ),
    'tx_t3registration_temporary_email_changed' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_temporary_email_changed',
        'excludeFromRegistrationForm' => true,
        'config' => array(
            'type' => 'check'
        ),
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $registrationExtensionTable);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('fe_users', '2', 'tx_t3registration_privacy',
    'after:email');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users',
    '--div--;LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.div.separatorRegistration,tx_t3registration_user_authentication_token,tx_t3registration_admin_moderation_token,tx_t3registration_temporary_email_changed,tx_t3registration_email_change_process_token');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_t3registration_domain_model_registrationform');
