<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 18/11/2018$
 */
namespace Pallino\T3registration\View;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Fluid\View\StandaloneView;

class BasicView extends StandaloneView
{

    /** @var ConfigurationManagerInterface */
    protected $configurationManager;

    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager): void
    {
        $this->configurationManager = $configurationManager;
    }

    public function setContentTemplate(
        $templateName,
        $extensionaName = 't3registration',
        $pluginName = 'Form'
    ): void {
        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            $extensionaName,
            $pluginName
        );
        $this->setRootPaths($extbaseFrameworkConfiguration['view']);
        preg_match_all('/(?P<controller>.*)\/(?P<action>.*)\.(?P<format>.*)/', $templateName, $regexpResults);
        if (!isset($regexpResults['controller'][0]) || !isset($regexpResults['action'][0]) || !isset($regexpResults['format'][0])) {
            throw new Exception(sprintf('impossible find view template: %s', $templateName));
        }
        $configurationTemplateFormat = [
            'controller' => $regexpResults['controller'][0],
            'action' => $regexpResults['action'][0],
            'format' => $regexpResults['format'][0]
        ];
        $template = $this->getTemplateFilePath($configurationTemplateFormat);
        $this->setTemplatePathAndFilename($template);
    }

    protected function setRootPaths($configurationView): void
    {
        $this->setLayoutRootPaths($configurationView['layoutRootPaths']);
        $this->setPartialRootPaths($configurationView['partialRootPaths']);
        $this->setTemplateRootPaths($configurationView['templateRootPaths']);
    }

    protected function getTemplateFilePath($templateConfiguration): string
    {
        return $this->getTemplatePaths()->resolveTemplateFileForControllerAndActionAndFormat(
            $templateConfiguration['controller'],
            $templateConfiguration['action'],
            $templateConfiguration['format']
        );
    }
}
