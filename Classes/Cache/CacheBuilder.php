<?php declare(strict_types=1);
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2017 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

namespace Pallino\T3registration\Cache;

use Pallino\T3registration\Utility\ClassLoader;
use Pallino\T3registration\Utility\ClassParser;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class CacheBuilder
 * This class builds the general Frontend User class extending all extension files
 * To extend the basic user class each extension can create a user class defining the path to extended class:
 * $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['classes']['Domain/Model/FrontendUser'][extensionkey] = 'path_to_PHP_extended_class';
 *
 * @author  Federico Bernardin <federico@bernardin.it>
 *
 */
class CacheBuilder implements SingletonInterface
{
    /**
     * @var \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend
     */
    protected $cacheInstance;

    /**
     * @var array
     */
    protected $constructorLines = [];

    public function __construct()
    {
        $classLoader = GeneralUtility::makeInstance(ClassLoader::class);
        $this->cacheInstance = $classLoader->initializeCache();
    }

    /**
     * Class cycles from extensions to find all extending PHP class from array $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['classes']['Domain/Model/FrontendUser']
     * @throws \Exception
     */
    public function reBuild(): void
    {
        $this->checkIfTokenGetAllExist();
        $path = $this->getModelFilepathByExtensionKey('t3registration');
        $code = $this->parseSingleFile($path, true);
        if ($this->existAtLeastOneClassesExtendingFrontendUser()) {
            $code .= $this->getCodeFromExtension();
        }
        if (count($this->constructorLines)) {
            $code .= LF . '    public function __construct()' . LF . '    {' . LF . implode(
                LF,
                $this->constructorLines
            ) . LF . '    }' . LF;
        }
        $code = $this->closeClassDefinition($code);
        $this->saveCodeIntoFileCache($code);
    }

    protected function checkIfTokenGetAllExist(): void
    {
        if (!function_exists('token_get_all')) {
            throw new \Exception(('The function token_get_all must exist. Please install the module PHP Module Tokenizer'));
        }
    }

    protected function getModelFilepathByExtensionKey(string $extensionKey): string
    {
        $filePath = ExtensionManagementUtility::extPath($extensionKey) . 'Classes/Domain/Model/FrontendUser.php';
        if (!is_file($filePath)) {
            throw new \Exception('Given file "' . $filePath . '" does not exist');
        }
        return $filePath;
    }

    /**
     * Parse a single file and does some magic
     * - Remove the <?php tags
     * - Remove the class definition (if set)
     *
     * @param string $filePath path of the file
     * @param bool $baseClass If class definition should be removed
     * @return string path of the saved file
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    protected function parseSingleFile($filePath, $baseClass = false)
    {
        $code = $this->getCodeFromFilepath($filePath);
        if ($baseClass) {
            return $this->getPhpClassContentWithoutClosureTag($code);
        } else {
            return $this->getInnerCodeFromClass($code, $filePath);
        }
    }

    protected function getCodeFromFilepath(string $filePath): string
    {
        if (!is_file($filePath)) {
            throw new \InvalidArgumentException(sprintf('File "%s" could not be found', $filePath));
        }
        return GeneralUtility::getUrl($filePath);
    }

    protected function getPhpClassContentWithoutClosureTag(string $code): string
    {
        $closingBracket = strrpos($code, '}');
        $content = substr($code, 0, $closingBracket);
        $content = str_replace('<?php', '', $content);
        return $content;
    }

    protected function getInnerCodeFromClass(string $code, string $filePath): string
    {
        $classParser = $this->getClassParser();
        $classParser->parse($code);
        $classParserInformation = $classParser->getFirstClass();
        $codeInLines = explode(LF, str_replace(CR, '', $code));

        if (isset($classParserInformation['eol'])) {
            $innerPart = array_slice(
                $codeInLines,
                $classParserInformation['start'],
                ($classParserInformation['eol'] - $classParserInformation['start'] - 1)
            );
        } else {
            $innerPart = array_slice($codeInLines, $classParserInformation['start']);
        }

        if (trim($innerPart[0]) === '{') {
            unset($innerPart[0]);
        }
        $codePart = implode(LF, $innerPart);
        $closingBracket = strrpos($codePart, '}');
        return $this->getPartialInfo($filePath) . substr($codePart, 0, $closingBracket);
    }

    protected function getClassParser(): ClassParser
    {
        return GeneralUtility::makeInstance(ClassParser::class);
    }

    /**
     * @param string $filePath
     * @return string
     */
    protected function getPartialInfo($filePath): string
    {
        return LF . '/*' . str_repeat('*', 70) . LF . TAB .
            'this is partial from: ' . LF . TAB . str_replace(PATH_site, '', $filePath) . LF . str_repeat(
                '*',
                70
            ) . '*/' . LF;
    }

    protected function existAtLeastOneClassesExtendingFrontendUser(): bool
    {
        $extendingClassArray = $this->getExtendingClassArray();
        if (count($extendingClassArray)) {
            return true;
        } else {
            return false;
        }
    }

    protected function getExtendingClassArray(): array
    {
        if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['classes']['Domain/Model/FrontendUser'])) {
            return $GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['classes']['Domain/Model/FrontendUser'];
        } else {
            return [];
        }
    }

    protected function getCodeFromExtension(): string
    {
        $code = '';
        foreach ($this->getExtendingClassArray() as $extensionKey) {
            $path = $this->getModelFilepathByExtensionKey($extensionKey);
            $code .= $this->parseSingleFile($path, false);
        }
        return $code;
    }

    /**
     * @param string $code
     * @return string
     */
    protected function closeClassDefinition($code)
    {
        return $code . LF . '}';
    }

    protected function saveCodeIntoFileCache(string $code): void
    {
        $cacheEntryIdentifier = 'tx_t3registration_domain_model_frontenduser';
        try {
            $this->cacheInstance->set($cacheEntryIdentifier, $code);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
