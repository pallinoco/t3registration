<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 19/11/2018$
 */
namespace Pallino\T3registration\Mail;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\View\BasicView;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

abstract class AbstractMailProcess implements MailProcessInterface
{
    /** @var FrontendUser */
    protected $frontendUser;

    /** @var RegistrationForm */
    protected $registrationForm;

    /** @var MailMessage */
    protected $mailMessage;

    /** @var ObjectManager */
    protected $objectManager;

    /** @var array */
    protected $configuration;

    /** @var array */
    protected $receiver;

    /** @var string */
    protected $emailTemplate;

    /** @var array */
    protected $settings;

    /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher */
    protected $signalSlotDispatcher;

    abstract public function process();

    public function injectObjectManager(ObjectManager $objectManager): void
    {
        $this->objectManager = $objectManager;
    }

    public function injectSignalSlotDispatcher(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher)
    {
        $this->signalSlotDispatcher = $signalSlotDispatcher;
    }

    public function __construct(FrontendUser $frontendUser, RegistrationForm $registrationForm, array $settings = [])
    {
        $this->frontendUser = $frontendUser;
        $this->registrationForm = $registrationForm;
        $this->settings = $settings;
        $this->mailMessage = GeneralUtility::makeInstance(MailMessage::class);
    }

    protected function prepareEmail(string $subjectTranslationKey): MailMessage
    {
        $this->setSender()
            ->setReceiver();
        $this->mailMessage->setBody($this->getEmailBody());
        $this->mailMessage->setSubject($this->getTranslationKey($subjectTranslationKey));

        return $this->mailMessage;
    }

    protected function sendEmail()
    {
        return $this->mailMessage->send();
    }

    protected function getTranslationKey(string $translationKey): ?string
    {
        return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($translationKey);
    }

    protected function setSender(): self
    {
        $this->mailMessage->addFrom(
            $this->registrationForm->getGeneralSenderEmailAddress(),
            $this->registrationForm->getGeneralSenderEmailName()
        );
        return $this;
    }

    protected function setReceiver(): self
    {
        if (!isset($this->receiver['name']) || !isset($this->receiver['address'])) {
            throw new Exception('Email receiver information is malformed.');
        }
        $this->mailMessage->addTo($this->receiver['address'], $this->receiver['name']);
        return $this;
    }

    protected function getEmailBody(): string
    {
        $emailView = $this->objectManager->get(BasicView::class);
        $emailView->setContentTemplate($this->emailTemplate);
        $variables = [
            'user' => $this->frontendUser,
            'form' => $this->registrationForm,
            'settings' => $this->settings
        ];
        $emailView->assignMultiple($variables);
        return $emailView->render();
    }

    protected function emitMethodSignal(string $event, $preparedArguments): void
    {
        $this->signalSlotDispatcher->dispatch(__CLASS__, $event, [$preparedArguments]);
    }
}
