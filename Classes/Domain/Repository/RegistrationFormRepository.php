<?php declare(strict_types=1);
/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 31/10/2018$
 */
namespace Pallino\T3registration\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;

class RegistrationFormRepository extends Repository
{
    public function initializeObject()
    {
        /** @var $defaultQuerySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $defaultQuerySettings->setRespectStoragePage(false);
        $this->defaultQuerySettings = $defaultQuerySettings;
    }

    public function getFormByUid(int $uid)
    {
        return parent::findByUid($uid);
    }
}
