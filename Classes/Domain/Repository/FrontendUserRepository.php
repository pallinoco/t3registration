<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Domain\Repository;

use Pallino\T3registration\Domain\Model\FrontendUser;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Federico Bernardin <federico.bernardin@immaginario.com>, BFConsulting
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FrontendUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\Session $persistenceSession
     */
    protected $persistenceSession;

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\Session $persistenceSession
     */
    public function injectPersistenceSession(\TYPO3\CMS\Extbase\Persistence\Generic\Session $persistenceSession)
    {
        $this->persistenceSession = $persistenceSession;
    }

    public function countUniqueByField($field, $value, $pid = 0)
    {
        $query = $this->createQuery();
        if ($pid) {
            $query->getQuerySettings()->setStoragePageIds([$pid]);
        } else {
            $query->getQuerySettings()->setRespectStoragePage(false);
        }
        return $query->matching($query->equals($field, $value))->count();
    }

    public function findUserFromToken($token)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        return $query->matching($query->equals('userAuthenticationToken', $token))->execute()->getFirst();
    }

    public function findUserFromAdminModerationToken($token)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        return $query->matching($query->equals('adminModerationToken', $token))->execute()->getFirst();
    }

    public function findUserFromTokenEmail($token)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        return $query->matching($query->equals('emailChangeProcessToken', $token))->execute()->getFirst();
    }

    public function isUsernameUnique(string $username): bool
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true)->getRespectStoragePage(false);
        return $query->matching(
            $query->logicalOr([
                    $query->equals('username', $username),
                    $query->equals('temporaryEmailChanged', $username)
                ])
        )->count() == 0;
    }

    public function isUsernameUniqueInPid(string $username, string $pid): bool
    {
        if (is_numeric($pid)) {
            $query = $this->createQuery();
            $query->getQuerySettings()->setIgnoreEnableFields(true)->getRespectStoragePage(false);
            return $query->matching($query->logicalOr([
                    $query->logicalAnd([
                        $query->equals('pid', $pid),
                        $query->equals('username', $username)
                    ]),
                    $query->logicalAnd([
                        $query->equals('pid', $pid),
                        $query->equals('temporaryEmailChanged', $username)
                    ])
                ]))->count() == 0;
        } else {
            return false;
        }
    }

    public function findFreshUserByUid($identifier)
    {
        $temporaryObject = null;
        if ($this->persistenceSession->hasIdentifier($identifier, $this->objectType)) {
            $temporaryObject = $this->persistenceSession->getObjectByIdentifier($identifier, $this->objectType);
            $this->persistenceSession->unregisterObject($temporaryObject);
        }
        $originalUser = $this->getOriginalUserIgnoringDisable($identifier);
        if ($temporaryObject != null) {
            $this->persistenceSession->registerObject($temporaryObject, $temporaryObject->getUid());
        }
        return $originalUser;
    }

    public function getOriginalUserIgnoringDisable($uid): ?FrontendUser
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setEnableFieldsToBeIgnored(['disabled']);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        return $query->matching($query->equals('uid', $uid))->execute()->getFirst();
    }
}
