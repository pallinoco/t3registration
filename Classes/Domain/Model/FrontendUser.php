<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 23/10/2018$
 */
namespace Pallino\T3registration\Domain\Model;

class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * @var bool
     */
    protected $registrationPrivacy;
    /**
     * @var string
     */
    protected $userAuthenticationToken;
    /**
     * @var string
     */
    protected $adminModerationToken;

    /**
     * @var string
     */
    protected $emailChangeProcessToken;

    /**
     * @var string
     */
    protected $temporaryEmailChanged;

    /**
     * @var bool
     */
    protected $disable = false;

    /**
     * @var bool
     */
    protected $delete = false;

    /**
     * @var array
     */
    protected $excludeProperties = [];

    /**
     * @return string
     */
    public function getAdminModerationToken(): string
    {
        return $this->adminModerationToken;
    }

    /**
     * @param string $adminModerationToken
     */
    public function setAdminModerationToken(string $adminModerationToken): void
    {
        $this->adminModerationToken = $adminModerationToken;
    }

    /**
     * @return bool
     */
    public function isRegistrationPrivacy(): bool
    {
        return $this->registrationPrivacy;
    }

    /**
     * @param bool $registrationPrivacy
     */
    public function setRegistrationPrivacy(bool $registrationPrivacy): void
    {
        $this->registrationPrivacy = $registrationPrivacy;
    }

    /**
     * @return string
     */
    public function getUserAuthenticationToken(): string
    {
        return $this->userAuthenticationToken;
    }

    /**
     * @param string $userAuthenticationToken
     */
    public function setUserAuthenticationToken(string $userAuthenticationToken): void
    {
        $this->userAuthenticationToken = $userAuthenticationToken;
    }

    /**
     * @return string
     */
    public function getEmailChangeProcessToken(): string
    {
        return $this->emailChangeProcessToken;
    }

    /**
     * @param string $emailChangeProcessToken
     */
    public function setEmailChangeProcessToken(string $emailChangeProcessToken): void
    {
        $this->emailChangeProcessToken = $emailChangeProcessToken;
    }

    /**
     * @return string
     */
    public function getTemporaryEmailChanged(): string
    {
        return $this->temporaryEmailChanged;
    }

    /**
     * @param string $temporaryEmailChanged
     */
    public function setTemporaryEmailChanged(string $temporaryEmailChanged): void
    {
        $this->temporaryEmailChanged = $temporaryEmailChanged;
    }

    /**
     * @return bool
     */
    public function isDisable(): bool
    {
        return $this->disable;
    }

    /**
     * @param bool $disable
     */
    public function setDisable(bool $disable): void
    {
        $this->disable = $disable;
    }

    /**
     * @param bool $delete
     */
    public function setDelete(bool $delete): void
    {
        $this->delete = $delete;
    }

    public function isDeleted()
    {
        return $this->delete;
    }

    public function setExcludeProperties($properties)
    {
        $this->excludeProperties = $properties;
    }

    public function addExcludeProperty($property)
    {
        if (!in_array($property, $this->excludeProperties)) {
            $this->excludeProperties[] = $property;
        }
    }

    public function _getProperties()
    {
        $properties = parent::_getProperties();
        foreach ($properties as $propertyKey => $property) {
            if (in_array($propertyKey, $this->excludeProperties)) {
                unset($properties[$propertyKey]);
            }
        }
        return $properties;
    }
}
