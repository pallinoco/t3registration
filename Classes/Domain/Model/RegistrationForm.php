<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Domain\Model;

use JsonSerializable;

class RegistrationForm extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity implements JsonSerializable
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $fields;

    /**
     * @var bool
     */
    protected $doubleOptin = false;

    /**
     * @var bool
     */
    protected $adminAuth = false;

    /**
     * @var string
     */
    protected $adminEmails;

    /**
     * @var string
     */
    protected $afterAdminConfirmGroups = '';

    /**
     * @var bool
     */
    protected $userEnableWithoutAdminAuth = false;

    /**
     * @var bool
     */
    protected $adminAuthEnableSignals = false;

    /**
     * @var bool
     */
    protected $enableEmailModerationResultToUser = false;

    /**
     * @var bool
     */
    protected $userEnableWithoutEmailVerification = false;

    /**
     * @var string
     */
    protected $beforeUserConfirmGroups = '';

    /**
     * @var string
     */
    protected $afterUserConfirmationGroups = '';

    /**
     * @var string
     */
    protected $generalSenderEmailAddress;

    /**
     * @var string
     */
    protected $generalSenderEmailName;

    /**
     * @var bool
     */
    protected $useEmailAsUsername = false;

    /**
     * @var bool
     */
    protected $enableChangeUsername = false;

    /**
     * @var int
     */
    protected $changeUsernameAction = 0;

    /**
     * @var string
     */
    protected $deciders;

    /**
     * @var bool
     */
    protected $enableApi = false;

    /**
     * @var bool
     */
    protected $enableFeReport = false;

    /**
     * @var bool
     */
    protected $enableTestEnvironment = false;

    /**
     * @var string
     */
    protected $testingEmail;

    /**
     * @var bool
     */
    protected $enableModelAnnotationValidation = false;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFields(): string
    {
        return $this->fields;
    }

    /**
     * @param string $fields
     */
    public function setFields(string $fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return bool
     */
    public function isDoubleOptin(): bool
    {
        return $this->doubleOptin;
    }

    /**
     * @param bool $doubleOptin
     */
    public function setDoubleOptin(bool $doubleOptin): void
    {
        $this->doubleOptin = $doubleOptin;
    }

    /**
     * @return bool
     */
    public function isAdminAuth(): bool
    {
        return $this->adminAuth;
    }

    /**
     * @param bool $adminAuth
     */
    public function setAdminAuth(bool $adminAuth): void
    {
        $this->adminAuth = $adminAuth;
    }

    /**
     * @return string
     */
    public function getAdminEmails(): string
    {
        return $this->adminEmails;
    }

    /**
     * @param string $adminEmails
     */
    public function setAdminEmails(string $adminEmails): void
    {
        $this->adminEmails = $adminEmails;
    }

    /**
     * @return string
     */
    public function getAfterAdminConfirmGroups(): string
    {
        return $this->afterAdminConfirmGroups;
    }

    /**
     * @param string $afterAdminConfirmGroups
     */
    public function setAfterAdminConfirmGroups(string $afterAdminConfirmGroups): void
    {
        $this->afterAdminConfirmGroups = $afterAdminConfirmGroups;
    }

    /**
     * @return bool
     */
    public function isUserEnableWithoutAdminAuth(): bool
    {
        return $this->userEnableWithoutAdminAuth;
    }

    /**
     * @param bool $userEnableWithoutAdminAuth
     */
    public function setUserEnableWithoutAdminAuth(bool $userEnableWithoutAdminAuth): void
    {
        $this->userEnableWithoutAdminAuth = $userEnableWithoutAdminAuth;
    }

    /**
     * @return bool
     */
    public function isAdminAuthEnableSignals(): bool
    {
        return $this->adminAuthEnableSignals;
    }

    /**
     * @param bool $adminAuthEnableSignals
     */
    public function setAdminAuthEnableSignals(bool $adminAuthEnableSignals): void
    {
        $this->adminAuthEnableSignals = $adminAuthEnableSignals;
    }

    /**
     * @return bool
     */
    public function isEnableEmailModerationResultToUser(): bool
    {
        return $this->enableEmailModerationResultToUser;
    }

    /**
     * @param bool $enableEmailModerationResultToUser
     */
    public function setEnableEmailModerationResultToUser(bool $enableEmailModerationResultToUser): void
    {
        $this->enableEmailModerationResultToUser = $enableEmailModerationResultToUser;
    }

    /**
     * @return bool
     */
    public function isUserEnableWithoutEmailVerification(): bool
    {
        return $this->userEnableWithoutEmailVerification;
    }

    /**
     * @param bool $userEnableWithoutEmailVerification
     */
    public function setUserEnableWithoutEmailVerification(bool $userEnableWithoutEmailVerification): void
    {
        $this->userEnableWithoutEmailVerification = $userEnableWithoutEmailVerification;
    }

    /**
     * @return string
     */
    public function getBeforeUserConfirmGroups(): string
    {
        return $this->beforeUserConfirmGroups;
    }

    /**
     * @param string $beforeUserConfirmGroups
     */
    public function setBeforeUserConfirmGroups(string $beforeUserConfirmGroups): void
    {
        $this->beforeUserConfirmGroups = $beforeUserConfirmGroups;
    }

    /**
     * @return string
     */
    public function getAfterUserConfirmationGroups(): string
    {
        return $this->afterUserConfirmationGroups;
    }

    /**
     * @param string $afterUserConfirmationGroups
     */
    public function setAfterUserConfirmationGroups(string $afterUserConfirmationGroups): void
    {
        $this->afterUserConfirmationGroups = $afterUserConfirmationGroups;
    }

    /**
     * @return string
     */
    public function getGeneralSenderEmailAddress(): string
    {
        return $this->generalSenderEmailAddress;
    }

    /**
     * @param string $generalSenderEmailAddress
     */
    public function setGeneralSenderEmailAddress(string $generalSenderEmailAddress): void
    {
        $this->generalSenderEmailAddress = $generalSenderEmailAddress;
    }

    /**
     * @return string
     */
    public function getGeneralSenderEmailName(): string
    {
        return $this->generalSenderEmailName;
    }

    /**
     * @param string $generalSenderEmailName
     */
    public function setGeneralSenderEmailName(string $generalSenderEmailName): void
    {
        $this->generalSenderEmailName = $generalSenderEmailName;
    }

    /**
     * @return bool
     */
    public function isUseEmailAsUsername(): bool
    {
        return $this->useEmailAsUsername;
    }

    /**
     * @param bool $useEmailAsUsername
     */
    public function setUseEmailAsUsername(bool $useEmailAsUsername): void
    {
        $this->useEmailAsUsername = $useEmailAsUsername;
    }

    /**
     * @return bool
     */
    public function isEnableChangeUsername(): bool
    {
        return $this->enableChangeUsername;
    }

    /**
     * @param bool $enableChangeUsername
     */
    public function setEnableChangeUsername(bool $enableChangeUsername): void
    {
        $this->enableChangeUsername = $enableChangeUsername;
    }

    /**
     * @return int
     */
    public function getChangeUsernameAction(): int
    {
        return $this->changeUsernameAction;
    }

    /**
     * @param int $changeUsernameAction
     */
    public function setChangeUsernameAction(int $changeUsernameAction): void
    {
        $this->changeUsernameAction = $changeUsernameAction;
    }

    /**
     * @return string
     */
    public function getDeciders(): string
    {
        return $this->deciders;
    }

    /**
     * @param string $deciders
     */
    public function setDeciders(string $deciders): void
    {
        $this->deciders = $deciders;
    }

    /**
     * @return bool
     */
    public function isEnableApi(): bool
    {
        return $this->enableApi;
    }

    /**
     * @param bool $enableApi
     */
    public function setEnableApi(bool $enableApi): void
    {
        $this->enableApi = $enableApi;
    }

    /**
     * @return bool
     */
    public function isEnableFeReport(): bool
    {
        return $this->enableFeReport;
    }

    /**
     * @param bool $enableFeReport
     */
    public function setEnableFeReport(bool $enableFeReport): void
    {
        $this->enableFeReport = $enableFeReport;
    }

    /**
     * @return bool
     */
    public function isEnableTestEnvironment(): bool
    {
        return $this->enableTestEnvironment;
    }

    /**
     * @param bool $enableTestEnvironment
     */
    public function setEnableTestEnvironment(bool $enableTestEnvironment): void
    {
        $this->enableTestEnvironment = $enableTestEnvironment;
    }

    /**
     * @return string
     */
    public function getTestingEmail(): string
    {
        return $this->testingEmail;
    }

    /**
     * @param string $testingEmail
     */
    public function setTestingEmail(string $testingEmail): void
    {
        $this->testingEmail = $testingEmail;
    }

    /**
     * @return bool
     */
    public function isEnableModelAnnotationValidation(): bool
    {
        return $this->enableModelAnnotationValidation;
    }

    /**
     * @param bool $enableModelAnnotationValidation
     */
    public function setEnableModelAnnotationValidation(bool $enableModelAnnotationValidation): void
    {
        $this->enableModelAnnotationValidation = $enableModelAnnotationValidation;
    }

    public function getRegistrationFormFields()
    {
        return new RegistrationFormFields($this->fields);
    }

    public function jsonSerialize()
    {
        return [
            'fields' => $this->fields ? json_decode($this->fields) : [],
            'config' => [
                'uid' => $this->uid,
                'title' => $this->title,
                'doubleOptin' => $this->doubleOptin,
                'adminAuth' => $this->adminAuth,
                'adminEmails' => $this->adminEmails,
                'afterAdminConfirmGroups' => $this->afterAdminConfirmGroups,
                'userEnableWithoutAdminAuth' => $this->userEnableWithoutAdminAuth,
                'adminAuthEnableSignals' => $this->adminAuthEnableSignals,
                'enableEmailModerationResultToUser' => $this->enableEmailModerationResultToUser,
                'userEnableWithoutEmailVerification' => $this->userEnableWithoutEmailVerification,
                'beforeUserConfirmGroups' => $this->beforeUserConfirmGroups,
                'afterUserConfirmationGroups' => $this->afterUserConfirmationGroups,
                'generalSenderEmailAddress' => $this->generalSenderEmailAddress,
                'generalSenderEmailName' => $this->generalSenderEmailName,
                'useEmailAsUsername' => $this->useEmailAsUsername,
                'enableChangeUsername' => $this->enableChangeUsername,
                'changeUsernameAction' => $this->changeUsernameAction,
                'deciders' => $this->deciders != '' ? json_decode($this->deciders) : [],
                'enableApi' => $this->enableApi,
                'enableFeReport' => $this->enableFeReport,
                'testingEmail' => $this->testingEmail,
                'enableTestEnvironment' => $this->enableTestEnvironment,
                'enableModelAnnotationValidation' => $this->enableModelAnnotationValidation
            ]
        ];
    }

    public function toJson()
    {
        return json_encode($this);
    }
}
