<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Domain\Model;

class RegistrationFormFields
{
    protected $fields = [];

    /**
     * RegistrationFormFields constructor.
     * @param string $fields
     */
    public function __construct(string $fields)
    {
        $fieldsData = json_decode($fields, true);
        $this->fields = $this->arrangeFieldsStructure($fieldsData);
    }

    protected function arrangeFieldsStructure(array $fields): array
    {
        $arrangedStructure = [];
        foreach ($fields as $field) {
            $arrangedStructure[] = [
                'id' => $field['id'],
                'name' => $field['name'],
                'validators' => $this->prepareValidatorForUsage($field['config']['validators'])
            ];
        }
        return $arrangedStructure;
    }

    protected function prepareValidatorForUsage(array $validators): array
    {
        $preparedValidators = [];
        foreach ($validators as $validator) {
            $tempValidator['class'] = str_replace('_', '\\', $validator['id']);
            $tempValidator['options'] = [];
            foreach ($validator['params'] as $option) {
                $tempValidator['options'][$option['id']] = $option['value'];
            }
            $preparedValidators[] = $tempValidator;
        }
        return $preparedValidators;
    }

    public function getValidatorsForField(string $fieldName): array
    {
        foreach ($this->fields as $field) {
            if (isset($field['id']) && $field['id'] == $fieldName) {
                return $field['validators'];
            }
        }
        return [];
    }

    public function getFieldsList(): array
    {
        $fieldsList = [];
        foreach ($this->fields as $field) {
            $fieldsList[] = $field['id'];
        }
        return $fieldsList;
    }
}
