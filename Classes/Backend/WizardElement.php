<?php declare(strict_types=1);

/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 15/11/2018$
 */
namespace Pallino\T3registration\Backend;

class WizardElement
{
    /**
     * Processing the wizard items array
     *
     * @param array $wizardItems The wizard items
     * @return array Modified array with wizard items
     */
    public function proc($wizardItems): array
    {
        $wizardItems['plugins_tx_t3registration_form'] = [
            'iconIdentifier' => 'RegistrationFormWizard',
            'title' => $this->getTranslationForLabel('LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:plugin_wizard_title'),
            'description' => $this->getTranslationForLabel('LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:plugin_wizard_description'),
            'params' => '&defVals[tt_content][CType]=list&&defVals[tt_content][list_type]=t3registration_form'
        ];

        return $wizardItems;
    }

    protected function getTranslationForLabel($label)
    {
        return $GLOBALS['LANG']->sL($label);
    }
}
