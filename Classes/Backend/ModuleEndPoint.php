<?php declare(strict_types=1);
/**
 *
 *  PALLINO COPYRIGHT SOFTWARE
 *   __________________
 *
 *  [20010] - [2018] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Pallino & Co. Srl  and its suppliers,
 *  if any.  The intellectual and technical concepts contained
 *  herein are proprietary to Pallino & Co. Srl
 *  and its suppliers and are protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Pallino & Co. Srl .
 *
 */
namespace Pallino\T3registration\Backend;

use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Property\PropertyMapper;

class ModuleEndPoint
{
    const LL_PATH = 'LLL:EXT:t3registration/Resources/Private/Language/locallang_module.xlf:';
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var RegistrationFormRepository
     */
    protected $registrationFormRepository;

    /**
     * @var PersistenceManager
     */
    protected $persistenceManager;

    /**
     * @var PropertyMapper
     */
    protected $propertyMapper;

    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->registrationFormRepository = $this->objectManager->get(RegistrationFormRepository::class);
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $this->propertyMapper = $this->objectManager->get(PropertyMapper::class);
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     * @return Response
     */
    public function save(ServerRequestInterface $request, Response $response)
    {
        try {
            $jsonContent = $this->getJsonContent($request);
            $registrationForm = $this->getFormattedObject($jsonContent);
            $this->saveRegistrationForm($registrationForm);
            $message = $this->getTranslation();
            $response = $this->setResponse($response, 200, $message, $registrationForm->toJson());
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->setResponse($response, 500, $message);
        }
        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    protected function getJsonContent(ServerRequestInterface $request): string
    {
        return $request->getBody()->getContents();
    }

    /**
     * @param $jsonContent
     * @return mixed
     * @throws \TYPO3\CMS\Extbase\Property\Exception
     */
    protected function getFormattedObject($jsonContent): RegistrationForm
    {
        $objectFields = json_decode($jsonContent)->fields;
        $objectConfig = (array)json_decode($jsonContent)->config;
        if (isset($objectConfig['uid'])) {
            $objectConfig['__identity'] = $objectConfig['uid'];
            unset($objectConfig['uid']);
        }
        $deciders = $objectConfig['deciders'];
        unset($objectConfig['deciders']);
        $registrationForm = $this->getRegistrationFormObject($objectConfig, $objectFields, $deciders);
        return $registrationForm;
    }

    /**
     * @param $formArray
     * @param $objectFields
     * @param $deciders
     * @return mixed|object
     * @throws \TYPO3\CMS\Extbase\Property\Exception
     */
    protected function getRegistrationFormObject($formArray, $objectFields, $deciders): RegistrationForm
    {
        if (array_key_exists('uid', $formArray) && is_null($formArray['uid'])) {
            unset($formArray['uid']);
        }
        $registrationForm = $this->propertyMapper->convert($formArray, RegistrationForm::class);
        $registrationForm->setFields(json_encode($objectFields));
        $registrationForm->setDeciders(json_encode($deciders));
        return $registrationForm;
    }

    /**
     * @param RegistrationForm $registrationForm
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    protected function saveRegistrationForm(RegistrationForm $registrationForm): void
    {
        if ($registrationForm->getUid()) {
            $this->registrationFormRepository->update($registrationForm);
        } else {
            $this->registrationFormRepository->add($registrationForm);
        }
        $this->persistenceManager->persistAll();
    }

    /**
     * @return string
     */
    public function getTranslation(): string
    {
        $message = $GLOBALS['LANG']->sL(self::LL_PATH . 'formList.save_correctly');
        return $message;
    }

    /**
     * @param Response $response
     * @param $message
     * @param $registrationForm
     */
    protected function setResponse(Response $response, $status, $message, $body = ''): Response
    {
        $response->withStatus($status, $message);
        $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write($body);
        return $response;
    }
}
