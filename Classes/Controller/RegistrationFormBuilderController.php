<?php declare(strict_types=1);

/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 26/10/2018$
 */
namespace Pallino\T3registration\Controller;

use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\FrontendUserGroupRepository;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use Pallino\T3registration\Utility\DeciderUtility;
use Pallino\T3registration\Utility\UserTcaUtility;
use Pallino\T3registration\Utility\ValidatorUtility;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class RegistrationFormBuilderController extends ActionController
{
    const LL_PATH = 'LLL:EXT:t3registration/Resources/Private/Language/locallang_module.xlf:';

    /**
     * @var RegistrationFormRepository
     */
    protected $registrationFormRepository;

    /**
     * @var FrontendUserGroupRepository
     */
    protected $frontendUserGroupRepository;

    public function injectRegistrationFormRepository(RegistrationFormRepository $registrationFormRepository): void
    {
        $this->registrationFormRepository = $registrationFormRepository;
    }

    public function injectFrontendUserGroupRepository(FrontendUserGroupRepository $frontendUserGroupRepository): void
    {
        $this->frontendUserGroupRepository = $frontendUserGroupRepository;
    }

    public function listAction()
    {
        $forms = $this->registrationFormRepository->findAll();
        $this->view->assignMultiple([
            'forms' => $forms
        ]);
    }

    public function newAction()
    {
        $json = $this->getConfigurationArray();
        $userGroups = $this->getFrontendUserGroups();

        if (!empty($this->settings['translationFile'])) {
            $this->getPageRenderer()->addInlineLanguageLabelFile($this->settings['translationFile']);
        }

        $emptyForm = new RegistrationForm();
        $this->view->assignMultiple([
            'config' => $json,
            'feUserGroups' => $userGroups,
            'jsonForm' => $emptyForm->toJson(),
            'extensionPath' => substr(str_replace(Environment::getPublicPath(), '', ExtensionManagementUtility::extPath('t3registration')), 0, -1)
        ]);
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function editAction()
    {
        $json = $this->getConfigurationArray();
        $jsonForm = $this->getCurrentForm();
        $userGroups = $this->getFrontendUserGroups();

        if (!empty($this->settings['translationFile'])) {
            $this->getPageRenderer()->addInlineLanguageLabelFile($this->settings['translationFile']);
        }

        if ($jsonForm) {
            $this->view->assignMultiple([
                'config' => $json,
                'jsonForm' => $jsonForm,
                'feUserGroups' => $userGroups,
                'extensionPath' => substr(str_replace(Environment::getPublicPath(), '', ExtensionManagementUtility::extPath('t3registration')), 0, -1)
            ]);
        } else {
            $this->addFlashMessage(LocalizationUtility::translate(self::LL_PATH . 'formList.form_not_found'), LocalizationUtility::translate(self::LL_PATH . 'formList.error_occurred'), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            $this->redirect('list');
        }
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction()
    {
        if ($this->request->hasArgument('form')) {
            $registrationFormID = $this->request->getArgument('form');
            $registrationForm = $this->registrationFormRepository->findByUid($registrationFormID);
            $this->registrationFormRepository->remove($registrationForm);
            $this->addFlashMessage(LocalizationUtility::translate(self::LL_PATH . 'formList.remove_correctly'));
        } else {
            $this->addFlashMessage(LocalizationUtility::translate(self::LL_PATH . 'formList.form_not_found'), LocalizationUtility::translate(self::LL_PATH . 'formList.error_occurred'), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
        }
        return $this->redirect('list');
    }

    /**
     * @return null
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     */
    protected function getCurrentForm()
    {
        $jsonForm = null;
        if ($this->request->hasArgument('form')) {
            $registrationFormID = $this->request->getArgument('form');
            $registrationForm = $this->registrationFormRepository->findByUid($registrationFormID);
            if ($registrationForm) {
                $jsonForm = $registrationForm->toJson();
            }
        }
        return $jsonForm;
    }

    /**
     * @return array
     */
    protected function getFrontendUserGroups()
    {
        $groups = [];
        $userGroups = $this->frontendUserGroupRepository->findAllIgnoringPid();
        foreach ($userGroups as $group) {
            $groups[] = [
                'id' => $group->getUid(),
                'title' => $group->getTitle()
            ];
        }
        return $groups;
    }

    /**
     * @return array
     */
    protected function getConfigurationArray()
    {
        $userFields = UserTcaUtility::getFields();
        $validatorFields = ValidatorUtility::getValidatorsDefinitionStructure();
        $deciderFields = DeciderUtility::getDecidersDefinitionStructure();
        $json = [
            'fields' => $userFields,
            'validators' => $validatorFields,
            'deciders' => $deciderFields
        ];
        return $json;
    }

    /**
     * Returns the page renderer
     *
     * @return PageRenderer
     */
    protected function getPageRenderer(): PageRenderer
    {
        return GeneralUtility::makeInstance(PageRenderer::class);
    }
}
