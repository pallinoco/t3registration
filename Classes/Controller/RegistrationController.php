<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Controller;

use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Model\RegistrationFormFields;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use Pallino\T3registration\Process\MainCommonProcess;
use Pallino\T3registration\Process\ProcessInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Property\Exception\TargetNotFoundException;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility;

class RegistrationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements \Psr\Log\LoggerAwareInterface
{
    use LoggerAwareTrait;

    const SEVERITY_HEIGHT = 3;
    const SEVERITY_MEDIUM = 2;
    const SEVERITY_LOW = 1;

    /** @var \Pallino\T3registration\Domain\Repository\FrontendUserRepository */
    protected $frontendUserRepository;
    /** @var RegistrationFormRepository */
    protected $registrationFormRepository;
    /** @var RegistrationForm */
    protected $registrationForm;
    /** @var RegistrationFormFields */
    protected $registrationFormFields;
    /** @var bool */
    protected $logEnabled;
    /** @var array */
    protected $validationResult = [];
    /** @var int */
    protected $severityLevel = self::SEVERITY_LOW;
    /** @var Request */
    protected $request;

    /** @var TypoScriptFrontendController */
    protected $typoscriptFrontendController;

    /** @var ProcessInterface */
    protected $process = null;

    /**
     * @var TokenAuthenticationInterface
     */
    protected $tokenAuthentication;

    public function injectRegistrationFormRepository(RegistrationFormRepository $registrationFormRepository): void
    {
        $this->registrationFormRepository = $registrationFormRepository;
    }

    public function injectTokenAuthentication(TokenAuthenticationInterface $tokenAuthentication)
    {
        $this->tokenAuthentication = $tokenAuthentication;
    }

    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository): void
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    public function injectLogger(\TYPO3\CMS\Core\Log\LogManager $logManager): void
    {
        GeneralUtility::deprecationLog(sprintf(
            'Class %s: injectLogger will be removed in 3.0, logger will be automatically injected',
            __CLASS__
        ));
        $this->logger = $logManager->getLogger(__CLASS__);
    }

    public function newAction(FrontendUser $user = null): void
    {
        $this->getProcess()->process($this->request, $this->response);
        $this->getProcess()->close();
        $this->view->assign('user', $this->getProcess()->getUser());
    }

    protected function getProcess()
    {
        if (!($this->process instanceof ProcessInterface)) {
            $processObject = $this->objectManager->get(
                MainCommonProcess::class,
                $this->controllerContext,
                $this->settings
            );
            $this->process = $processObject->getProcess();
        }
        return $this->process;
    }

    public function createAction(FrontendUser $user): void
    {
        if ($this->getProcess()->process($this->request, $this->response, $this->controllerContext)) {
            $this->persistNewUser($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    protected function persistNewUser(FrontendUser &$user): void
    {
        $this->setPassword($user);
        $preparedArguments = [$user];
        $this->emitMethodSignal('beforeUserCreate', $preparedArguments);
        $this->frontendUserRepository->add($user);
        $preparedArguments = [$user];
        $this->emitMethodSignal('afterUserCreate', $preparedArguments);
        $this->writeLog(
            self::SEVERITY_LOW,
            sprintf('user %s was created.', $user->getUsername()),
            LogLevel::INFO
        );
    }

    public function setPassword(FrontendUser $user): FrontendUser
    {
        if ($this->request->hasArgument('user') && $this->checkForSaltedExtensionInstalled()) {
            $requestUserArray = $this->request->getArgument('user');
            /** @noinspection PhpIllegalStringOffsetInspection */
            if (isset($requestUserArray['password'])) {
                $objSalt = $this->getSaltInstance();
                if (is_object($objSalt)) {
                    $user->setPassword($objSalt->getHashedPassword($user->getPassword()));
                } else {
                    throw new Exception('Salted password encryption error: ObjSalt is not defined.');
                }
            }
        }
        return $user;
    }

    protected function checkForSaltedExtensionInstalled()
    {
        return ExtensionManagementUtility::isLoaded('saltedpasswords') && SaltedPasswordsUtility::isUsageEnabled('FE');
    }

    protected function getSaltInstance()
    {
        return \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance(null);
    }

    protected function emitMethodSignal(string $event, array $preparedArguments): void
    {
        $this->signalSlotDispatcher->dispatch(__CLASS__, $event, [$preparedArguments]);
    }

    protected function writeLog(
        int $severity,
        string $message,
        int $logLevel = LogLevel::INFO,
        array $context = []
    ): void {
        if ($this->isLogEnabled() && $this->severityLevel <= $severity) {
            $this->logger->log($logLevel, $message, $context);
        }
    }

    protected function isLogEnabled(): bool
    {
        return GeneralUtility::cmpIP(
            GeneralUtility::getIndpEnv('REMOTE_ADDR'),
            $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask']
        ) && $this->settings['enableLog'];
    }

    public function updateCompleteAction(FrontendUser $user)
    {
        $this->view->assign('user', $user);
    }

    public function doubleOptinConfirmationAction()
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->frontendUserRepository->update($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    public function changeEmailConfirmationAction(FrontendUser $user = null)
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->frontendUserRepository->update($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    public function doubleOptinProcessCompleteAction(FrontendUser $user)
    {
        $this->view->assign('user', $user);
    }

    public function moderationConfirmAction()
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->frontendUserRepository->update($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    public function moderationRejectAction()
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->frontendUserRepository->update($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    public function changeEmailProcessCompleteAction(FrontendUser $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * @param string $token
     */
    public function tokenNotValidAction(string $token = '')
    {
        $this->view->assign('token', $token);
    }

    /**
     * @param FrontendUser|null $user
     */
    public function registrationCompleteAction(FrontendUser $user = null): void
    {
        $this->view->assign('user', $this->getProcess()->getUser());
    }

    public function previewAction(FrontendUser $user): void
    {
        if (!$this->getProcess()->process($this->request, $this->response, $this->controllerContext)) {
            //todo: Questo errore avviene da un'eccezione in Process
            throw new Exception('Preview Error da sistemare');
        }
        $this->getProcess()->close();
        $this->view->assign('user', $user);
    }

    public function editAction(FrontendUser $user = null)
    {
        if (!$this->getProcess()->process($this->request, $this->response, $this->controllerContext)) {
            //todo: Questo errore avviene da un'eccezione in Process
            throw new Exception('Preview Error da sistemare');
        }
        $this->getProcess()->close();
        $this->view->assign('user', $this->getProcess()->getUser());
    }

    public function updateAction(FrontendUser $user)
    {
        if ($this->request->hasArgument($this->settings['backToEditFromPreviewButtonName'])) {
            $this->forward('edit', null, null, ['user' => $user]);
        } else {
            if ($this->getProcess()->process($this->request, $this->response, $this->controllerContext)) {
                $this->persistUpdatedUser($this->getProcess()->getUser());
            }
            $this->getProcess()->close();
        }
    }

    protected function persistUpdatedUser(FrontendUser $user): void
    {
        if ($user->getPassword() != '') {
            $user = $this->setPassword($user);
        }
        $preparedArguments = [$user];
        $this->emitMethodSignal('beforeUserUpdate', $preparedArguments);
        $this->frontendUserRepository->update($user);
        $preparedArguments = [$user];
        $this->emitMethodSignal('afterUserUpdate', $preparedArguments);
        $this->writeLog(
            self::SEVERITY_LOW,
            sprintf('user %s was updated.', $user->getUsername()),
            LogLevel::INFO
        );
    }

    public function accessDeniedAction(FrontendUser $user = null)
    {
    }

    public function deleteAction(FrontendUser $user): void
    {
        if ($this->getProcess()->process($this->request, $this->response, $this->controllerContext)) {
            $this->persistUpdatedUser($user);
        }
        $this->getProcess()->close();
    }

    public function deleteProcessEmailSentAction(FrontendUser $user)
    {
        $this->view->assign('user', $user);
    }

    public function deleteProcessCancellationConfirmationAction(FrontendUser $user)
    {
        $this->view->assign('user', $user);
    }

    public function deleteProcessCancellationAction()
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->persistUpdatedUser($this->getProcess()->getUser());
            $this->writeLog(
                self::SEVERITY_LOW,
                sprintf(
                    'user %s has received email to cancel but decline.',
                    $this->getProcess()->getUser()->getUsername()
                ),
                LogLevel::INFO
            );
        }
        $this->getProcess()->close();
    }

    public function deleteProcessConfirmationAction()
    {
        if ($this->getProcess()->process($this->request, $this->response)) {
            $this->persistDeletedUser($this->getProcess()->getUser());
        }
        $this->getProcess()->close();
    }

    public function deleteProcessConfirmationCompleteAction(FrontendUser $user = null)
    {
        $this->view->assign('user', $user);
    }

    public function deleteProcessCancellationCompleteAction(FrontendUser $user = null)
    {
        $this->view->assign('user', $user);
    }

    protected function persistDeletedUser(FrontendUser $user): void
    {
        $preparedArguments = [$user];
        $this->emitMethodSignal('beforeUserDelete', $preparedArguments);
        $this->frontendUserRepository->remove($user);
        $preparedArguments = [$user];
        $this->emitMethodSignal('afterUserDelete', $preparedArguments);
        $this->writeLog(
            self::SEVERITY_LOW,
            sprintf('user %s was deleted.', $user->getUsername()),
            LogLevel::INFO
        );
    }

    protected function callActionMethod(): void
    {
        $preparedArguments = [];
        /** @var \TYPO3\CMS\Extbase\Mvc\Controller\Argument $argument */
        foreach ($this->arguments as $argument) {
            $preparedArguments[] = $argument->getValue();
        }
        //add t3registration validation process
        $validationResult = $this->getProcess()->getValidationResults();
        if (!$validationResult->hasErrors()) {
            $this->emitBeforeCallActionMethodSignal($preparedArguments);
            $actionResult = call_user_func_array([$this, $this->actionMethodName], $preparedArguments);
        } else {
            $methodTagsValues = $this->reflectionService->getMethodTagsValues(
                get_class($this),
                $this->actionMethodName
            );
            $ignoreValidationAnnotations = [];
            if (isset($methodTagsValues['ignorevalidation'])) {
                $ignoreValidationAnnotations = $methodTagsValues['ignorevalidation'];
            }
            // if there exist errors which are not ignored with @ignorevalidation => call error method
            // else => call action method
            $shouldCallActionMethod = true;
            foreach ($validationResult->getSubResults() as $argumentName => $subValidationResult) {
                if (!$subValidationResult->hasErrors()) {
                    continue;
                }
                if (array_search('$' . $argumentName, $ignoreValidationAnnotations) !== false) {
                    continue;
                }
                $shouldCallActionMethod = false;
                break;
            }
            if ($shouldCallActionMethod) {
                $this->emitBeforeCallActionMethodSignal($preparedArguments);
                $actionResult = call_user_func_array([$this, $this->actionMethodName], $preparedArguments);
            } else {
                $actionResult = call_user_func([$this, $this->errorMethodName]);
            }
        }

        if ($actionResult === null && $this->view instanceof ViewInterface) {
            $this->response->appendContent($this->view->render());
        } elseif (is_string($actionResult) && $actionResult !== '') {
            $this->response->appendContent($actionResult);
        } elseif (is_object($actionResult) && method_exists($actionResult, '__toString')) {
            $this->response->appendContent((string)$actionResult);
        }
    }

    protected function errorAction()
    {
        $this->clearCacheOnError();
        $this->forwardToReferringRequest();
        return $this->getFlattenedValidationErrorMessage();
    }

    protected function forwardToReferringRequest(): void
    {
        $referringRequest = $this->request->getReferringRequest();
        if ($referringRequest !== null) {
            $originalRequest = clone $this->request;
            $this->request->setOriginalRequest($originalRequest);
            $this->request->setOriginalRequestMappingResults($this->getProcess()->getValidationResults());
            $this->forward(
                $referringRequest->getControllerActionName(),
                $referringRequest->getControllerName(),
                $referringRequest->getControllerExtensionName(),
                $referringRequest->getArguments()
            );
        }
    }

    protected function initializeAction(): void
    {
        $this->typoscriptFrontendController = $this->getTyposcriptFrontendController();
    }

    protected function getTyposcriptFrontendController(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }

    protected function mapRequestArgumentsToControllerArguments()
    {
        try {
            parent::mapRequestArgumentsToControllerArguments();
        } catch (TargetNotFoundException $exception) {
        } finally {
            if ($this->request->hasArgument('user') && (!$this->arguments->hasArgument('user') || !$this->arguments->getArgument('user')->getValue() instanceof FrontendUser)) {
                $user = $this->frontendUserRepository->getOriginalUserIgnoringDisable($this->request->getArgument('user'));
                if ($user != null) {
                    $this->arguments->getArgument('user')->setValue($user);
                }
            }
        }
    }
}
