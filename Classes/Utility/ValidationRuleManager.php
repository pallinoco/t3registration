<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Utility;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationFormFields;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\ColumnMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;

class ValidationRuleManager
{
    /**
     * @var null|FrontendUser
     */
    protected $frontendUser = null;

    /**
     * @var null|RegistrationFormFields
     */
    protected $registrationFormFields = null;

    /**
     * @var DataMapper
     */
    protected $dataMapper;

    /**
     * @var DataMap
     */
    protected $dataMap;

    /**
     * @var array
     */
    protected $fieldsStructureWithValueAndValidators;

    /** @var ObjectManager */
    protected $objectManager;

    protected $excludeFields = [];

    /**
     * ValidationRuleManager constructor.
     * @param null|FrontendUser $frontendUser
     * @param RegistrationFormFields $registrationFormFields
     * @param array $excludeFields
     */
    public function __construct(
        FrontendUser $frontendUser,
        RegistrationFormFields $registrationFormFields,
        array $excludeFields = []
    ) {
        $this->frontendUser = $frontendUser;
        $this->registrationFormFields = $registrationFormFields;
        $this->excludeFields = $excludeFields;
    }

    public function injectDataMapper(DataMapper $dataMapper): void
    {
        $this->dataMapper = $dataMapper;
        $this->dataMap = $this->dataMapper->getDataMap(FrontendUser::class);
    }

    public function injectObjectManager(ObjectManager $objectManager): void
    {
        $this->objectManager = $objectManager;
    }

    public function process(): Result
    {
        $this->fieldsStructureWithValueAndValidators = $this->getValueFromUserObjectWithValidators();
        $result = $this->getResult();
        foreach ($this->fieldsStructureWithValueAndValidators as $fieldKey => $field) {
            if (in_array($fieldKey, $this->excludeFields)) {
                continue;
            }
            $result->forProperty($field['propertyName'])->merge($this->validateField($fieldKey));
        }
        return $result;
    }

    protected function getValueFromUserObjectWithValidators(): array
    {
        $properties = $this->frontendUser->_getProperties();
        $propertyMappedValue = [];
        $fields = $this->registrationFormFields->getFieldsList();
        foreach ($properties as $propertyName => $propertyValue) {
            $columnMap = $this->dataMap->getColumnMap($propertyName);
            if ($columnMap instanceof ColumnMap) {
                $databaseField = $columnMap->getColumnName();
                if (in_array($databaseField, $fields)) {
                    $propertyMappedValue[$databaseField] = [
                        'propertyName' => $propertyName,
                        'value' => $propertyValue,
                        'validators' => $this->registrationFormFields->getValidatorsForField($databaseField)
                    ];
                }
            }
        }
        return $propertyMappedValue;
    }

    protected function getResult(): Result
    {
        return $this->objectManager->get(Result::class);
    }

    protected function validateField($fieldName): Result
    {
        $conjunctionValidator = $this->getConjunctionValidator();
        foreach ($this->fieldsStructureWithValueAndValidators[$fieldName]['validators'] as $validator) {
            $validatorObject = $this->createValidator($validator['class'], $validator['options']);
            if ($validatorObject == null) {
                continue;
            }
            $conjunctionValidator->addValidator($validatorObject);
        }
        return $conjunctionValidator->validate($this->fieldsStructureWithValueAndValidators[$fieldName]['value']);
    }

    protected function getConjunctionValidator(): ConjunctionValidator
    {
        return $this->objectManager->get(ConjunctionValidator::class);
    }

    protected function createValidator($validatorName, $validatorOptions = []): ?ValidatorInterface
    {
        if ($validatorName[0] === '\\') {
            $validatorName = substr($validatorName, 1);
        }
        if (ValidatorUtility::hasValidator($validatorName)) {
            return $this->objectManager->get($validatorName, $validatorOptions);
        } else {
            return null;
        }
    }
}
