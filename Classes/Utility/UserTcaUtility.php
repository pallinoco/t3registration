<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

namespace Pallino\T3registration\Utility;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class UserTcaUtility
{
    protected static $fieldsNotExported = [
        'disable',
        'hidden',
        'endtime',
        'felogin_forgotHash',
        'felogin_redirectPid',
        'lastlogin',
        'lockToDomain',
        'TSconfig',
        'tx_extbase_type'
    ];

    public static function getFields()
    {
        $fields = self::getFeUserTCA();
        $fieldsArray = [];
        foreach ($fields as $fieldKey => $fieldValue) {
            if (self::isFieldVisible($fieldKey, $fieldValue)) {
                $fieldsArray[] = [
                    'id' => $fieldKey,
                    'name' => self::getTranslation($fieldValue['label'])
                ];
            }
        }
        return $fieldsArray;
    }

    protected static function isFieldVisible($fieldKey, $fieldValue)
    {
        if (in_array($fieldKey, self::$fieldsNotExported)) {
            $isVisible = false;
        } elseif (isset($fieldValue['excludeFromRegistrationForm']) && $fieldValue['excludeFromRegistrationForm'] == 1) {
            $isVisible = false;
        } else {
            $isVisible = true;
        }
        return $isVisible;
    }

    protected static function getFeUserTCA(): array
    {
        return (isset($GLOBALS['TCA']['fe_users']['columns'])) ? $GLOBALS['TCA']['fe_users']['columns'] : [];
    }

    protected static function getTranslation($key)
    {
        return LocalizationUtility::translate($key);
    }
}
