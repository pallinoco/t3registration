<?php declare(strict_types=1);

/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2019 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 2019-06-05$
 */
namespace Pallino\T3registration\Utility;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use TYPO3\CMS\Core\Exception;

class UserEnableResolver
{
    const ADMIN_APPROVAL = 1;
    const DOUBLEOPTIN_APPROVAL = 2;

    /** @var FrontendUser */
    protected $user;
    /** @var RegistrationForm */
    protected $formConfiguration;

    public function isUserToEnable(
        FrontendUser $user,
        RegistrationForm $formConfiguration,
        int $action,
        bool $AdminAuthorized = true
    ): bool {
        $this->user = $user;
        $this->formConfiguration = $formConfiguration;
        if ($action == $this::ADMIN_APPROVAL) {
            return $this->isUserToEnableFromAdminModerationProcess($AdminAuthorized);
        } elseif ($action == $this::DOUBLEOPTIN_APPROVAL) {
            return $this->isUserToEnableFromDoubleOptinProcess($AdminAuthorized);
        } else {
            throw new Exception('UserEnableResolver called with unsupported action argument');
        }
    }

    protected function isUserToEnableFromAdminModerationProcess(bool $AdminAuthorized): bool
    {
        if (!$AdminAuthorized) {
            return false;
        } elseif ($this->formConfiguration->isDoubleOptin()) {
            if ($this->user->getUserAuthenticationToken() != '') {
                if ($this->formConfiguration->isUserEnableWithoutEmailVerification()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    protected function isUserToEnableFromDoubleOptinProcess(bool $AdminAuthorized): bool
    {
        if ($this->formConfiguration->isAdminAuth()) {
            if ($this->user->getAdminModerationToken() != '') {
                if ($this->formConfiguration->isUserEnableWithoutAdminAuth()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (!$AdminAuthorized) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return true;
        }
    }
}
