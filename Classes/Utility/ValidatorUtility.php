<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Utility;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class ValidatorUtility manages the operation of validator (add, remove, get, etc...)
 *
 * @author Federico Bernardin <federico@bernardin.it>
 */
class ValidatorUtility implements \TYPO3\CMS\Core\SingletonInterface
{
    const TRANSLATION_FILE_PATH = 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.';

    private static $validators = [];

    /**
     * Add the validator
     * @param string $validator the validator to add
     * @param array $options
     */

    /**
     * Add the validator
     * @param string $validator the validator to add
     * @param array $options list of optionally value for validator
     * @throws \ReflectionException
     */
    public static function addValidator(string $validator, array $options = []): void
    {
        if (class_exists($validator) && !array_key_exists($validator, self::$validators)) {
            $validatorClassReflection = new \ReflectionClass($validator);
            if (in_array(
                \TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface::class,
                $validatorClassReflection->getInterfaceNames()
            )) {
                self::$validators[$validator] = ['class' => $validator, 'options' => $options];
            }
        }
    }

    /**
     * Remove all validator
     */
    public static function removeAll(): void
    {
        foreach (self::$validators as $validatorKey => $validator) {
            self::removeValidator($validatorKey);
        }
    }

    /**
     * Remove single validator from key
     * @param $validator
     */
    public static function removeValidator(string $validator): void
    {
        if (array_key_exists($validator, self::$validators)) {
            unset(self::$validators[$validator]);
        }
    }

    /**
     * @return array validators array
     */
    public static function getValidators(): array
    {
        return self::$validators;
    }

    /**
     * @param array $validators
     */
    public static function setValidators(array $validators): void
    {
        self::$validators = $validators;
    }

    public static function hasValidator(string $validator): bool
    {
        if (array_key_exists($validator, self::$validators)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * return specific validator object or null id it's not found
     * @param string $key key to search
     * @return null|\TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface the specific validator or null
     */
    public static function getValidator(string $key): \TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface
    {
        if (isset(self::$validators[$key])) {
            return self::$validators[$key];
        } else {
            return null;
        }
    }

    public static function getValidatorsDefinitionStructure(): array
    {
        $validatorsStructure = [];
        foreach (self::$validators as $validator) {
            $validatorsStructure[] = self::getValidatorSubStructureSignature($validator);
        }
        return $validatorsStructure;
    }

    protected static function getValidatorSubStructureSignature(array $validator): array
    {
        $options = [];
        foreach ($validator['options'] as $option) {
            $options[] = [
                'id' => $option,
                'name' => self::getTranslationLabel(self::TRANSLATION_FILE_PATH . self::getClassNameFromNamespace($validator['class']) . '.' . $option . '.label')
            ];
        }
        return
            [
                'id' => $validator['class'],
                'name' => self::getTranslationLabel(self::TRANSLATION_FILE_PATH . self::getClassNameFromNamespace($validator['class']) . '.label'),
                'params' => $options
            ];
    }

    protected static function getClassNameFromNamespace(string $namespace): string
    {
        $path = explode('\\', $namespace);
        return array_pop($path);
    }

    protected static function getTranslationLabel($translationKey)
    {
        return LocalizationUtility::translate($translationKey);
    }
}
