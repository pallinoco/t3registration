<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Utility;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class UserLoggedUtility
{
    /** @var FrontendUserRepository */
    protected $frontendUserRepository;

    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    public function isUserLoggedEqualTo($uid, $username = ''): bool
    {
        if ($this->getTyposcriptFrontendController()->fe_user && ($loggedData = $this->getLoggedUserData()) != null) {
            if ($uid != null) {
                return $loggedData['uid'] == $uid;
            } elseif ($username != '') {
                return $loggedData['username'] == $username;
            }
        }
        return false;
    }

    public function isUserLogged(): bool
    {
        return $this->getTyposcriptFrontendController()->loginUser;
    }

    protected function getTyposcriptFrontendController(): ?TypoScriptFrontendController
    {
        if (isset($GLOBALS['TSFE']->fe_user)) {
            return $GLOBALS['TSFE'];
        } else {
            return null;
        }
    }

    protected function getLoggedUserData(): ?array
    {
        if ($this->getTyposcriptFrontendController() != null &&
            isset($this->getTyposcriptFrontendController()->fe_user->user) &&
            is_array($this->getTyposcriptFrontendController()->fe_user->user)) {
            return $this->getTyposcriptFrontendController()->fe_user->user;
        } else {
            return null;
        }
    }

    public function getLoggedUser(): ?FrontendUser
    {
        $loggedUser = $this->getLoggedUserData();
        if (is_array($loggedUser)) {
            return $this->frontendUserRepository->findByUid($loggedUser['uid']);
        } else {
            return null;
        }
    }
}
