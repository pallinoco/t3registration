<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Utility;

use Pallino\T3registration\Decider\DeciderInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Class ValidatorUtility manages the operation of validator (add, remove, get, etc...)
 *
 * @author Federico Bernardin <federico@bernardin.it>
 */
class DeciderUtility implements \TYPO3\CMS\Core\SingletonInterface
{
    const TRANSLATION_FILE_PATH = 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.';

    private static $deciders = [];

    /**
     * Add the validator
     * @param string $validator the validator to add
     * @param array $options
     */

    /**
     * Add the decider
     * @param string $decider the decider to add
     * @param array $options list of optionally value for decider
     * @throws \ReflectionException
     */
    public static function addDecider(string $decider, array $options = []): void
    {
        if (class_exists($decider) && !array_key_exists($decider, self::$deciders)) {
            $deciderClassReflection = new \ReflectionClass($decider);
            if (in_array(
                DeciderInterface::class,
                $deciderClassReflection->getInterfaceNames()
            )) {
                self::$deciders[$decider] = ['class' => $decider, 'options' => $options];
            }
        }
    }

    /**
     * Remove all validator
     */
    public static function removeAll(): void
    {
        foreach (self::$deciders as $deciderKey => $decider) {
            self::removeDecider($deciderKey);
        }
    }

    /**
     * Remove single decider from key
     * @param $decider
     */
    public static function removeDecider(string $decider): void
    {
        if (array_key_exists($decider, self::$deciders)) {
            unset(self::$deciders[$decider]);
        }
    }

    /**
     * @return array deciders array
     */
    public static function getDeciders(): array
    {
        return self::$deciders;
    }

    /**
     * @param array $deciders
     */
    public static function setDeciders(array $deciders): void
    {
        self::$deciders = $deciders;
    }

    public static function hasDecider(string $decider): bool
    {
        if (array_key_exists($decider, self::$deciders)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * return specific validator object or null id it's not found
     * @param string $key key to search
     * @return null|\TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface the specific validator or null
     */
    public static function getDecider(string $key): \TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface
    {
        if (isset(self::$deciders[$key])) {
            return self::$deciders[$key];
        } else {
            return null;
        }
    }

    public static function getDecidersDefinitionStructure(): array
    {
        $decidersStructure = [];
        foreach (self::$deciders as $decider) {
            $decidersStructure[] = self::getDeciderSubStructureSignature($decider);
        }
        return $decidersStructure;
    }

    protected static function getDeciderSubStructureSignature(array $decider): array
    {
        $options = [];
        foreach ($decider['options'] as $option) {
            $options[] = [
                'id' => $option,
                'name' => self::getTranslationLabel(self::TRANSLATION_FILE_PATH . self::getClassNameFromNamespace($decider['class']) . '.' . $option . '.label')
            ];
        }
        return
            [
                'id' => $decider['class'],
                'name' => self::getTranslationLabel(self::TRANSLATION_FILE_PATH . self::getClassNameFromNamespace($decider['class']) . '.label'),
                'params' => $options
            ];
    }

    protected static function getClassNameFromNamespace(string $namespace): string
    {
        $path = explode('\\', $namespace);
        return array_pop($path);
    }

    protected static function getTranslationLabel($translationKey)
    {
        return LocalizationUtility::translate($translationKey);
    }
}
