<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 05/12/2018$
 */
namespace Pallino\T3registration\Utility;

use Pallino\T3registration\Decider\DeciderInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DeciderRuleManager
{
    /**
     * @var null|FrontendUser
     */
    protected $frontendUser = null;

    /**
     * @var null|RegistrationForm
     */
    protected $registrationForm = null;

    protected $excludeDeciders = [];

    protected $deciders = [];

    /** @var ObjectManager */
    protected $objectManager;

    /** @var Request */
    protected $request;

    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * ValidationRuleManager constructor.
     * @param null|FrontendUser $frontendUser
     * @param RegistrationForm $registrationForm
     * @param Request $request
     * @param array $excludeDeciders
     */
    public function __construct(
        FrontendUser $frontendUser,
        RegistrationForm $registrationForm,
        Request $request,
        array $excludeDeciders = []
    ) {
        $this->frontendUser = $frontendUser;
        $this->registrationForm = $registrationForm;
        $this->excludeDeciders = $excludeDeciders;
        $this->request = $request;
        $this->setDeciders();
    }

    protected function prepareDecidersForUsage(array $deciders): array
    {
        $preparedDeciders = [];
        foreach ($deciders as $decider) {
            $tempDecider['class'] = str_replace('_', '\\', $decider['id']);
            $tempDecider['options'] = [];
            foreach ($decider['params'] as $option) {
                $tempDecider['options'][$option['id']] = $option['value'];
            }
            $preparedDeciders[] = $tempDecider;
        }
        return $preparedDeciders;
    }

    public function process(): Result
    {
        $result = new Result();
        $requestArguments = $this->request->getArguments();
        foreach ($this->deciders as $decider) {
            $deciderObject = $this->createDecider($decider['class'], $decider['options']);
            if (in_array($decider, $this->excludeDeciders) || $deciderObject == null) {
                continue;
            }
            if ($deciderObject->getErrorOwnerField() != null) {
                $field = $deciderObject->getErrorOwnerField();
            } else {
                $field = 'generic';
            }
            $deciderObject->setFrontendUserArguments($requestArguments);
            $result->forProperty($field)->merge($deciderObject->validate($this->frontendUser));
        }
        return $result;
    }

    protected function setDeciders(): void
    {
        $decidersEncoded = $this->registrationForm->getDeciders();
        $decodedDeciders = json_decode($decidersEncoded, true);
        if (!is_array($this->deciders) || !is_array($decodedDeciders)) {
            $this->deciders = [];
        } else {
            $this->deciders = $this->prepareDecidersForUsage($decodedDeciders);
        }
    }

    protected function createDecider($deciderName, $deciderOptions = []): ?DeciderInterface
    {
        if ($deciderName[0] === '\\') {
            $deciderName = substr($deciderName, 1);
        }
        if (DeciderUtility::hasDecider($deciderName)) {
            return $this->objectManager->get($deciderName, $deciderOptions);
        } else {
            return null;
        }
    }
}
