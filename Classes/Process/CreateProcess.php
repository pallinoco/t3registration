<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 28/11/2018$
 */
namespace Pallino\T3registration\Process;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Mail\DoubleOptInProcess;
use Pallino\T3registration\Mail\ModerationProcess;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;

class CreateProcess extends AbstractProcess
{
    const USERNAME_COULD_BE_EMPTY = 500500500;
    const USER_CLICK_TO_EDIT = 500500501;
    protected $created = false;

    public function process(\TYPO3\CMS\Extbase\Mvc\Web\Request $request, Response $response): bool
    {
        try {
            parent::process($request, $response);
            $this->userClickedToEditButtonInPreview();
            if (!$this->created) {
                $this->initialize();
            }
            if ($this->getValidationResults()->hasErrors()) {
                return false;
            }
            $this->startDoubleOptinProcess();
            $this->startModerationProcess();
            return true;
        } catch (Exception $exception) {
            $this->processResult->addError(new Error($exception->getMessage(), $exception->getCode()));
            return false;
        }
    }

    protected function userClickedToEditButtonInPreview()
    {
        if ($this->request->hasArgument($this->controllerSettings['backToEditFromPreviewButtonName'])) {
            throw new Exception(
                'Create: backToEditFromPreviewButtonName was clicked, forward to edit',
                self::USER_CLICK_TO_EDIT
            );
        }
    }

    protected function initialize()
    {
        $this->processResult->clear();
        $this->validateUsername($this->user);
        $this->setEmailAsUsername();
        $this->attachUserGroupsBeforeConfirmation();
        $this->created = true;
    }

    /**
     * @param FrontendUser $user
     * @throws Exception
     */
    protected function validateUsername(): void
    {
        if (($this->registrationForm->isUseEmailAsUsername() && $this->user->getEmail() == '') || (!$this->registrationForm->isUseEmailAsUsername() && $this->user->getUsername() == '')) {
            throw new Exception(
                'Username field hasn\'t not empty validation and it is empty',
                self::USERNAME_COULD_BE_EMPTY
            );
        }
    }

    protected function setEmailAsUsername()
    {
        if ($this->registrationForm->isUseEmailAsUsername()) {
            $this->user->setUsername($this->user->getEmail());
        }
    }

    protected function attachUserGroupsBeforeConfirmation()
    {
        $groupsIds = explode(',', $this->registrationForm->getBeforeUserConfirmGroups());
        $usergroups = $this->getFrontendUserGroups($groupsIds);
        foreach ($usergroups as $usergroup) {
            $this->user->addUsergroup($usergroup);
        }
    }

    protected function getFrontendUserGroups($groupsIds)
    {
        return $this->frontendUserGroupRepository->findAllByIds($groupsIds);
    }

    public function getValidationResults(): Result
    {
        return parent::getValidationResults();
    }

    protected function startDoubleOptinProcess(): void
    {
        if ($this->registrationForm->isDoubleOptin()) {
            $this->prepareDoubleOptinUserConfirmation();
            $doubleOptinProcess = $this->getDoubleOptinObject();
            $doubleOptinProcess->process();
        }
    }

    protected function startModerationProcess(): void
    {
        if ($this->registrationForm->isAdminAuth()) {
            $this->prepareModerationData();
            $moderationObject = $this->getModerationObject();
            $moderationObject->process();
        }
    }

    protected function getModerationObject(): ModerationProcess
    {
        $moderationProcess = $this->objectManager->get(
            ModerationProcess::class,
            $this->user,
            $this->registrationForm,
            $this->controllerSettings
        );
        return $moderationProcess;
    }

    protected function prepareModerationData(): void
    {
        $token = $this->tokenAuthentication->getToken();
        $this->user->setAdminModerationToken($token);
        if (!$this->registrationForm->isUserEnableWithoutAdminAuth()) {
            $this->user->setDisable($this->registrationForm->isAdminAuth());
        }
        if ($this->registrationForm->isAdminAuthEnableSignals()) {
            $preparedArguments = [$this->user];
            $this->emitMethodSignal('prepareUserBeforeEmailModeration', $preparedArguments);
        }
    }

    protected function prepareDoubleOptinUserConfirmation(): void
    {
        if ($this->registrationForm->isDoubleOptin()) {
            $token = $this->tokenAuthentication->getToken();
            $this->user->setUserAuthenticationToken($token);
            $this->user->setDisable(!$this->registrationForm->isUserEnableWithoutEmailVerification());
            $preparedArguments = [$this->user];
            $this->emitMethodSignal('prepareUserConfirmation', $preparedArguments);
            $this->setDoubleOptinLandingPage();
        } else {
            $this->user->setDisable(false);
        }
    }

    protected function setDoubleOptinLandingPage(): void
    {
        if ($this->controllerSettings['doubleoptinConfirmationLandingPage'] == '') {
            $this->controllerSettings['doubleoptinConfirmationLandingPage'] = $this->getCurrentPageId();
        }
    }

    public function close(): void
    {
        parent::close();
        if ($this->processResult->hasErrors()) {
            $this->forward('new', ['user' => $this->user]);
        } else {
            $this->redirect('registrationComplete', ['user' => $this->user]);
        }
    }

    /**
     * @return object|DoubleOptInProcess
     */
    protected function getDoubleOptinObject()
    {
        $doubleOptinProcess = $this->objectManager->get(
            DoubleOptInProcess::class,
            $this->user,
            $this->registrationForm,
            $this->controllerSettings
        );
        return $doubleOptinProcess;
    }

    /**
     * @return int
     */
    protected function getCurrentPageId()
    {
        if (is_object($GLOBALS['TSFE'])) {
            return (int)$GLOBALS['TSFE']->id;
        }
        return 0;
    }
}
