<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 02/12/2018$
 */
namespace Pallino\T3registration\Process;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Mail\MailProcessInterface;
use TYPO3\CMS\Extbase\Error\Result;

class DeleteProcess extends AbstractProcess
{
    public function process(
        \TYPO3\CMS\Extbase\Mvc\Web\Request $request,
        \TYPO3\CMS\Extbase\Mvc\Response $response
    ): bool {
        parent::process($request, $response);

        if (!$this->canGetUserFromLogged()) {
            $this->processResult->addError(new \TYPO3\CMS\Extbase\Error\Error(
                'Update process: cannot get any user from session',
                self::CANNOT_GET_USER_FROM_SESSION
            ));
            return false;
        }
        $this->user = $this->prepareUserDeletingProcess($this->user);
        $deleteProcess = $this->getMailProcessObject();
        $deleteProcess->process();
        return true;
    }

    protected function prepareUserDeletingProcess(FrontendUser $user): FrontendUser
    {
        $token = $this->tokenAuthentication->getToken();
        $this->user->setUserAuthenticationToken($token);
        if ($this->controllerSettings['deleteConfirmationLandingPage'] == '') {
            $this->controllerSettings['deleteConfirmationLandingPage'] = $this->getCurrentPageId();
        }
        return $user;
    }

    protected function getCurrentPageId(): string
    {
        return $GLOBALS['TSFE']->id;
    }

    /**
     * @return object|MailProcessInterface
     */
    protected function getMailProcessObject()
    {
        $mailProcessProcess = $this->objectManager->get(
            \Pallino\T3registration\Mail\DeleteProcess::class,
            $this->user,
            $this->registrationForm,
            $this->controllerSettings
        );
        return $mailProcessProcess;
    }

    public function close(): void
    {
        if ($this->processResult->hasErrors()) {
            $this->redirect('accessDenied');
        } else {
            $this->redirect('deleteProcessEmailSent', ['user' => $this->user]);
        }
    }

    public function getValidationResults(): Result
    {
        return new Result();
    }
}
