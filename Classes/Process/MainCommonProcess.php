<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 28/11/2018$
 */
namespace Pallino\T3registration\Process;

use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class MainCommonProcess
{
    /** @var ProcessInterface */
    protected $processObject = null;

    /** @var ObjectManager */
    protected $objectManager;

    protected $validationResult = [];

    protected $controllerSettings = [];

    /** @var RegistrationFormRepository */
    protected $registrationFormRepository = null;

    /** @var RegistrationForm */
    protected $registrationForm = null;

    /** @var ControllerContext */
    protected $controllerContext;

    protected $processClass = '';

    public function __construct(ControllerContext $controllerContext, array $controllerSettings)
    {
        $this->controllerContext = $controllerContext;
        $this->processClass = 'Pallino\\T3registration\\Process\\' . ucfirst($this->controllerContext->getRequest()->getControllerActionName()) . 'Process';
        $this->controllerSettings = $controllerSettings;
    }

    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectRegistrationFormRepository(RegistrationFormRepository $registrationFormRepository)
    {
        $this->registrationFormRepository = $registrationFormRepository;
        $this->registrationForm = $this->registrationFormRepository->findByUid($this->controllerSettings['registrationForm']);
    }

    public function getProcess(): ProcessInterface
    {
        if ($this->processObject == null) {
            $this->setOverrideClassForProcess();
            if (class_exists($this->processClass)) {
                $this->processObject = $this->getProcessObject($this->processClass);
            } else {
                $this->processObject = $this->getProcessObject(SimpleProcess::class);
            }
        }
        return $this->processObject;
    }

    protected function setOverrideClassForProcess(): void
    {
        if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['processOverride'][$this->processClass])) {
            ksort($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['processOverride'][$this->processClass]);
            $processClassName = array_pop($GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['processOverride'][$this->processClass]);
            if ($processClassName instanceof ProcessInterface) {
                $this->processClass = $processClassName;
            }
        }
    }

    protected function getProcessObject(string $processObjectName): ProcessInterface
    {
        return $this->objectManager->get(
            $processObjectName,
            $this->controllerContext,
            $this->controllerSettings
        );
    }
}
