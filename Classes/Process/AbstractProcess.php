<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 28/11/2018$
 */
namespace Pallino\T3registration\Process;

use Nimut\TestingFramework\Http\Response;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\FrontendUserGroupRepository;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use Pallino\T3registration\Utility\DeciderRuleManager;
use Pallino\T3registration\Utility\UserLoggedUtility;
use Pallino\T3registration\Utility\ValidationRuleManager;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

abstract class AbstractProcess implements ProcessInterface
{
    const TOKEN_NOT_VALID = 500500900;

    const TOKEN_MISSED = 500500901;

    const CANNOT_GET_USER_FROM_SESSION = 500500902;
    /**
     * @var
     */
    protected $processName;
    /** @var ProcessInterface */
    protected $processObject = null;

    /** @var ObjectManager */
    protected $objectManager;

    protected $validationResult = null;

    protected $controllerSettings = [];

    /** @var RegistrationFormRepository */
    protected $registrationFormRepository = null;

    /** @var RegistrationForm */
    protected $registrationForm = null;

    /** @var ControllerContext */
    protected $controllerContext;

    /** @var FrontendUser */
    protected $user = null;

    protected $excludeFieldFromValidation = [];

    protected $excludeDecidersFromValidation = [];

    /** @var ConfigurationManagerInterface */
    protected $configurationManager;

    /** @var Result */
    protected $processResult;

    /** @var Dispatcher */
    protected $signalSlotDispatcher;
    /** @var Request */
    protected $request;
    /** @var Response */
    protected $response;

    /** @var PersistenceManager */
    protected $persistenceManager;

    /** @var FrontendUserGroupRepository */
    protected $frontendUserGroupRepository;

    /** @var UserLoggedUtility */
    protected $userLoggedUtility;

    /** @var FrontendUserRepository */
    protected $frontendUserRepository;

    /**
     * @var TokenAuthenticationInterface
     */
    protected $tokenAuthentication;

    /** @var string */
    protected $token;

    public function injectTokenAuthentication(TokenAuthenticationInterface $tokenAuthentication)
    {
        $this->tokenAuthentication = $tokenAuthentication;
    }

    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    public function injectUserLoggedUtility(UserLoggedUtility $userLoggedUtility)
    {
        $this->userLoggedUtility = $userLoggedUtility;
    }

    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function injectFrontendUserGroupRepository(FrontendUserGroupRepository $frontendUserGroupRepository)
    {
        $this->frontendUserGroupRepository = $frontendUserGroupRepository;
    }

    public function injectRegistrationFormRepository(RegistrationFormRepository $registrationFormRepository)
    {
        $this->registrationFormRepository = $registrationFormRepository;
        $this->registrationForm = $this->registrationFormRepository->findByUid($this->controllerSettings['registrationForm']);
    }

    public function injectPersistanceManager(PersistenceManager $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * @param ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher
     */
    public function injectSignalSlotDispatcher(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher)
    {
        $this->signalSlotDispatcher = $signalSlotDispatcher;
    }

    public function __construct(ControllerContext $controllerContext, array $controllerSettings)
    {
        $this->controllerContext = $controllerContext;
        $this->controllerSettings = $controllerSettings;
        if ($this->controllerContext->getArguments()->hasArgument('user')) {
            $this->user = $this->controllerContext->getArguments()->getArgument('user')->getValue();
        }
        $this->processResult = new Result();
        $this->request = $this->controllerContext->getRequest();
    }

    public function process(
        \TYPO3\CMS\Extbase\Mvc\Web\Request $request,
        \TYPO3\CMS\Extbase\Mvc\Response $response
    ): bool {
        $this->request = $request;
        $this->response = $response;
        return true;
    }

    public function getValidationResults(): Result
    {
        if (!($this->validationResult instanceof Result)) {
            if ($this->isDisableAnnotationValidation()) {
                $this->validationResult = new Result();
            } else {
                $this->validationResult = $this->controllerContext->getArguments()->getValidationResults();
            }
            if ($this->user instanceof FrontendUser) {
                /** @var ValidationRuleManager $validationRuleManager */
                $validationRuleManager = $this->getValidationRuleManager();
                $this->validationResult->forProperty('user')
                    ->merge(
                        $validationRuleManager->process()
                    );
                $deciderRuleManager = $this->getDeciderRuleManager();
                $this->validationResult->forProperty('user')
                    ->merge(
                        $deciderRuleManager->process()
                    );
            }
        }
        return $this->validationResult;
    }

    protected function getArrayWithExcludedFieldsFromValidation()
    {
        return array_keys($this->excludeFieldFromValidation);
    }

    protected function getArrayWithExcludedDecidersFromValidation()
    {
        return array_keys($this->excludeDecidersFromValidation);
    }

    protected function getValidationRuleManager()
    {
        $excludeFields = $this->getArrayWithExcludedFieldsFromValidation();
        return $this->objectManager->get(
            ValidationRuleManager::class,
            $this->user,
            $this->registrationForm->getRegistrationFormFields(),
            $excludeFields
        );
    }

    protected function getDeciderRuleManager()
    {
        $excludeDeciders = $this->getArrayWithExcludedDecidersFromValidation();
        return $this->objectManager->get(
            DeciderRuleManager::class,
            $this->user,
            $this->registrationForm,
            $this->request,
            $excludeDeciders
        );
    }

    protected function isDisableAnnotationValidation(): bool
    {
        return !$this->registrationForm->isEnableModelAnnotationValidation();
    }

    public function getResult(): Result
    {
        return $this->processResult;
    }

    public function forward(string $actionName, array $arguments = []): void
    {
        $forwardUtility = $this->objectManager->get(
            ProcessRedirectAndForward::class,
            $this->request,
            $this->response,
            $this->controllerContext
        );
        $forwardUtility->forward($actionName, $arguments);
    }

    public function redirect(string $actionName, array $arguments = [], string $pageUid = null): void
    {
        $forwardUtility = $this->objectManager->get(
            ProcessRedirectAndForward::class,
            $this->request,
            $this->response,
            $this->controllerContext
        );
        $forwardUtility->redirect($actionName, $arguments, $pageUid);
    }

    protected function getProcessName(): string
    {
        return $this->processName;
    }

    protected function emitMethodSignal(string $event, array $preparedArguments): void
    {
        $this->signalSlotDispatcher->dispatch(__CLASS__, $event, [$preparedArguments]);
    }

    public function getUser(): ?FrontendUser
    {
        return $this->user;
    }

    public function close(): void
    {
        $this->persistenceManager->persistAll();
        if ($this->processResult->hasErrors()) {
            switch ($this->processResult->getFirstError()->getCode()) {
                case self::TOKEN_MISSED:
                    $this->redirect('tokenNotValid');
                    break;
                case self::TOKEN_NOT_VALID:
                    $this->redirect('tokenNotValid', ['token' => $this->token]);
                    break;
            }
        }
    }

    protected function getUserFromToken(string $token): ?FrontendUser
    {
        //fake method, need override
        return null;
    }

    protected function verifyTokenInProcess(): ?FrontendUser
    {
        $user = null;
        if ($this->request->hasArgument('token')) {
            $token = $this->request->getArgument('token');
            if (strlen($token) > 0 && $this->tokenAuthentication->isValid($token) == TokenAuthenticationInterface::VALID) {
                $user = $this->getUserFromToken($token);
                if ($user == null) {
                    $this->token = $token;
                    throw new Exception('Token not valid, user not found', self::TOKEN_NOT_VALID);
                }
            } else {
                $this->token = $token;
                throw new Exception('Token not valid', self::TOKEN_NOT_VALID);
            }
        } else {
            throw new Exception('Missing token', self::TOKEN_MISSED);
        }
        return $user;
    }

    protected function canGetUserFromLogged()
    {
        if ($this->user == null) {
            if ($this->userLoggedUtility->isUserLogged()) {
                $this->user = $this->userLoggedUtility->getLoggedUser();
                return true;
            }
        } else {
            return $this->userLoggedUtility->isUserLoggedEqualTo($this->user->getUid());
        }
        return false;
    }

    protected function getRegistrationForm()
    {
        return $this->registrationForm;
    }
}
