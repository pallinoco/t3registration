<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Process;

use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Mail\ChangeEmailProcess;
use Pallino\T3registration\Mail\MailProcessInterface;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Error\Result;

class UpdateProcess extends EditProcess
{
    const CANNOT_FIND_UPDATED_USER_FROM_PERSISTENCE = 500500700;

    /**
     * @var TokenAuthenticationInterface
     */
    protected $tokenAuthentication;

    public function injectTokenAuthentication(TokenAuthenticationInterface $tokenAuthentication)
    {
        $this->tokenAuthentication = $tokenAuthentication;
    }

    public function process(
        \TYPO3\CMS\Extbase\Mvc\Web\Request $request,
        \TYPO3\CMS\Extbase\Mvc\Response $response
    ): bool {
        $this->request = $request;
        $this->response = $response;
        if (!$this->canGetUserFromLogged()) {
            $this->processResult->addError(new Error(
                'Update process: cannot get any user from session',
                self::CANNOT_GET_USER_FROM_SESSION
            ));
            return false;
        }
        if (!$this->isPasswordToValidate()) {
            $this->user->addExcludeProperty('password');
        }
        if ($this->isUsernameToValidate()) {
            $this->processEmailProcessModification();
            $this->startChangeEmailMailProcess();
        }
        return true;
    }

    protected function startChangeEmailMailProcess()
    {
        $this->getChangeEmailMailProcess()->process();
    }

    protected function processEmailProcessModification()
    {
        if ($this->registrationForm->isUseEmailAsUsername()) {
            if ($this->registrationForm->getChangeUsernameAction()) {
                $this->user->setTemporaryEmailChanged($this->user->getEmail());
                $token = $this->tokenAuthentication->getToken();
                $this->user->setEmailChangeProcessToken($token);
                $this->user->setEmail($this->user->getUsername());
                $preparedArguments = [$this->user];
                $this->emitMethodSignal('setChangedEmailConfirmation', $preparedArguments);
            } else {
                $this->user->setUsername($this->user->getEmail());
                $preparedArguments = [$this->user];
                $this->emitMethodSignal('setChangedEmailWithoutEmailProcessConfirmation', $preparedArguments);
            }
        }
    }

    public function close(): void
    {
        if ($this->processResult->hasErrors()) {
            switch ($this->processResult->getFirstError()->getCode()) {
                case self::CANNOT_GET_USER_FROM_SESSION:
                    $this->redirect('accessDenied');
                    break;
                case self::CANNOT_FIND_UPDATED_USER_FROM_PERSISTENCE:
                    $this->redirect('accessDenied');
                    break;
            }
        } else {
            $this->redirect('updateComplete', ['user' => $this->user], null);
        }
    }

    protected function isPasswordToValidate(): bool
    {
        return $this->user->getPassword() != '';
    }

    protected function isUsernameToValidate(): bool
    {
        $modifiedUser = $this->getUserFromPersistence($this->user);
        if (!($modifiedUser instanceof FrontendUser)) {
            throw new Exception(
                'Update process: cannot find user from persistence',
                self::CANNOT_FIND_UPDATED_USER_FROM_PERSISTENCE
            );
        }
        if ($this->registrationForm->isUseEmailAsUsername()) {
            return $modifiedUser->getEmail() != $this->user->getEmail();
        } else {
            return $modifiedUser->getUsername() != $this->user->getUsername();
        }
    }

    protected function getUserFromPersistence(FrontendUser $modifiedUser): ?FrontendUser
    {
        return $this->frontendUserRepository->findFreshUserByUid($modifiedUser->getUid());
    }

    public function getValidationResults(): Result
    {
        if (!$this->isPasswordToValidate()) {
            $this->excludeFieldFromValidation['password'] = 1;
        }
        if (!$this->isUsernameToValidate()) {
            if ($this->registrationForm->isUseEmailAsUsername()) {
                $this->excludeFieldFromValidation['username'] = 1;
                $this->excludeFieldFromValidation['email'] = 1;
            } else {
                $this->excludeFieldFromValidation['username'] = 1;
            }
        }
        return parent::getValidationResults();
    }

    /**
     * @return object|MailProcessInterface
     */
    protected function getChangeEmailMailProcess(): MailProcessInterface
    {
        $changeEmailProcess = $this->objectManager->get(
            ChangeEmailProcess::class,
            $this->user,
            $this->registrationForm,
            $this->controllerSettings
        );
        return $changeEmailProcess;
    }
}
