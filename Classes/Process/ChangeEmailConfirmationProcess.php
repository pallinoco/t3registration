<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 02/12/2018$
 */
namespace Pallino\T3registration\Process;

use Pallino\T3registration\Domain\Model\FrontendUser;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Error\Error;

class ChangeEmailConfirmationProcess extends AbstractProcess
{
    public function process(
        \TYPO3\CMS\Extbase\Mvc\Web\Request $request,
        \TYPO3\CMS\Extbase\Mvc\Response $response
    ): bool {
        parent::process($request, $response);
        try {
            $this->user = $this->verifyTokenInProcess();
            $this->user->setEmail($this->user->getTemporaryEmailChanged());
            $this->user->setUsername($this->getUser()->getTemporaryEmailChanged());
            $this->user->setTemporaryEmailChanged('');
            $this->user->setEmailChangeProcessToken('');
            $preparedArguments = [$this->user];
            $this->emitMethodSignal('afterChangeEmailConfirmation', $preparedArguments);
            return true;
        } catch (Exception $exception) {
            $this->processResult->addError(new Error($exception->getMessage(), $exception->getCode()));
            return false;
        }
    }

    public function close(): void
    {
        parent::close();
        if (!$this->processResult->hasErrors()) {
            $this->redirect('changeEmailProcessComplete', ['user' => $this->user]);
        }
    }

    protected function getUserFromToken(string $token): ?FrontendUser
    {
        /** @var FrontendUser $user */
        return $this->frontendUserRepository->findUserFromTokenEmail($token);
    }
}
