<?php declare(strict_types=1);

/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 28/11/2018$
 */
namespace Pallino\T3registration\Process;

use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class ProcessRedirectAndForward
{
    /** @var Request */
    protected $request;
    /** @var Response */
    protected $response;
    /** @var ControllerContext */
    protected $controllerContext;
    /** @var ObjectManager */
    protected $objectManager;

    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function __construct(Request $request, Response $response, ControllerContext $controllerContext)
    {
        $this->request = $request;
        $this->response = $response;
        $this->controllerContext = $controllerContext;
    }

    public function forward($actionName, array $arguments = null)
    {
        $this->request->setDispatched(false);
        if ($this->request instanceof Request) {
            $this->request->setControllerActionName($actionName);
        }
        if ($arguments !== null) {
            $this->request->setArguments($arguments);
        }
        throw new StopActionException('forward', 1476045801);
    }

    public function redirect($actionName, array $arguments = null, $pageUid = null, $delay = 0, $statusCode = 303)
    {
        if (!$this->request instanceof Request) {
            throw new UnsupportedRequestTypeException('redirect() only supports web requests.', 1220539734);
        }
        $this->controllerContext->getUriBuilder()->reset()->setTargetPageUid($pageUid)->setCreateAbsoluteUri(true);
        if (\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_SSL')) {
            $this->controllerContext->getUriBuilder()->setAbsoluteUriScheme('https');
        }
        $uri = $this->controllerContext->getUriBuilder()->uriFor(
            $actionName,
            $arguments,
            $this->request->getControllerName(),
            $this->request->getControllerExtensionName()
        );
        $this->redirectToUri($uri, $delay, $statusCode);
    }

    protected function redirectToUri($uri, $delay = 0, $statusCode = 303)
    {
        if (!$this->request instanceof Request) {
            throw new UnsupportedRequestTypeException('redirect() only supports web requests.', 1220539735);
        }

        $this->objectManager->get(\TYPO3\CMS\Extbase\Service\CacheService::class)->clearCachesOfRegisteredPageIds();

        $uri = $this->addBaseUriIfNecessary($uri);
        $escapedUri = htmlentities($uri, ENT_QUOTES, 'utf-8');
        $this->response->setContent('<html><head><meta http-equiv="refresh" content="' . (int)$delay . ';url=' . $escapedUri . '"/></head></html>');
        if ($this->response instanceof Response) {
            $this->response->setStatus($statusCode);
            $this->response->setHeader('Location', (string)$uri);
        }
        throw new StopActionException('redirectToUri', 1476045828);
    }

    protected function addBaseUriIfNecessary($uri)
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::locationHeaderUrl((string)$uri);
    }
}
