<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Validator;

use Pallino\T3registration\Domain\Repository\FrontendUserRepository;

class UsernameUniqueInPidValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{

    /**
     * @var \Pallino\T3registration\Domain\Repository\FrontendUserRepository
     */
    protected $frontendUserRepository = null;

    /**
     * This validator always needs to be executed even if the given value is empty.
     * See AbstractValidator::validate()
     *
     * @var bool
     */
    protected $acceptsEmptyValues = false;

    /**
     * @var array
     */
    protected $supportedOptions = [
        'pid' => [0, 'pid in which search uniqueness', 'integer', true]
    ];

    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository): void
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    public function isValid($value): void
    {
        if ($value == '') {
            $this->addError(
                $this->translateErrorMessage(
                    'validator.usernameUniqueInPid.usernameEmpty',
                    't3registration'
                ),
                200200300
            );
        }
        if (!$this->frontendUserRepository->isUsernameUniqueInPid($value, $this->options['pid'])) {
            $this->addError(
                $this->translateErrorMessage(
                    'validator.usernameUniqueInPid.isNotUnique',
                    't3registration'
                ),
                200200400
            );
        }
    }
}
