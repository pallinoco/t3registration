
#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
	tx_t3registration_privacy tinyint(4) unsigned DEFAULT '0' NOT NULL,
	tx_t3registration_user_authentication_token varchar(255) DEFAULT '' NOT NULL,
	tx_t3registration_admin_moderation_token varchar(255) DEFAULT '' NOT NULL,
	tx_t3registration_email_change_process_token varchar(255) DEFAULT '' NOT NULL,
	tx_t3registration_temporary_email_changed varchar(255) DEFAULT '' NOT NULL
);

CREATE TABLE tx_t3registration_domain_model_registrationform (
	uid int(11) unsigned NOT NULL auto_increment,
	pid int(11) unsigned DEFAULT '0' NOT NULL,
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	fields text,
	double_optin tinyint(4) unsigned DEFAULT '0' NOT NULL,
----------------- ADMIN
	admin_auth tinyint(4) unsigned DEFAULT '0' NOT NULL,
	admin_emails text,
	after_admin_confirm_groups varchar(255) DEFAULT '0' NOT NULL,
	user_enable_without_admin_auth tinyint(4) unsigned DEFAULT '0' NOT NULL,
	admin_auth_enable_signals tinyint(4) unsigned DEFAULT '0' NOT NULL,
	enable_email_moderation_result_to_user tinyint(4) unsigned DEFAULT '0' NOT NULL,
------------------
	user_enable_without_email_verification tinyint(4) unsigned DEFAULT '0' NOT NULL,
	before_user_confirm_groups varchar(255) DEFAULT '0' NOT NULL,
	after_user_confirmation_groups varchar(255) DEFAULT '0' NOT NULL,

	general_sender_email_address varchar(255) DEFAULT '' NOT NULL,
	general_sender_email_name varchar(255) DEFAULT '' NOT NULL,
----------------- EMAIL AS USERNAME
	use_email_as_username tinyint(4) unsigned DEFAULT '0' NOT NULL,
	enable_change_username tinyint(4) unsigned DEFAULT '0' NOT NULL,
	change_username_action int(11) unsigned NOT NULL,
------------------
	deciders text,
	enable_api tinyint(4) unsigned DEFAULT '0' NOT NULL,
	enable_fe_report tinyint(4) unsigned DEFAULT '0' NOT NULL,

	enable_test_environment tinyint(4) unsigned DEFAULT '0' NOT NULL,
	testing_email varchar(255) DEFAULT '' NOT NULL,

	enable_model_annotation_validation tinyint(4) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid)
);
