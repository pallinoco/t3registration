export default {
    methods: {
        getMetaContent(name) {
            const header = document.querySelector(`meta[name="${name}"]`)
            return header && header.content
        },
    }
}
