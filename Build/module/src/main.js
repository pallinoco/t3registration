import Vue from 'vue'
import VModal from 'vue-js-modal'
import {store} from './store/store'
import utils from './utils'
import App from './App.vue'

Vue.use(VModal)

new Vue({
    el: '#app',
    render: h => h(App),
    mixins: [utils],
    store,
    methods: {
        initConfig() {
            if (typeof(config) !== "undefined") {
                this.$store.dispatch('setConfig', config)
            }
        },
        initUserGroups() {
            if (typeof(feUserGroups) !== "undefined") {
                this.$store.dispatch('setUserGroups', feUserGroups)
            }
        },
        initForm() {
            if (typeof(userForm) !== "undefined") {
                this.$store.dispatch('setForm', userForm)
            }
        },
        initTranslation(){
            if (typeof(translationLabel) !== "undefined") {
                this.$store.dispatch('setTranslation', translationLabel)
            }
        },
        initExtensionPath(){
            if (typeof(extensionPath) !== "undefined") {
                this.$store.dispatch('setExtensionPath', extensionPath)
            }
        }
    },
    created() {
        this.initConfig()
        this.initUserGroups()
        this.initForm()
        this.initTranslation()
        this.initExtensionPath()
    }
})
