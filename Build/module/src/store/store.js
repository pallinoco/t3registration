import Vue from 'vue'
import Vuex from 'vuex'
import cloneDeep from 'lodash.clonedeep'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    config: {
      fields: [],
      validators: [],
      deciders: []
    },
    userGroups: [],
    form: {
      fields: [],
      config: {
        title: '',
        adminAuth: false,
        adminEmails: '',
        doubleOptin: false,
        afterAdminConfirmGroups: [],
        userEnableWithoutAdminAuth: false,
        adminAuthEnableSignals: false,
        enableEmailModerationResultToUser: false,
        userEnableWithoutEmailVerification: false,
        beforeUserConfirmGroups: [],
        afterUserConfirmationGroups: [],
        generalSenderEmailAddress: '',
        generalSenderEmailName: '',
        useEmailAsUsername: false,
        enableChangeUsername: false,
        changeUsernameAction: 0,
        deciders: [],
        enableApi: false,
        enableFeReport: false,
        testingEmail: '',
        enableTestEnvironment: false,
        enableModelAnnotationValidation: false,
      }
    },
    translation: [],
    extensionPath: '',
    isFormSelected: false,
    selectedFiledId: '',
    selectedValidatorId: '',
    selectedDeciderId: '',
    defaultFieldConfig: {
      typeOverwrite: false,
      validators: [],
      hiddenInPreview: false,
      hiddenInEdit: false,
      regExp: ''
    },
    flashMessages: []
  },
  getters: {
    getAvailableFields: state => state.config.fields.filter(field => !state.form.fields
      .reduce((acc, field) => {
        acc.push(field.id)
        return acc
      }, [])
      .includes(field.id)),
    getFormFields: state => state.form.fields,
    getFieldById: state => id => state.form.fields.find(field => field.id === id),
    getSelectedField: (state, getters) => getters.getFieldById(state.selectedFiledId) || {
      id: '',
      name: '',
      config: state.defaultFieldConfig
    },
    getShowFieldConfig: (state, getters) => getters.getSelectedField.id.length,
    getAvailableDeciders: state => {
      const usedDecidersIds = state.form.config.deciders !== undefined ? state.form.config.deciders
          .reduce((acc, decider) => {
            acc.push(decider.id)
            return acc
          }, [])
        :
        []
      return state.config.deciders.filter(decider => !usedDecidersIds.includes(decider.id))
    },
    getAvailableValidatorsForField: state => id => {
      const field = state.form.fields.find(field => field.id === id)
      const usedValidatorsIds = field !== undefined ? field.config.validators
          .reduce((acc, validator) => {
            acc.push(validator.id)
            return acc
          }, [])
        :
        []
      return state.config.validators.filter(validator => !usedValidatorsIds.includes(validator.id))
    },
    getValidatorByIds: (state, getters) => (fieldId, validatorId) => {
      const field = getters.getFieldById(fieldId)
      return field !== undefined ? field.config.validators.find(validator => validator.id === validatorId) : undefined
    },
    getSelectedValidator: (state, getters) => getters.getValidatorByIds(state.selectedFiledId, state.selectedValidatorId) || {
      id: '',
      name: ''
    },
    getDeciderById: (state) => (deciderId) => {
      return state.form.config.deciders.find(decider => decider.id === deciderId)
    },
    getShowValidatorConfig: (state, getters) => !!getters.getSelectedValidator.id.length,
    getForm: state => {
      const form = cloneDeep(state.form)
      form.config.afterAdminConfirmGroups = form.config.afterAdminConfirmGroups
        .reduce((acc, group) => {
          acc.push(group.id)
          return acc
        }, [])
        .join(',')
      form.config.beforeUserConfirmGroups = form.config.beforeUserConfirmGroups
        .reduce((acc, group) => {
          acc.push(group.id)
          return acc
        }, [])
        .join(',')
      form.config.afterUserConfirmationGroups = form.config.afterUserConfirmationGroups
        .reduce((acc, group) => {
          acc.push(group.id)
          return acc
        }, [])
        .join(',')
      return form
    },
    getTranslation: (state) => (translateKey) => {
      return state.translation[translateKey] ? state.translation[translateKey] : translateKey
    },
  },
  mutations: {
    setTranslation(state, payload) {
      state.translation = payload
    },
    setExtensionPath(state, payload) {
      state.extensionPath = payload
    },
    setConfig(state, payload) {
      state.config = payload
    },
    setForm(state, payload) {
      state.form = payload
    },
    setUserGroups(state, payload) {
      state.userGroups = payload
    },
    updateFormTitle(state, payload) {
      state.form.config.title = payload
    },
    updateFormDoubleOptin(state, payload) {
      state.form.config.doubleOptin = payload
    },
    updateFormAdminAuth(state, payload) {
      state.form.config.adminAuth = payload
    },
    updateFormAdminEmails(state, payload) {
      state.form.config.adminEmails = payload
    },
    updateFormUserEnableWithoutAdminAuth(state, payload) {
      state.form.config.userEnableWithoutAdminAuth = payload
    },
    updateFormAdminAuthEnableSignals(state, payload) {
      state.form.config.adminAuthEnableSignals = payload
    },
    updateFormEnableEmailModerationResultToUser(state, payload) {
      state.form.config.enableEmailModerationResultToUser = payload
    },
    updateFormUserEnableWithoutEmailVerification(state, payload) {
      state.form.config.userEnableWithoutEmailVerification = payload
    },
    updateFormAfterAdminConfirmGroups(state, payload) {
      state.form.config.afterAdminConfirmGroups = payload
    },
    updateFormBeforeUserConfirmGroups(state, payload) {
      state.form.config.beforeUserConfirmGroups = payload
    },
    updateFormAfterUserConfirmationGroups(state, payload) {
      state.form.config.afterUserConfirmationGroups = payload
    },
    updateFormGeneralSenderEmailAddress(state, payload) {
      state.form.config.generalSenderEmailAddress = payload
    },
    updateFormGeneralSenderEmailName(state, payload) {
      state.form.config.generalSenderEmailName = payload
    },
    updateFormUseEmailAsUsername(state, payload) {
      state.form.config.useEmailAsUsername = payload
    },
    updateFormEnableChangeUsername(state, payload) {
      state.form.config.enableChangeUsername = payload
    },
    updateFormChangeUsernameAction(state, payload) {
      state.form.config.changeUsernameAction = payload
    },
    updateFormDeciders(state, payload) {
      state.form.config.deciders = payload
    },
    updateFormEnableApi(state, payload) {
      state.form.config.enableApi = payload
    },
    updateFormEnableFeReport(state, payload) {
      state.form.config.enableFeReport = payload
    },
    updateFormTestingEmail(state, payload) {
      state.form.config.testingEmail = payload
    },
    updateFormEnableTestEnvironment(state, payload) {
      state.form.config.enableTestEnvironment = payload
    },
    updateFormEnableModelAnnotationValidation(state, payload) {
      state.form.config.enableModelAnnotationValidation = payload
    },
    updateFormType(state, payload) {
      state.form.config.type = payload
    },
    updateOptInGroupsBefore(state, payload) {
      state.form.config.optInGroupsBefore = payload
    },
    updateOptInGroupsAfter(state, payload) {
      state.form.config.optInGroupsAfter = payload
    },
    setFieldTypeOverwrite(state, payload) {
      payload.target.config.typeOverwrite = payload.value
    },
    setFieldHiddenInPreview(state, payload) {
      payload.target.config.hiddenInPreview = payload.value
    },
    setFieldRegExp(state, payload) {
      payload.target.config.regExp = payload.value
    },
    pushNewField(state, payload) {
      state.form.fields.push(payload)
    },
    pushNewValidator(state, payload) {
      payload.target.config.validators.push(payload.value)
    },
    pushNewDecider(state, payload) {
      state.form.config.deciders.push(payload)
    },
    setIsFormSelected(state, payload) {
      state.isFormSelected = payload
    },
    setSelectedFieldId(state, payload) {
      state.selectedFiledId = payload
    },
    setSelectedValidatorId(state, payload) {
      state.selectedValidatorId = payload
    },
    setSelectedDeciderId(state, payload) {
      state.selectedDeciderId = payload
    },
    removeField(state, payload) {
      top.TYPO3.Modal.confirm('Are you sure', 'Are you sure you want ot delete this item', top.TYPO3.Severity.warning)
        .on('confirm.button.ok', function () {
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
          state.form.fields.splice(state.form.fields.indexOf(state.form.fields.find(field => field.id === payload)), 1)
        })
        .on('confirm.button.cancel', function () {
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
        });
    },
    removeValidator(state, payload) {
      top.TYPO3.Modal.confirm('Are you sure', 'Are you sure you want ot delete this item', top.TYPO3.Severity.warning)
        .on('confirm.button.ok', function () {
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
          payload.target.config.validators.splice(payload.target.config.validators.indexOf(payload.target.config.validators.find(validator => validator.id === payload.validator)), 1)
        })
        .on('confirm.button.cancel', function () {
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
        });
    },
    removeDecider(state, payload) {
      top.TYPO3.Modal.confirm('Are you sure', 'Are you sure you want ot delete this item', top.TYPO3.Severity.warning)
        .on('confirm.button.ok', function () {
          state.form.config.deciders.splice(state.form.config.deciders.indexOf(state.form.config.deciders.find(decider => decider.id === payload.decider)), 1)
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
        })
        .on('confirm.button.cancel', function () {
          top.TYPO3.Modal.currentModal.trigger('modal-dismiss');
        });
    },
    setValidatorParam(state, payload) {
      payload.target.value = payload.value
    },
    setDeciderParam(state, payload) {
      payload.target.value = payload.value
    },
    toggleValidatorOpen(state, target) {
      target.open = !target.open
    },
    toggleDeciderOpen(state, target) {
      target.open = !target.open
    },
    pushFlashMessage(state, payload) {
      state.flashMessages = []
      state.flashMessages.push(payload)
    },
    flushFlashMessages(state) {
      state.flashMessages = []
    }
  },
  actions: {
    setTranslation({commit}, payload) {
      commit('setTranslation', payload)
    },
    setExtensionPath({commit}, payload) {
      commit('setExtensionPath', payload)
    },
    setConfig({commit}, payload) {
      payload.validators = payload.validators.map(validator => {
        validator.params.map(param => {
          param.value = ''
          return param
        })
        validator.open = false
        return validator
      })
      payload.deciders = payload.deciders.map(decider => {
        decider.params.map(param => {
          param.value = ''
          return param
        })
        decider.open = false
        return decider
      })
      commit('setConfig', payload)
    },
    setForm({commit, state}, payload) {

      payload.config.afterAdminConfirmGroups = payload.config.afterAdminConfirmGroups
        .split(',')
        .map(optInGroup => {
          optInGroup = state.userGroups.find(userGroup => userGroup.id === parseInt(optInGroup))
          return optInGroup
        })
      payload.config.afterAdminConfirmGroups = payload.config.afterAdminConfirmGroups.filter(optInGroup => {
        return typeof (optInGroup) !== 'undefined'
      })
      payload.config.beforeUserConfirmGroups = payload.config.beforeUserConfirmGroups
        .split(',')
        .map(optInGroup => {
          optInGroup = state.userGroups.find(userGroup => userGroup.id === parseInt(optInGroup))
          return optInGroup
        })
      payload.config.beforeUserConfirmGroups = payload.config.beforeUserConfirmGroups.filter(optInGroup => {
        return typeof (optInGroup) !== 'undefined'
      })
      payload.config.afterUserConfirmationGroups = payload.config.afterUserConfirmationGroups
        .split(',')
        .map(optInGroup => {
          optInGroup = state.userGroups.find(userGroup => userGroup.id === parseInt(optInGroup))
          return optInGroup
        })
      payload.config.afterUserConfirmationGroups = payload.config.afterUserConfirmationGroups.filter(optInGroup => {
        return typeof (optInGroup) !== 'undefined'
      })
      payload.fields.map(field => {
        field.config.validators.map(validator => {
          validator.open = false
          return validator
        })
        return field
      })
      commit('setForm', payload)
    },
    setUserGroups({commit}, payload) {
      commit('setUserGroups', payload)
    },
    updateFormTitle({commit}, payload) {
      commit('updateFormTitle', payload)
    },
    updateFormDoubleOptin({commit}, payload) {
      commit('updateFormDoubleOptin', payload)
    },
    updateFormAdminAuth({commit}, payload) {
      commit('updateFormAdminAuth', payload)
    },
    updateFormAdminEmails({commit}, payload) {
      commit('updateFormAdminEmails', payload)
    },
    updateFormUserEnableWithoutAdminAuth({commit}, payload) {
      commit('updateFormUserEnableWithoutAdminAuth', payload)
    },
    updateFormAdminAuthEnableSignals({commit}, payload) {
      commit('updateFormAdminAuthEnableSignals', payload)
    },
    updateFormEnableEmailModerationResultToUser({commit}, payload) {
      commit('updateFormEnableEmailModerationResultToUser', payload)
    },
    updateFormUserEnableWithoutEmailVerification({commit}, payload) {
      commit('updateFormUserEnableWithoutEmailVerification', payload)
    },
    updateFormAfterAdminConfirmGroups({commit}, payload) {
      commit('updateFormAfterAdminConfirmGroups', payload)
    },
    updateFormBeforeUserConfirmGroups({commit}, payload) {
      commit('updateFormBeforeUserConfirmGroups', payload)
    },
    updateFormAfterUserConfirmationGroups({commit}, payload) {
      commit('updateFormAfterUserConfirmationGroups', payload)
    },
    updateFormGeneralSenderEmailAddress({commit}, payload) {
      commit('updateFormGeneralSenderEmailAddress', payload)
    },
    updateFormGeneralSenderEmailName({commit}, payload) {
      commit('updateFormGeneralSenderEmailName', payload)
    },
    updateFormUseEmailAsUsername({commit}, payload) {
      commit('updateFormUseEmailAsUsername', payload)
    },
    updateFormEnableChangeUsername({commit}, payload) {
      commit('updateFormEnableChangeUsername', payload)
    },
    updateFormChangeUsernameAction({commit}, payload) {
      commit('updateFormChangeUsernameAction', payload)
    },
    updateFormDeciders({commit}, payload) {
      commit('updateFormDeciders', payload)
    },
    updateFormEnableApi({commit}, payload) {
      commit('updateFormEnableApi', payload)
    },
    updateFormEnableFeReport({commit}, payload) {
      commit('updateFormEnableFeReport', payload)
    },
    updateFormTestingEmail({commit}, payload) {
      commit('updateFormTestingEmail', payload)
    },
    updateFormEnableTestEnvironment({commit}, payload) {
      commit('updateFormEnableTestEnvironment', payload)
    },
    updateFormEnableModelAnnotationValidation({commit}, payload) {
      commit('updateFormEnableModelAnnotationValidation', payload)
    },
    updateFormType({commit}, payload) {
      commit('updateFormType', payload)
    },
    updateOptInGroupsBefore({commit}, payload) {
      commit('updateOptInGroupsBefore', payload)
    },
    updateOptInGroupsAfter({commit}, payload) {
      commit('updateOptInGroupsAfter', payload)
    },
    setFieldTypeOverwrite({commit}, payload) {
      commit('setFieldTypeOverwrite', payload)
    },
    setFieldHiddenInPreview({commit}, payload) {
      commit('setFieldHiddenInPreview', payload)
    },
    setFieldRegExp({commit}, payload) {
      commit('setFieldRegExp', payload)
    },
    pushNewField({commit, state}, payload) {
      const newField = cloneDeep(state.config.fields.find(field => field.id === payload))
      newField.config = cloneDeep(state.defaultFieldConfig)
      commit('pushNewField', newField)
    },
    pushNewValidator({commit, state, getters}, payload) {
      const value = cloneDeep(state.config.validators.find(validator => validator.id === payload.value))
      const target = getters.getFieldById(payload.target)
      commit('pushNewValidator', {value, target})
    },
    pushNewDecider({commit, state}, payload) {
      const value = cloneDeep(state.config.deciders.find(decider => decider.id === payload))
      commit('pushNewDecider', value)
    },
    setIsFormSelected({commit, dispatch}, payload) {
      dispatch('deselectField')
      dispatch('deselectValidator')
      commit('setIsFormSelected', payload)
    },
    setSelectedFieldId({commit, dispatch}, payload) {
      dispatch('deselectForm')
      dispatch('deselectValidator')
      commit('setSelectedFieldId', payload)
    },
    setSelectedValidatorId({commit}, payload) {
      commit('setSelectedValidatorId', payload)
    },
    setSelectedDeciderId({commit}, payload) {
      commit('setSelectedDeciderId', payload)
    },
    deselectForm({commit}) {
      commit('setIsFormSelected', false)
    },
    deselectField({commit}) {
      commit('setSelectedFieldId', '')
    },
    deselectValidator({commit}) {
      commit('setSelectedValidatorId', '')
    },
    removeField({commit}, payload) {
      commit('removeField', payload)
    },
    removeValidator({commit, getters}, payload) {
      const target = getters.getFieldById(payload.field)
      commit('removeValidator', {target, validator: payload.validator})
    },
    removeDecider({commit}, payload) {
      commit('removeDecider', payload)
    },
    setValidatorParam({commit, getters}, payload) {
      const target = getters.getValidatorByIds(payload.target.fieldId, payload.target.validatorId).params.find(param => param.id === payload.target.paramId)
      commit('setValidatorParam', {target, value: payload.value})
    },
    setDeciderParam({commit, getters}, payload) {
      const target = getters.getDeciderById(payload.target.deciderId).params.find(param => param.id === payload.target.paramId)
      console.log(target)
      commit('setDeciderParam', {target, value: payload.value})
    },
    toggleValidatorOpen({commit, getters}, payload) {
      const target = getters.getValidatorByIds(payload.fieldId, payload.validatorId)

      if (!target.params.length) {
        return
      }
      commit('toggleValidatorOpen', target)
    },
    toggleDeciderOpen({commit, getters}, payload) {
      const target = getters.getDeciderById(payload)
      if (!target.params.length) {
        return
      }
      commit('toggleValidatorOpen', target)
    },
    pushFlashMessage({commit}, payload) {
      commit('pushFlashMessage', payload)
    },
    flushFlashMessages({commit}) {
      commit('flushFlashMessages')
    },
  }
})
