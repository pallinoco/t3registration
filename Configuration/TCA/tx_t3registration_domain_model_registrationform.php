<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$ll = 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:';

return [
    'ctrl' => [
        'title' => $ll . 'tx_t3registration_domain_model_registrationform',
        'descriptionColumn' => 'notes',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'default_sortby' => 'ORDER BY title',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'uid,title',
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden,title'
    ],
    'columns' => [
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'title' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'required,unique,trim',
            ]
        ],
        'fields' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.fields',
            'config' => [
                'type' => 'input',
            ],
        ],
        'double_optin' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.double_optin',
            'config' => [
                'type' => 'check',
            ],
        ],
        'admin_auth' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.admin_auth',
            'config' => [
                'type' => 'check',
            ],
        ],
        'admin_emails' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.admin_emails',
            'config' => [
                'type' => 'text',
            ],
        ],
        'after_admin_confirm_groups' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.after_admin_confirm_groups',
            'config' => [
                'type' => 'text',
            ],
        ],
        'user_enable_without_admin_auth' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.user_enable_without_admin_auth',
            'config' => [
                'type' => 'text',
            ],
        ],
        'admin_auth_enable_signals' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.admin_auth_enable_signals',
            'config' => [
                'type' => 'text',
            ],
        ],
        'enable_email_moderation_result_to_user' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_email_moderation_result_to_user',
            'config' => [
                'type' => 'check',
            ],
        ],
        'user_enable_without_email_verification' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.user_enable_without_email_verification',
            'config' => [
                'type' => 'check',
            ],
        ],
        'before_user_confirm_groups' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.before_user_confirm_groups',
            'config' => [
                'type' => 'text',
            ],
        ],

        'after_user_confirmation_groups' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.before_user_confirm_groups',
            'config' => [
                'type' => 'text',
            ],
        ],

        'general_sender_email_address' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.general_sender_email_address',
            'config' => [
                'type' => 'text',
            ],
        ],
        'general_sender_email_name' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.general_sender_email_name',
            'config' => [
                'type' => 'text',
            ],
        ],
        'use_email_as_username' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.use_email_as_username',
            'config' => [
                'type' => 'check',
            ]
        ],
        'enable_change_username' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_change_username',
            'config' => [
                'type' => 'check',
            ],
        ],
        'change_username_action' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.change_username_action',
            'config' => [
                'type' => 'input',
            ],
        ],
        'deciders' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.deciders',
            'config' => [
                'type' => 'text',
            ],
        ],
        'enable_api' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_api',
            'config' => [
                'type' => 'check',
            ],
        ],
        'enable_fe_report' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_fe_report',
            'config' => [
                'type' => 'check',
            ],
        ],
        'enable_test_environment' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_test_environment',
            'config' => [
                'type' => 'check',
            ],
        ],
        'testing_email' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.testing_email',
            'config' => [
                'type' => 'text',
            ],
        ],
        'enable_email_moderation_result_to_user' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_email_moderation_result_to_user',
            'config' => [
                'type' => 'check',
            ]
        ],
        'enable_model_annotation_validation' => [
            'exclude' => false,
            'label' => $ll . 'tx_t3registration_domain_model_registrationform.enable_model_annotation_validation',
            'config' => [
                'type' => 'check',
            ]
        ]
    ],
    'types' => [
        0 => [
            'showitem' => 'title,fields,type,double_optin,admin_auth,admin_emails,admin_template,after_admin_confirm_groups,user_enable_without_admin_auth,admin_auth_enable_hooks,before_user_confirm_groups,after_user_confirmation_groups,general_sender_email_address,general_sender_email_name,enable_change_username,enable_change_username_confirmation,send_change_username_email_to_old_email_address,template,deciders,enable_api,enable_fe_report,enable_preview,preview_template,testing_email,enable_test_environment,enable_email_moderation_result_to_user,use_email_as_username,enable_model_annotation_validation'
        ]
    ],
    'palettes' => [
        'paletteCore' => [
            'showitem' => 'hidden,',
        ],
    ]
];
