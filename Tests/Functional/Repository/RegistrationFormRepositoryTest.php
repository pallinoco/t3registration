<?php declare(strict_types=1);
/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 31/10/2018$
 */
namespace Pallino\T3registration\Tests\Functional\Repository;

use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\RegistrationFormRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class RegistrationFormRepositoryTest extends FunctionalTestCase
{
    /** @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface The object manager */
    protected $objectManager;
    /** @var  \Pallino\T3registration\Domain\Repository\RegistrationFormRepository */
    protected $registrationFormRepository;
    protected $testExtensionsToLoad = ['typo3conf/ext/t3registration'];

    public function setUp()
    {
        parent::setUp();
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->registrationFormRepository = $this->objectManager->get(RegistrationFormRepository::class);
        $this->importDataSet(__DIR__ . '/../Fixtures/tx_t3registration_domain_model_registrationform.xml');
    }
    /**
     * Test if startingpoint is working
     *
     * @test
     */
    public function testFindRecordsByUid()
    {
        /** @var RegistrationForm $form */
        $form = $this->registrationFormRepository->getFormByUid(1);
        //var_export($form);
        $this->assertEquals('RegistrationForm', $form->getTitle());
        //$this->assertEquals($form->getTitle(), 'findRecordsByUid');
    }
}
