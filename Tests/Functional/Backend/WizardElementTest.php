<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 15/11/2018$
 */
namespace Pallino\T3registration\Tests\Functional\Backend;

use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use Pallino\T3registration\Backend\WizardElement;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class WizardElementTest extends FunctionalTestCase
{
    /** @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface The object manager */
    protected $objectManager;
    protected $testExtensionsToLoad = ['typo3conf/ext/t3registration'];

    public function setUp()
    {
        parent::setUp();
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    }

    public function testWizardClassReturnWizardItem()
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject|WizardElement $wizardElement */
        $wizardElement = $this->getMockBuilder(WizardElement::class)
            ->setMethods(['getTranslationForLabel'])
            ->getMock();
        $wizardElement->method('getTranslationForLabel')
            ->will(
                $this->onConsecutiveCalls(
                    'DummyTitle',
                    'DummyDescription'
                )
            );
        $this->assertEquals(
            [
                'plugins_tx_t3registration_form' => [
                    'iconIdentifier' => 'RegistrationFormWizard',
                    'title' => 'DummyTitle',
                    'description' => 'DummyDescription',
                    'params' => '&defVals[tt_content][CType]=list&&defVals[tt_content][list_type]=t3registration_form'
                ]
            ],
            $wizardElement->proc([])
        );
    }

    public function testWizardIconIsRegistered()
    {
        /** @var  \TYPO3\CMS\Core\Imaging\IconRegistry */
        $iconRegistry = $this->objectManager->get(IconRegistry::class);
        $this->assertTrue(
            $iconRegistry->isRegistered('RegistrationFormWizard')
        );
    }

    protected function tearDown()
    {
        $this->objectManager = null;
        parent::tearDown();
    }
}
