<?php namespace Pallino\T3registration\Tests\Acceptance\Support;

use Pallino\T3registration\Tests\Acceptance\Support\Step\FrontendUser;

class RegistrationCest
{
    public function _before(FrontendUser $I)
    {
        $I->deleteAllEmails();
    }

    /**
     * @param FrontendUser $I
     */
    public function TestPasswordWrongLength(FrontendUser $I)
    {
        $I->amOnPage('/');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'dummy@test.it', 'test');
        $I->see('The length of the given string was not between 5 and 100 characters.');
    }

    /**
     * @param FrontendUser $I
     */
    public function TestPasswordAreDifferent(FrontendUser $I)
    {
        $I->amOnPage('/');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'dummy@test.it', 'test123');
        $I->see('Passwords are different.');
    }

    /**
     * @param FrontendUser $I
     */
    public function TestUsernameIsNotUnique(FrontendUser $I)
    {
        $I->haveInDatabase(
            'fe_users',
            ['pid' => '2', 'username' => 'dummy@test.it', 'email' => 'dummy@test.it', 'disable' => 0]
        );
        $I->amOnPage('/');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'dummy@test.it', 'DummyPassword');
        $I->see('Passwords are different.');
        $I->see('Username is not unique.');
    }

    /**
     * @param FrontendUser $I
     */
    public function testCorrectRegistrationWithDboAndModerationWithoutAdminConfirmation(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=1');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'dummy@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'dummy@test.it');
        $I->see('Registrazione completata');

        $I->checkIfEmailExistsAndCheckEmailText('dummy@test.it', 'Registration confirmation for private area.', 'Confirm my account');
        $I->seeInDatabase('fe_users', ['username' => 'dummy@test.it', 'disable' => 1]);
        $link = $this->getConfirmationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Ora sei un utente registrato');
            $I->seeInDatabase('fe_users', ['username' => 'dummy@test.it', 'disable' => 1]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test without double optin and without moderation
     */
    public function testCorrectRegistrationWithNoDboAndNoModeration(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=3');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'no.control@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'no.control@test.it');
        $I->see('Registrazione completata');
        $I->dontHaveUnreadEmails();
        $I->seeInDatabase('fe_users', ['username' => 'no.control@test.it', 'disable' => 0]);
    }

    /**
     * @param FrontendUser $I
     * Registration test with double opt-in and without moderation
     */
    public function testCorrectRegistrationWithDboAndNoModeration(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=4');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'dbo@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'dbo@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('dbo@test.it', 'Registration confirmation for private area.', 'Confirm my account');
        $I->seeInDatabase('fe_users', ['username' => 'dbo@test.it', 'disable' => 1]);
        $link = $this->getConfirmationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Ora sei un utente registrato');
            $I->seeInDatabase('fe_users', ['username' => 'dbo@test.it', 'disable' => 0]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test without double opt-in but with moderation and admin that accepted and user result email is disable
     */
    public function testCorrectRegistrationWithNoDboAndWithModerationAndAccept(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=5');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'mod@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'mod@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('admin@t3registration.com', 'Moderation new subscriber.', 'Confirm Subscription');
        $I->seeInDatabase('fe_users', ['username' => 'mod@test.it', 'disable' => 1]);
        $link = $this->getConfirmationModerationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Registration successfully approved!');
            $I->fetchEmails();
            $I->accessInboxFor('mod@test.it');
            $I->dontHaveUnreadEmails();
            $I->seeInDatabase('fe_users', ['username' => 'mod@test.it', 'disable' => 0]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test without double opt-in but with moderation and admin that reject and user result email is disable
     */
    public function testCorrectRegistrationWithNoDboAndWithModerationAndReject(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=5');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'mod.reject@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'mod.reject@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('admin@t3registration.com', 'Moderation new subscriber.', 'Confirm Subscription');
        $I->seeInDatabase('fe_users', ['username' => 'mod.reject@test.it', 'disable' => 1]);
        $link = $this->getRejectModerationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Registration successfully rejected!');
            $I->fetchEmails();
            $I->accessInboxFor('mod.reject@test.it');
            $I->dontHaveUnreadEmails();
            $I->seeInDatabase('fe_users', ['username' => 'mod.reject@test.it', 'disable' => 1]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test without double opt-in but with moderation and admin that accept and user result email is enable
     */
    public function testCorrectRegistrationWithNoDboAndWithModerationAndUserIsEnableWithoutUser(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=6');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'mod.reject@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'mod.reject@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('admin@t3registration.com', 'Moderation new subscriber.', 'Confirm Subscription');
        $I->seeInDatabase('fe_users', ['username' => 'mod.reject@test.it', 'disable' => 0]);
        $link = $this->getConfirmationModerationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Registration successfully approved!');
            $I->fetchEmails();
            $I->accessInboxFor('mod.reject@test.it');
            $I->haveEmails();
            $I->haveUnreadEmails();
            $I->seeInDatabase('fe_users', ['username' => 'mod.reject@test.it', 'disable' => 0]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test with double opt-in and moderation. Admin accept before user
     */
    public function testCorrectRegistrationWithModerationAndDoubleOptinAndAdminAcceptBeforeUser(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=7');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'complete@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'complete@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('admin@t3registration.com', 'Moderation new subscriber.', 'Confirm Subscription');
        $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]);
        $link = $this->getConfirmationModerationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Registration successfully approved!');
            $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]); // disable because of double opt-in not confirmed
        }
        $I->checkIfEmailExistsAndCheckEmailText('complete@test.it', 'Registration confirmation for private area.', 'Confirm my account');
        $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]);
        $link = $this->getConfirmationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Ora sei un utente registrato');
            $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 0]);
        }
    }

    /**
     * @param FrontendUser $I
     * Registration test with double opt-in and moderation. User accept before admin
     */
    public function testCorrectRegistrationWithModerationAndDoubleOptinAndUserAcceptBeforeAdmin(FrontendUser $I)
    {
        $I->amOnPage('/index.php?id=7');
        $I->compileFormAndClickSave('DummyLastName', 'DummyFirstName', 'complete@test.it', 'DummyPassword', 'DummyPassword');
        $I->ISeeCorrectPreviewAndClickSave('DummyLastName', 'DummyFirstName', 'complete@test.it');
        $I->see('Registrazione completata');
        $I->checkIfEmailExistsAndCheckEmailText('complete@test.it', 'Registration confirmation for private area.', 'Confirm my account');
        $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]);
        $link = $this->getConfirmationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Ora sei un utente registrato');
            $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]);
        }
        $I->checkIfEmailExistsAndCheckEmailText('admin@t3registration.com', 'Moderation new subscriber.', 'Confirm Subscription');
        $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 1]);
        $link = $this->getConfirmationModerationLink($I->grabBodyFromEmail());
        if ($link == '') {
            $I->assertTrue(false);
        } else {
            $I->amOnUrl($link);
            $I->see('Registration successfully approved!');
            $I->seeInDatabase('fe_users', ['username' => 'complete@test.it', 'disable' => 0]); // disable because of double opt-in not confirmed
        }
    }

    /**
     * @param $body
     * @return string
     */
    protected function getConfirmationLink($body)
    {
        $crawler = new \Symfony\Component\DomCrawler\Crawler($body);
        return html_entity_decode($crawler->filterXPath('//a[@id = \'confirmationLink\']')->attr('href'));
    }

    /**
     * @param $body
     * @return string
     */
    protected function getConfirmationModerationLink($body)
    {
        $crawler = new \Symfony\Component\DomCrawler\Crawler($body);
        return html_entity_decode($crawler->filterXPath('//a[@id = \'confirmationModerationLink\']')->attr('href'));
    }

    /**
     * @param $body
     * @return string
     */
    protected function getRejectModerationLink($body)
    {
        $crawler = new \Symfony\Component\DomCrawler\Crawler($body);
        return html_entity_decode($crawler->filterXPath('//a[@id = \'rejectModerationLink\']')->attr('href'));
    }
}
