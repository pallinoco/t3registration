<?php
declare(strict_types=1);

namespace Pallino\T3registration\Tests\Acceptance\Support\Step;

use Pallino\T3registration\Tests\Acceptance\Support\FrontendTester;

class FrontendUser extends FrontendTester
{

    /**
     * @param $lastName
     * @param $firstName
     * @param $email
     * @param $password1
     * @param string $password2
     */
    public function compileFormAndClickSave($lastName, $firstName, $email, $password1, $password2 = '')
    {
        $I = $this;
        $I->fillField('input#lastName', $lastName);
        $I->fillField('input#firstName', $firstName);
        $I->fillField('input#email', $email);
        $I->fillField('input#password', $password1);
        $I->fillField('input#passwordTwice', $password2);
        $I->click('#create');
    }

    /**
     * @param $lastName
     * @param $firstName
     * @param $email
     */
    public function ISeeCorrectPreviewAndClickSave($lastName, $firstName, $email)
    {
        $I = $this;
        $I->see($lastName);
        $I->see($firstName);
        $I->see($email);
        $I->dontSee('Username is not unique.');
        $I->click('#save');
    }

    /**
     * @param $mailBox
     * @param $subjectToCheck
     * @param $textToCheck
     */
    public function checkIfEmailExistsAndCheckEmailText($mailBox, $subjectToCheck, $textToCheck)
    {
        $I = $this;
        $I->fetchEmails();
        $I->accessInboxFor($mailBox);
        $I->haveEmails();
        $I->haveUnreadEmails();
        $I->openNextUnreadEmail();
        $I->seeInOpenedEmailSubject($subjectToCheck);
        $I->seeInOpenedEmailBody($textToCheck);
        $I->seeInOpenedEmailRecipients($mailBox);
    }
}
