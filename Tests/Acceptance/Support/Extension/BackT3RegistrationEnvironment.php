<?php declare(strict_types=1);

namespace Pallino\T3registration\Tests\Acceptance\Support\Extension;

use TYPO3\TestingFramework\Core\Acceptance\Extension\BackendEnvironment;

class BackT3RegistrationEnvironment extends BackendEnvironment
{
    /**
     * Load a list of core extensions and styleguide
     *
     * @var array
     */
    protected $localConfig = [
        'coreExtensionsToLoad' => [
            'core',
            'extbase',
            'fluid',
            'backend',
            'about',
            'install',
            'frontend',
            'recordlist',
            'rsa',
            'saltedpasswords',
            'felogin',
            'fluid_styled_content'
        ],
        'testExtensionsToLoad' => [
            'typo3conf/ext/t3registration'
        ],
        'xmlDatabaseFixtures' => [
            'PACKAGE:typo3/testing-framework/Resources/Core/Acceptance/Fixtures/be_users.xml',
            'PACKAGE:typo3/testing-framework/Resources/Core/Acceptance/Fixtures/be_sessions.xml',
            'PACKAGE:typo3/testing-framework/Resources/Core/Acceptance/Fixtures/be_groups.xml',
            'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/pages.xml',
            //'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/fe_groups.xml',
            'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/sys_file_storage.xml',
            'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/sys_template.xml',
            'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/tt_content.xml',
            'EXT:t3registration/Tests/Acceptance/Fixtures/Tables/tx_t3registration_domain_model_registrationform.xml',
        ],
        'configurationToUseInTestInstance' => [
            'SYS' => ['displayErrors' => true, 'devIPmask' => '*'],
            'MAIL' => ['transport' => 'smtp', 'transport_smtp_server' => 'mailhog:1025' ]
        ],
    ];
}
