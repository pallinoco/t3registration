<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 19/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Mail;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Mail\AbstractMailProcess;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Mail\MailMessage;

class AbstractMailProcessTest extends UnitTestCase
{
    public function testEmailProcess()
    {
        $user = new FrontendUser();
        $user->setEmail('dummy@dummy.it');
        $user->setName('MyFakeName');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['getGeneralSenderEmailAddress', 'getGeneralSenderEmailName'])
            ->getMock();
        $registrationForm->method('getGeneralSenderEmailAddress')->will(
            $this->returnValue(
                'dummysender@sender.it'
            )
        );
        $registrationForm->method('getGeneralSenderEmailName')->will(
            $this->returnValue(
                'Dummy Sender'
            )
        );
        /** @var MockObject|MailMessage $mailMessage */
        $mailMessage = $this->getMockBuilder(MailMessage::class)
            ->setMethods(['setSubject'])
            ->getMock();
        /** @var AbstractMailProcess|MockObject|AccessibleMockObjectInterface $mailProcess */
        $mailProcess = $this->getAccessibleMockForAbstractClass(
            AbstractMailProcess::class,
            [$user, $registrationForm],
            '',
            false,
            false,
            true,
            ['getEmailBody', 'getTranslationKey']
        );
        $mailProcess->_set('frontendUser', $user);
        $mailProcess->_set('registrationForm', $registrationForm);
        $mailProcess->_set('mailMessage', $mailMessage);
        $mailProcess->_set('receiver', [
            'address' => $user->getEmail(),
            'name' => $user->getName()
        ]);
        $mailProcess->expects($this->once())
            ->method('getEmailBody')
            ->will($this->returnValue(''));
        $mailProcess->expects($this->any())
            ->method('getTranslationKey')
            ->will($this->returnValue('DummyLabel'));
        $mailProcess->_call('setSender');
        $mailProcess->_call('setReceiver');
        $mailProcess->_call('prepareEmail', 'dummy');
        $this->assertEquals(['dummysender@sender.it' => 'Dummy Sender'], $mailMessage->getFrom());
        $this->assertEquals([$user->getEmail() => $user->getName()], $mailMessage->getTo());
    }

    /**
     * @expectedException Exception
     */
    public function testThrowExceptionInCaseOfEmptyReceiver()
    {
        $user = new FrontendUser();
        $user->setEmail('dummy@dummy.it');
        $user->setName('MyFakeName');

        /** @var MockObject|MailMessage $mailMessage */
        $mailMessage = $this->getMockBuilder(MailMessage::class)
            ->setMethods(['setSubject'])
            ->getMock();
        /** @var AbstractMailProcess|MockObject|AccessibleMockObjectInterface $mailProcess */
        $mailProcess = $this->getAccessibleMockForAbstractClass(
            AbstractMailProcess::class,
            [$user],
            '',
            false,
            false,
            true,
            ['getEmailBody', 'getTranslationKey']
        );
        $mailProcess->_set('mailMessage', $mailMessage);
        $mailProcess->_set('receiver', [
            'address' => $user->getEmail()
        ]);
        $mailProcess->_call('setReceiver');
    }
}
