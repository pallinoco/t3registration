<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 24/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Validator;

use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Validator\UsernameUniqueInPidValidator;
use Pallino\T3registration\Validator\UsernameUniqueValidator;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;

class UsernameUniqueInPidValidatorTest extends UnitTestCase
{

    /** @var UsernameUniqueValidator|MockObject */
    protected $usernameUniqueInPidValidator;
    /** @var FrontendUserRepository|MockObject */
    protected $frontendUserRepository;

    protected function setUp()
    {
        $this->frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->setMethods(['isUsernameUniqueInPid'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->usernameUniqueInPidValidator = $this->getAccessibleMock(
            UsernameUniqueInPidValidator::class,
            ['translateErrorMessage'],
            [['pid' => '1']]
        );
    }

    protected function tearDown()
    {
        $this->frontendUserRepository = null;
        $this->usernameUniqueInPidValidator = null;
    }

    protected function setUsernameUniqueInPid(string $username, int $pid, $unique = true)
    {
        $this->frontendUserRepository->expects(self::once())
            ->method('isUsernameUniqueInPid')
            ->with($this->equalTo($username), $this->equalTo($pid))
            ->will(
                self::returnValue($unique)
            );
        $this->usernameUniqueInPidValidator->injectFrontendUserRepository($this->frontendUserRepository);
    }

    public function testUsernameIsEmpty()
    {
        $pid = '1';
        $username = '';
        $this->setUsernameUniqueInPid($username, $pid, true);
        /** @var Result $error */
        $error = $this->usernameUniqueInPidValidator->validate('');
        $this->assertTrue($error->hasErrors());
        $this->assertEquals(200200300, $error->getFirstError()->getCode());
    }

    public function testUsernameIsNotUniqueInPid()
    {
        $pid = 1;
        $username = 'dummyUsername';
        $this->setUsernameUniqueInPid($username, $pid, false);
        /** @var Result $error */
        $error = $this->usernameUniqueInPidValidator->validate($username);
        $this->assertTrue($error->hasErrors());
        $this->assertEquals(200200400, $error->getFirstError()->getCode());
    }

    public function testUsernameIsUniqueInPid()
    {
        $pid = 1;
        $username = 'dummyUsername';
        $this->setUsernameUniqueInPid($username, $pid, true);
        /** @var Result $error */
        $error = $this->usernameUniqueInPidValidator->validate($username);
        $this->assertFalse($error->hasErrors());
    }
}
