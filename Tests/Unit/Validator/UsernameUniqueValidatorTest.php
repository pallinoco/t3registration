<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 24/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Validator;

use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Validator\UsernameUniqueValidator;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;

class UsernameUniqueValidatorTest extends \Nimut\TestingFramework\TestCase\UnitTestCase
{
    /** @var UsernameUniqueValidator|MockObject */
    protected $usernameUniqueValidator;
    /** @var FrontendUserRepository|MockObject */
    protected $frontendUserRepository;

    protected function setUp()
    {
        $this->frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->setMethods(['isUsernameUnique'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->usernameUniqueValidator = $this->getAccessibleMock(
            UsernameUniqueValidator::class,
            ['translateErrorMessage'],
            [],
            '',
            false
        );
    }

    protected function tearDown()
    {
        $this->frontendUserRepository = null;
        $this->usernameUniqueValidator = null;
    }

    protected function setUsernameUnique($unique = true)
    {
        $this->frontendUserRepository->expects(self::once())
            ->method('isUsernameUnique')
            ->will(
                self::returnValue($unique)
            );
        $this->usernameUniqueValidator->injectFrontendUserRepository($this->frontendUserRepository);
    }

    public function testUsernameIsEmpty()
    {
        $this->setUsernameUnique(true);
        /** @var Result $error */
        $error = $this->usernameUniqueValidator->validate('');
        $this->assertTrue($error->hasErrors());
        $this->assertEquals(200300300, $error->getFirstError()->getCode());
    }

    public function testUsernameIsNotUnique()
    {
        $this->setUsernameUnique(false);
        /** @var Result $error */
        $error = $this->usernameUniqueValidator->validate('dummyUsername');
        $this->assertTrue($error->hasErrors());
        $this->assertEquals(200300400, $error->getFirstError()->getCode());
    }

    public function testUsernameIsUnique()
    {
        $this->setUsernameUnique(true);
        /** @var Result $error */
        $error = $this->usernameUniqueValidator->validate('dummyUsername');
        $this->assertFalse($error->hasErrors());
    }
}
