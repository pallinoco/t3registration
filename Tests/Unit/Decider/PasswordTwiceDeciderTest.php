<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 05/12/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Decider;

use Pallino\T3registration\Decider\DeciderInterface;
use Pallino\T3registration\Decider\PasswordTwiceDecider;
use Pallino\T3registration\Domain\Model\FrontendUser;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PasswordTwiceDeciderTest extends TestCase
{
    /** @var DeciderInterface|MockObject */
    protected $twicePasswordDecider;

    public function testValidateTwicePasswordAndPasswordAreDifferent()
    {
        $arguments = ['passwordTwice' => 'testPasswordInError'];
        $this->twicePasswordDecider->setFrontendUserArguments($arguments);
        $this->twicePasswordDecider->expects(self::once())
            ->method('translateErrorMessage')
            ->with(self::equalTo('decider.twicePassword.notEqual'), self::equalTo('t3registration'));
        $user = new FrontendUser('dummyUsername', 'testPassword');
        $result = $this->twicePasswordDecider->validate($user);
        $this->assertTrue($result->hasErrors());
        $this->assertEquals(333000102, $result->getFirstError()->getCode());
    }

    public function testValidateTwicePasswordAndPasswordAreEqual()
    {
        $arguments = ['passwordTwice' => 'testPassword'];
        $this->twicePasswordDecider->setFrontendUserArguments($arguments);
        $this->twicePasswordDecider->expects(self::never())
            ->method('translateErrorMessage');
        $user = new FrontendUser('dummyUsername', 'testPassword');
        $result = $this->twicePasswordDecider->validate($user);
        $this->assertFalse($result->hasErrors());
    }

    protected function setUp()
    {
        $this->twicePasswordDecider = $this->getMockBuilder(PasswordTwiceDecider::class)
            ->setMethods(['translateErrorMessage'])
            ->getMock();
    }
}
