<?php
/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 23/10/2018$
 */
namespace Pallino\T3registration\Tests\Cache;

use Pallino\T3registration\Cache\CacheBuilder;
use PHPUnit\Framework\TestCase;

class CacheBuilderTest extends TestCase
{
    public function testReBuildInCaseOfNoExtendingClass()
    {
        $extendingClass = [];
        /** @var \PHPUnit_Framework_MockObject_MockObject|CacheBuilder $cacheBuilder */
        $cacheBuilder = $this->getMockBuilder(CacheBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'getModelFilepathByExtensionKey',
                    'getCodeFromFilepath',
                    'getExtendingClassArray',
                    'saveCodeIntoFileCache'
                ]
            )
            ->getMock();
        $cacheBuilder->expects($this->exactly(1))
            ->method('getModelFilepathByExtensionKey')
            ->withConsecutive(
                [
                    $this->equalTo('t3registration')
                ]
            );
        $cacheBuilder->method('getCodeFromFilepath')
            ->will(
                $this->returnValue("<?php\nclass FrontendUser\n{\nprivate function getData();\n}")
            );
        $cacheBuilder->method('getExtendingClassArray')
            ->will($this->returnValue($extendingClass));
        $cacheBuilder->expects($this->once())
            ->method('saveCodeIntoFileCache')
            ->with($this->stringContains("\n}"));
        $cacheBuilder->reBuild();
    }

    public function testReBuildInCaseOfAtLeastOneExtendingClass()
    {
        $extendingClass = ['dummy'];
        /** @var \PHPUnit_Framework_MockObject_MockObject|CacheBuilder $cacheBuilder */
        $cacheBuilder = $this->getMockBuilder(CacheBuilder::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'getModelFilepathByExtensionKey',
                    'getCodeFromFilepath',
                    'getExtendingClassArray',
                    'saveCodeIntoFileCache'
                ]
            )
            ->getMock();
        $cacheBuilder->expects($this->exactly(2))
            ->method('getModelFilepathByExtensionKey')
            ->withConsecutive(
                [
                    $this->equalTo('t3registration')
                ],
                [
                    $this->equalTo('dummy')
                ]
            );
        $cacheBuilder->expects($this->atLeast(2))
            ->method('getCodeFromFilepath')
            ->willReturnOnConsecutiveCalls(
                $this->returnValue("<?php\nclass FrontendUser\n{\nprivate function getData();\n}"),
                $this->returnValue("<?php\nclass dummy\n{\nprivate \$variable;\n}")
            );
        $cacheBuilder->method('getExtendingClassArray')
            ->will($this->returnValue($extendingClass));
        $cacheBuilder->expects($this->once())
            ->method('saveCodeIntoFileCache')
            ->with($this->stringContains('$variable'));
        $cacheBuilder->reBuild();
    }
}
