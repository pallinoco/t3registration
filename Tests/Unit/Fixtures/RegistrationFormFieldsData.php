<?php declare(strict_types=1);
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

return '
[{
        "id": "last_name",
        "name": "Last name",
        "config": {
            "validators": [{
                "id": "_TYPO3_CMS_Extbase_Validation_Validator_NumberRangeValidator",
                "name": "NumberRangeValidator",
                "params": [{
                    "id": "maximum",
                    "name": "Maximum",
                    "value": "100"
                },
                {
                    "id": "minimum",
                    "name": "Minimum",
                    "value": "5"
                }]
            },
            {
                "id": "_TYPO3_CMS_Extbase_Validation_Validator_StringLengthValidator",
                "name": "StringLengthValidator",
                "params": [{
                    "id": "maximum",
                    "name": "Maximum",
                    "value": "10"
                },
                {
                    "id": "minimum",
                    "name": "Minimum",
                    "value": "1"
                }]
            }],
            "hiddenInPreview": true,
            "hiddenInEdit": false
        }
    }, {
        "id": "email",
        "name": "Email",
        "config": {
            "typeOverwrite": true,
            "validators": [{
                "id": "_TYPO3_CMS_Extbase_Validation_Validator_EmailAddressValidator",
                "name": "EmailAddressValidator",
                "params": []
            }],
            "hiddenInPreview": false,
            "hiddenInEdit": false
        }
    }]
';
