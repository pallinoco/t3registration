<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 15/11/2018$
 */

namespace Pallino\T3registration\Tests\Unit\Controller;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Controller\RegistrationController;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Process\ProcessInterface;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Error;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;
use TYPO3\CMS\Saltedpasswords\Salt\SaltInterface;

class RegistrationControllerTest extends UnitTestCase
{
    public function testRedirectOnValidationError()
    {
        /** @var ReflectionService|\PHPUnit_Framework_MockObject_MockObject $reflectionService */
        $reflectionService = $this->getMockBuilder(\TYPO3\CMS\Extbase\Reflection\ReflectionService::class)
            ->disableOriginalConstructor()
            ->setMethods(['getMethodTagsValues'])
            ->getMock();
//        $reflectionService = $this->getAccessibleMock(\TYPO3\CMS\Extbase\Reflection\ReflectionService::class,['getMethodTagsValues']);
        $reflectionService->method('getMethodTagsValues')
            ->will($this->returnValue([]));
        /** @var \PHPUnit_Framework_MockObject_MockObject|AccessibleMockObjectInterface|RegistrationController $controller */
        $controller = $this->getAccessibleMock(\Pallino\T3registration\Controller\RegistrationController::class, [
            'getValidationResults',
            'forwardToReferringRequest',
            'clearCacheOnError',
            'getFlattenedValidationErrorMessage',
            'getProcess'
        ]);
        $controller->injectReflectionService($reflectionService);
        $fakeSubResult = new Result();
        $fakeSubResult->addError(new Error('general error', 104563));
        $fakeResult = new Result();
        $fakeResult->forProperty('username')->merge($fakeSubResult);
        $processObject = $this->getMockForAbstractClass(
            ProcessInterface::class,
            [],
            '',
            true,
            true,
            true,
            ['getValidationResults', 'getUser', 'getResult', 'getProcess', 'close']
        );
        $controller->expects(self::once())
            ->method('getProcess')
            ->will(self::returnValue($processObject));
        $processObject->method('getValidationResults')->will($this->returnValue($fakeResult));
        $controller->expects($this->once())->method('forwardToReferringRequest');
        $controller->expects($this->once())->method('clearCacheOnError');
        $controller->expects($this->once())->method('getFlattenedValidationErrorMessage');
        $controller->_set('arguments', []);
        $controller->_call('callActionMethod');
    }


    public function testPasswordEncryptedFailForLackOfArgumentUser()
    {
        $user = new FrontendUser();
        $user->setPassword('DummyPassword');
        /** @var Request|MockObject $request */
        $request = $this->getMockBuilder(Request::class)
            ->setMethods(['hasArgument', 'getArgument'])
            ->getMock();
        $request->expects(self::once())
            ->method('hasArgument')
            ->will(self::returnValue(false));
        /** @var RegistrationController|AccessibleMockObjectInterface|MockObject $registrationController */
        $registrationController = $this->getAccessibleMock(
            RegistrationController::class,
            ['checkForSaltedExtensionInstalled', 'getSaltInstance']
        );
        $registrationController->_set('request', $request);
        $user = $registrationController->setPassword($user);
        $this->assertEquals('DummyPassword', $user->getPassword());
    }

    public function testPasswordEncryptedCorrectly()
    {
        $user = new FrontendUser();
        $user->setPassword('DummyPassword');
        /** @var Request|MockObject $request */
        $request = $this->getMockBuilder(Request::class)
            ->setMethods(['hasArgument', 'getArgument'])
            ->getMock();
        $request->expects(self::once())
            ->method('hasArgument')
            ->will(self::returnValue(true));
        $request->expects(self::once())
            ->method('getArgument')
            ->with(self::equalTo('user'))
            ->will(self::returnValue(['password' => 'DummyPassword']));
        $saltObject = $this->getMockForAbstractClass(
            SaltInterface::class,
            [],
            '',
            false,
            false,
            true,
            ['getHashedPassword']
        );
        /** @var RegistrationController|AccessibleMockObjectInterface|MockObject $registrationController */
        $registrationController = $this->getAccessibleMock(
            RegistrationController::class,
            ['checkForSaltedExtensionInstalled', 'getSaltInstance']
        );
        $registrationController->_set('request', $request);
        $registrationController->expects(self::once())
            ->method('checkForSaltedExtensionInstalled')
            ->will(
                self::returnValue(true)
            );
        $registrationController->expects(self::once())
            ->method('getSaltInstance')
            ->will(
                self::returnValue($saltObject)
            );
        $saltObject->expects(self::once())
            ->method('getHashedPassword')
            ->with(self::equalTo('DummyPassword'))
            ->will(
                self::returnValue('SHA1')
            );
        $user = $registrationController->setPassword($user);
        $this->assertEquals('SHA1', $user->getPassword());
    }
}
