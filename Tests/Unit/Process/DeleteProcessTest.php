<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 03/12/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Process\DeleteProcess;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class DeleteProcessTest extends UnitTestCase
{
    /** @var AccessibleMockObjectInterface|MockObject|DeleteProcess */
    protected $deleteProcessTest;

    /** @var Request */
    protected $request;

    /** @var Response */
    protected $response;
    /** @var TokenAuthenticationInterface|AccessibleMockObjectInterface|MockObject */
    protected $authenticationToken;
    /** @var FrontendUserRepository|MockObject */
    protected $frontendUserRepository;

    public function testProcessFailIfUserIsNotLogged()
    {
        $this->deleteProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(false));
        $this->deleteProcessTest->expects(self::once())
            ->method('redirect')
            ->with(self::equalTo('accessDenied'));
        $this->assertFalse($this->deleteProcessTest->process($this->request, $this->response));
        $this->assertTrue($this->deleteProcessTest->getResult()->hasErrors());
        $this->deleteProcessTest->close();
    }

    public function testSendDeleteRequestToUser()
    {
        $token = '11111-22222-33333';
        $this->authenticationToken->method('getToken')
            ->will(self::returnValue($token));
        $user = new FrontendUser('dummyUsername');
        $user->setDisable(true);
        $this->deleteProcessTest->_set('user', $user);
        $this->deleteProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(true));
        $this->deleteProcessTest->expects(self::once())
            ->method('getCurrentPageId')
            ->will(self::returnValue(1));
        $expectedUser = new FrontendUser('dummyUsername');
        $expectedUser->setDisable(true);
        $expectedUser->setUserAuthenticationToken($token);
        $this->deleteProcessTest->expects(self::once())
            ->method('redirect')
            ->with(self::equalTo('deleteProcessEmailSent'), self::equalTo(['user' => $user]));
        $this->asserttrue($this->deleteProcessTest->process($this->request, $this->response));
        $this->assertEquals($expectedUser, $this->deleteProcessTest->getUser());
        $this->deleteProcessTest->close();
    }

    protected function setUp()
    {
        $this->request = new Request();
        $this->request->setControllerName('Registration');
        $this->response = new Response();
        $this->response->setContent('dummyContent');
        $userFromPersistence = new FrontendUser('dummyUsername', 'dummyPassword');
        $userFromPersistence->setEmail('dummyUsername');
        $persistenceManager = $this->getMockBuilder(PersistenceManager::class)
            ->setMethods(['persistAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUserFromToken'])
            ->getMock();
        $this->authenticationToken = $this->getMockForAbstractClass(TokenAuthenticationInterface::class, [], '', true, true, true, ['isValid']);
        $this->deleteProcessTest = $this->getAccessibleMock(DeleteProcess::class, [
            'redirect',
            'forward',
            'getValidationRuleManager',
            'isDisableAnnotationValidation',
            'emitMethodSignal',
            'getMailProcessObject',
            'canGetUserFromLogged',
            'getCurrentPageId'
        ], [], '', false);
        $mailProcessObject = $this->getMockBuilder(\Pallino\T3registration\Mail\DeleteProcess::class)
            ->disableOriginalConstructor()
            ->setMethods(['process'])
            ->getMock();
        $this->deleteProcessTest->expects(self::any())
            ->method('getMailProcessObject')
            ->will(self::returnValue($mailProcessObject));
        $this->deleteProcessTest->_set('processResult', new Result());
        $this->deleteProcessTest->_set('persistenceManager', $persistenceManager);
        $this->deleteProcessTest->_set('tokenAuthentication', $this->authenticationToken);
        $this->deleteProcessTest->_set('frontendUserRepository', $this->frontendUserRepository);
    }
}
