<?php

namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Mail\UserNotifyRejectModerationProcess;
use Pallino\T3registration\Process\ModerationRejectProcess;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Response;

class ModerationRejectProcessTest extends UnitTestCase
{


    public function testProcessWithUserEmailAndSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationRejectProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyRejectModerationProcess = $this->getAccessibleMock(UserNotifyRejectModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(true);
        $registrationForm->setAdminAuthEnableSignals(true);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyRejectModerationProcess);
        $userNotifyRejectModerationProcess->expects(self::once())->method('process');
        $moderationConfirmProcess->expects(self::once())->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

    public function testProcessWithUserEmailButNoSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationRejectProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyRejectModerationProcess = $this->getAccessibleMock(UserNotifyRejectModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(true);
        $registrationForm->setAdminAuthEnableSignals(false);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyRejectModerationProcess);
        $userNotifyRejectModerationProcess->expects(self::once())->method('process');
        $moderationConfirmProcess->expects($this->exactly(0))->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

    public function testProcessWithNoUserEmailButWithSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationRejectProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyRejectModerationProcess = $this->getAccessibleMock(UserNotifyRejectModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(false);
        $registrationForm->setAdminAuthEnableSignals(true);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyRejectModerationProcess);
        $userNotifyRejectModerationProcess->expects($this->exactly(0))->method('process');
        $moderationConfirmProcess->expects(self::once())->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }


    public function testProcessWithNoUserEmailAndNoSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationRejectProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyRejectModerationProcess = $this->getAccessibleMock(UserNotifyRejectModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(false);
        $registrationForm->setAdminAuthEnableSignals(false);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyRejectModerationProcess);
        $userNotifyRejectModerationProcess->expects($this->exactly(0))->method('process');
        $moderationConfirmProcess->expects($this->exactly(0))->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

}
