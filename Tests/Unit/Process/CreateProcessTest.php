<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\FrontendUserGroup;
use Pallino\T3registration\Mail\DoubleOptInProcess;
use Pallino\T3registration\Mail\ModerationProcess;
use Pallino\T3registration\Process\CreateProcess;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class CreateProcessTest extends \Nimut\TestingFramework\TestCase\UnitTestCase
{
    public function testUserClickToEditButtonInPreview()
    {
        $request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(['hasArgument'])
            ->getMock();
        /** @var CreateProcess|AccessibleMockObjectInterface|MockObject $process */
        $process = $this->getAccessibleMock(CreateProcess::class, [
            'getValidationResults',
            'getFrontendUserGroups',
            'getDoubleOptinObject',
            'emitMethodSignal',
            'getCurrentPageId',
            'forward'
        ], [], '', false);
        $process->_set('processResult', new Result());
        $process->_set('request', $request);
        $persistenceManager = $this->getMockBuilder(PersistenceManager::class)
            ->setMethods(['persistAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $process->_set('persistenceManager', $persistenceManager);
        $controllerSettings = ['backToEditFromPreviewButtonName' => 'Edit'];
        $process->_set('controllerSettings', $controllerSettings);
        $request->method('hasArgument')
            ->with(self::equalTo('Edit'))
            ->will(self::returnValue(true));
        $this->assertFalse($process->process($request, new Response()));
        $process->expects(self::once())
            ->method('forward')
            ->with(self::equalTo('new'));
        $process->close();
    }

    public function testProcessReturnFailWhenUsernameIsNotSet()
    {
        $user = new FrontendUser();
        $process = $this->prepareMockForUsernameValidation($user, false);
        $this->assertFalse($process->process(new Request(), new Response()));
        $processResult = $process->getResult();
        $this->assertEquals($processResult->getFirstError()->getCode(), CreateProcess::USERNAME_COULD_BE_EMPTY);
    }

    public function testProcessReturnFailWhenEmailIsNotSet()
    {
        $user = new FrontendUser();
        $process = $this->prepareMockForUsernameValidation($user, true);
        $this->assertFalse($process->process(new Request(), new Response()));
        $processResult = $process->getResult();
        $this->assertEquals($processResult->getFirstError()->getCode(), CreateProcess::USERNAME_COULD_BE_EMPTY);
    }

    public function testProcessAttachCorrectUserGroupToUser()
    {
        $user = new FrontendUser();
        $user->setUsername('dummy-username');
        /** @var CreateProcess|AccessibleMockObjectInterface|MockObject $process */
        $process = $this->prepareMockForUsernameValidation($user, false, true);
        $process->expects(self::once())
            ->method('getCurrentPageId')
            ->will(self::returnValue(1));
        $this->assertTrue($process->process(new Request(), new Response()));
        $user = $process->getUser();
        $token = '11111-22222-33333';
        $this->assertEquals($token, $user->getUserAuthenticationToken());
        $this->assertTrue($user->isDisable());
    }

    protected function prepareMockForUsernameValidation(
        FrontendUser $user,
        bool $isUseEmailAsUsername,
        bool $doubleOptin = false,
        bool $moderation = false
    ): MockObject {
        $usergroup = new FrontendUserGroup();
        $usergroup->setTitle('dummy Group');
        $usergroup->_setProperty('__ident', 1);
        $processResult = new Result();
        /** ---------------MOCKING-------------- */
        /** @var CreateProcess|AccessibleMockObjectInterface|MockObject $process */
        $process = $this->getAccessibleMock(CreateProcess::class, [
            'getValidationResults',
            'getFrontendUserGroups',
            'getDoubleOptinObject',
            'getModerationObject',
            'emitMethodSignal',
            'getCurrentPageId'
        ], [], '', false);
        $process->_set('processResult', $processResult);
        $process->_set('user', $user);
        $doubleOptinObject = $this->getMockBuilder(DoubleOptInProcess::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $moderationObject = $this->getMockBuilder(ModerationProcess::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm = $this->getMockBuilder(CreateProcess::class)
            ->disableOriginalConstructor()
            ->setMethods(['isUseEmailAsUsername', 'getBeforeUserConfirmGroups', 'isDoubleOptin', 'isAdminAuth', 'isUserEnableWithoutAdminAuth', 'isAdminAuthEnableSignals', 'isUserEnableWithoutEmailVerification'])
            ->getMock();
        $registrationForm->expects(self::atLeast(1))
            ->method('isUseEmailAsUsername')
            ->will(self::returnValue($isUseEmailAsUsername));
        $process->expects(self::any())
            ->method('getValidationResults')
            ->will(self::returnValue(new Result()));
        $process->_set('registrationForm', $registrationForm);
        $process->expects(self::any())
            ->method('getDoubleOptinObject')
            ->will(self::returnValue($doubleOptinObject));
        $process->expects(self::any())
            ->method('getModerationObject')
            ->will(self::returnValue($moderationObject));
        if ($doubleOptin) {
            $token = '11111-22222-33333';
            /** @var TokenAuthenticationInterface|MockObject $authenticationToken */
            $authenticationToken = $this->getMockForAbstractClass(TokenAuthenticationInterface::class, [], '', true, true, true, ['getToken']);
            $authenticationToken->expects(self::once())
                ->method('getToken')
                ->will(
                    self::returnValue($token)
                );
            $process->_set('tokenAuthentication', $authenticationToken);
            $process->expects(self::once())
                ->method('getDoubleOptinObject')
                ->will(self::returnValue($doubleOptinObject));
            $process->expects(self::once())
                ->method('getFrontendUserGroups')
                ->with(self::equalTo([1]))
                ->will(self::returnValue([$usergroup]));
            $registrationForm->expects(self::atLeast(1))
                ->method('getBeforeUserConfirmGroups')
                ->will(self::returnValue('1'));
            $registrationForm->expects(self::atLeast(1))
                ->method('isDoubleOptin')
                ->will(self::returnValue(true));
            $registrationForm->expects(self::atLeast(1))
                ->method('isAdminAuth')
                ->will(self::returnValue(false));
            $registrationForm->method('isUserEnableWithoutAdminAuth')
                ->will(self::returnValue(true));
        }
        if ($moderation) {
            $registrationForm->method('isUserEnableWithoutAdminAuth')
                ->will(self::returnValue(true));
            $registrationForm->method('isAdminAuthEnableSignals')
                ->will(self::returnValue(true));
        }
        return $process;
    }

    public function testModerationProcessCall()
    {
        $user = new FrontendUser();
        $user->setUsername('dummy-username');
        /** @var CreateProcess|AccessibleMockObjectInterface|MockObject $process */
        $process = $this->prepareMockForUsernameValidation($user, false, true, true);
        $process->expects(self::once())
            ->method('getCurrentPageId')
            ->will(self::returnValue(1));
        $this->assertTrue($process->process(new Request(), new Response()));
        $user = $process->getUser();
        $token = '11111-22222-33333';
        $this->assertEquals($token, $user->getUserAuthenticationToken());
        $this->assertTrue($user->isDisable());
    }
}
