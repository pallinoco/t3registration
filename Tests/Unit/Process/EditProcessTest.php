<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Process\EditProcess;
use Pallino\T3registration\Utility\UserLoggedUtility;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;

class EditProcessTest extends UnitTestCase
{
    /** @var AccessibleMockObjectInterface|MockObject|EditProcess */
    protected $editProcessTest;

    protected function setUp()
    {
        $this->editProcessTest = $this->getAccessibleMock(EditProcess::class, ['redirect'], [], '', false);
    }

    public function testUserIsLoggedAndGetFromPersistence()
    {
        $request = new Request();
        $request->setControllerName('Registration');
        $response = new Response();
        $response->setContent('dummyContent');
        $user = new FrontendUser('dummyusername');
        $userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['isUserLogged', 'getLoggedUser', 'isUserLoggedEqualTo'])
            ->getMock();
        $userLoggedUtility->expects(self::once())
            ->method('getLoggedUser')
            ->will($this->returnValue($user));
        $userLoggedUtility->expects(self::once())
            ->method('isUserLogged')
            ->will(self::returnValue(true));
        $this->editProcessTest->_set('user', null);
        $this->editProcessTest->_set('userLoggedUtility', $userLoggedUtility);
        $this->editProcessTest->_set('processResult', new Result());
        $this->assertTrue($this->editProcessTest->process($request, $response));
        $this->assertFalse($this->editProcessTest->getResult()->hasErrors());
    }

    public function testUserIsLoggedButDifferentFromUserPassed()
    {
        $request = new Request();
        $request->setControllerName('Registration');
        $response = new Response();
        $response->setContent('dummyContent');
        $user = new FrontendUser('dummyusername');
        $userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['isUserLogged', 'getLoggedUser', 'isUserLoggedEqualTo'])
            ->getMock();
        $userLoggedUtility->expects(self::once())
            ->method('isUserLoggedEqualTo')
            ->will($this->returnValue(false));
        $this->editProcessTest->_set('user', $user);
        $this->editProcessTest->_set('userLoggedUtility', $userLoggedUtility);
        $this->editProcessTest->_set('processResult', new Result());
        $this->assertFalse($this->editProcessTest->process($request, $response));
        $this->assertTrue($this->editProcessTest->getResult()->hasErrors());
    }

    public function testUserIsNotLoggedAndNotPassed()
    {
        $request = new Request();
        $request->setControllerName('Registration');
        $response = new Response();
        $response->setContent('dummyContent');
        $user = new FrontendUser('dummyusername');
        $userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['isUserLogged', 'getLoggedUser', 'isUserLoggedEqualTo'])
            ->getMock();
        $userLoggedUtility->expects(self::never())
            ->method('isUserLoggedEqualTo');
        $userLoggedUtility->expects(self::once())
            ->method('isUserLogged')
            ->will(self::returnValue(false));
        $this->editProcessTest->_set('user', null);
        $this->editProcessTest->_set('userLoggedUtility', $userLoggedUtility);
        $this->editProcessTest->_set('processResult', new Result());
        $this->assertFalse($this->editProcessTest->process($request, $response));
        $this->assertTrue($this->editProcessTest->getResult()->hasErrors());
    }

    public function testCloseWithError()
    {
        $request = new Request();
        $request->setControllerName('Registration');
        $response = new Response();
        $response->setContent('dummyContent');
        $user = new FrontendUser('dummyusername');
        $userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['isUserLogged', 'getLoggedUser', 'isUserLoggedEqualTo'])
            ->getMock();
        $userLoggedUtility->expects(self::never())
            ->method('isUserLoggedEqualTo');
        $userLoggedUtility->expects(self::once())
            ->method('isUserLogged')
            ->will(self::returnValue(false));
        $this->editProcessTest->_set('user', null);
        $this->editProcessTest->_set('userLoggedUtility', $userLoggedUtility);
        $this->editProcessTest->_set('processResult', new Result());
        $this->assertFalse($this->editProcessTest->process($request, $response));
        $this->assertTrue($this->editProcessTest->getResult()->hasErrors());
        $this->editProcessTest->expects(self::once())
            ->method('redirect')
            ->with(self::equalTo('accessDenied'));
        $this->editProcessTest->close();
    }
}
