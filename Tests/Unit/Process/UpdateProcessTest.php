<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Process\EditProcess;
use Pallino\T3registration\Process\UpdateProcess;
use Pallino\T3registration\Utility\DeciderRuleManager;
use Pallino\T3registration\Utility\ValidationRuleManager;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;

class UpdateProcessTest extends UnitTestCase
{
    /** @var AccessibleMockObjectInterface|MockObject|EditProcess */
    protected $updateProcessTest;

    /** @var Request */
    protected $request;

    /** @var Response */
    protected $response;

    protected function setUp()
    {
        $this->request = new Request();
        $this->request->setControllerName('Registration');
        $this->response = new Response();
        $this->response->setContent('dummyContent');
        $userFromPersistence = new FrontendUser('dummyUsername', 'dummyPassword');
        $userFromPersistence->setEmail('dummyUsername');
        $frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findFreshUserByUid'])
            ->getMock();
        $this->updateProcessTest = $this->getAccessibleMock(UpdateProcess::class, [
            'redirect',
            'forward',
            'canGetUserFromLogged',
            'getValidationRuleManager',
            'getDeciderRuleManager',
            'isDisableAnnotationValidation',
            'emitMethodSignal',
            'startChangeEmailMailProcess'
        ], [], '', false);
        $this->updateProcessTest->_set('processResult', new Result());
        $frontendUserRepository->expects(self::any())
            ->method('findFreshUserByUid')
            ->will(self::returnValue($userFromPersistence));
        $this->updateProcessTest->_set('frontendUserRepository', $frontendUserRepository);
    }

    public function testProcessFailIfUserIsNotLogged()
    {
        $this->updateProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(false));
        $this->updateProcessTest->expects(self::once())
            ->method('redirect')
            ->with(self::equalTo('accessDenied'));
        $this->assertFalse($this->updateProcessTest->process($this->request, $this->response));
        $this->assertTrue($this->updateProcessTest->getResult()->hasErrors());
        $this->updateProcessTest->expects(self::once())
            ->method('redirect')
            ->with(self::equalTo('accessDenied'));
        $this->updateProcessTest->close();
    }

    public function testUserNotChangeUsernameAndPassword()
    {
        $result = new Result();
        $user = new FrontendUser('dummyUsername', '');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['isUseEmailAsUsername'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm->method('isUseEmailAsUsername')
            ->will(self::returnValue(false));
        $this->updateProcessTest->_set('user', $user);
        $this->updateProcessTest->_set('registrationForm', $registrationForm);
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $this->updateProcessTest->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $this->updateProcessTest->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->getValidationResults();
        $this->assertEquals(
            ['password', 'username'],
            array_keys($this->updateProcessTest->_get('excludeFieldFromValidation'))
        );
    }

    public function testUserChangeOnlyUsername()
    {
        $result = new Result();
        $user = new FrontendUser('dummyUsernameChanged', '');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['isUseEmailAsUsername'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm->method('isUseEmailAsUsername')
            ->will(self::returnValue(false));
        $this->updateProcessTest->_set('user', $user);
        $this->updateProcessTest->_set('registrationForm', $registrationForm);
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $this->updateProcessTest->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $this->updateProcessTest->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->getValidationResults();
        $this->assertEquals(['password'], array_keys($this->updateProcessTest->_get('excludeFieldFromValidation')));
    }

    public function testUserChangeEmailWithEmailAsUsernameFlagSet()
    {
        $result = new Result();
        $user = new FrontendUser('', '');
        $user->setEmail('dummyUsernameChanged');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['isUseEmailAsUsername', 'getChangeUsernameAction'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm->method('isUseEmailAsUsername')
            ->will(self::returnValue(true));
        $registrationForm->method('getChangeUsernameAction')
            ->will(self::returnValue(1));
        $this->updateProcessTest->_set('user', $user);
        $this->updateProcessTest->_set('registrationForm', $registrationForm);
        $token = '11111-22222-33333';
        /** @var TokenAuthenticationInterface|MockObject $authenticationToken */
        $authenticationToken = $this->getMockForAbstractClass(TokenAuthenticationInterface::class, [], '', true, true, true, ['getToken']);
        $authenticationToken->expects(self::once())
            ->method('getToken')
            ->will(
                self::returnValue($token)
            );
        $this->updateProcessTest->_set('tokenAuthentication', $authenticationToken);
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->updateProcessTest->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $this->updateProcessTest->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->getValidationResults();
        $this->assertEquals(['password'], array_keys($this->updateProcessTest->_get('excludeFieldFromValidation')));
        $this->updateProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(true));
        $this->updateProcessTest->process($this->request, $this->response);
        $this->assertEquals('dummyUsernameChanged', $this->updateProcessTest->getUser()->getTemporaryEmailChanged());
        $this->assertEquals($token, $this->updateProcessTest->getUser()->getEmailChangeProcessToken());
    }

    public function testUserChangeEmailWithEmailAsUsernameFlagSetWithoutChangingEmailProcess()
    {
        $result = new Result();
        $user = new FrontendUser('', '');
        $user->setEmail('dummyUsernameChanged');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['isUseEmailAsUsername', 'getChangeUsernameAction'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm->method('isUseEmailAsUsername')
            ->will(self::returnValue(true));
        $registrationForm->method('getChangeUsernameAction')
            ->will(self::returnValue(0));
        $this->updateProcessTest->_set('user', $user);
        $this->updateProcessTest->_set('registrationForm', $registrationForm);
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $this->updateProcessTest->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $this->updateProcessTest->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->getValidationResults();
        $this->assertEquals(['password'], array_keys($this->updateProcessTest->_get('excludeFieldFromValidation')));
        $this->updateProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(true));
        $this->updateProcessTest->process($this->request, $this->response);
        $this->assertEquals('dummyUsernameChanged', $this->updateProcessTest->getUser()->getEmail());
        $this->assertEquals('dummyUsernameChanged', $this->updateProcessTest->getUser()->getUsername());
    }

    public function testUserChangePassword()
    {
        $result = new Result();
        $user = new FrontendUser('', '');
        $user->setEmail('dummyUsername', 'changedPassword');
        $registrationForm = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['isUseEmailAsUsername'])
            ->disableOriginalConstructor()
            ->getMock();
        $registrationForm->method('isUseEmailAsUsername')
            ->will(self::returnValue(true));
        $this->updateProcessTest->_set('user', $user);
        $this->updateProcessTest->_set('registrationForm', $registrationForm);
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $this->updateProcessTest->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $this->updateProcessTest->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($result));
        $this->updateProcessTest->getValidationResults();
        $this->assertEquals(
            ['password', 'username', 'email'],
            array_keys($this->updateProcessTest->_get('excludeFieldFromValidation'))
        );
        $this->updateProcessTest->expects(self::once())
            ->method('canGetUserFromLogged')
            ->will(self::returnValue(true));
        $this->updateProcessTest->process($this->request, $this->response);
        $this->assertEquals($user->getPassword(), $this->updateProcessTest->getUser()->getPassword());
    }
}
