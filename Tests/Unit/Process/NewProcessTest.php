<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 03/12/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Process\NewProcess;
use Pallino\T3registration\Utility\UserLoggedUtility;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class NewProcessTest extends UnitTestCase
{
    /** @var AccessibleMockObjectInterface|MockObject|NewProcess */
    protected $newProcessTest;

    /** @var Request */
    protected $request;

    /** @var Response */
    protected $response;

    /** @var UserLoggedUtility|MockObject */
    protected $userLoggedUtility;

    protected function setUp()
    {
        $this->request = new Request();
        $this->request->setControllerName('Registration');
        $this->response = new Response();
        $this->response->setContent('dummyContent');
        $this->newProcessTest = $this->getAccessibleMock(NewProcess::class, [
            'redirect',
            'forward',
            'getValidationRuleManager',
            'isDisableAnnotationValidation',
            'emitMethodSignal'
        ], [], '', false);
        $this->userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['isUserLogged'])
            ->disableOriginalConstructor()
            ->getMock();
        $persistenceManager = $this->getMockBuilder(PersistenceManager::class)
            ->setMethods(['persistAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->newProcessTest->_set('userLoggedUtility', $this->userLoggedUtility);
        $this->newProcessTest->_set('processResult', new Result());
        $this->newProcessTest->_set('persistenceManager', $persistenceManager);
    }

    public function testUserIsNotLogged()
    {
        $this->userLoggedUtility->expects(self::once())
            ->method('isUserLogged')
            ->will(self::returnValue(false));
        $this->assertTrue($this->newProcessTest->process($this->request, $this->response));
        $this->newProcessTest->expects(self::never())
            ->method('forward');
        $this->newProcessTest->close();
    }

    public function testForwardIfUserIsLogged()
    {
        $this->userLoggedUtility->expects(self::once())
            ->method('isUserLogged')
            ->will(self::returnValue(true));
        $this->assertFalse($this->newProcessTest->process($this->request, $this->response));
        $this->newProcessTest->expects(self::once())
            ->method('forward')
            ->with(self::equalTo('edit'));
        $this->newProcessTest->close();
    }
}
