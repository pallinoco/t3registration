<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Process\ChangeEmailConfirmationProcess;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class ChangeEmailConfirmationProcessTest extends \Nimut\TestingFramework\TestCase\UnitTestCase
{

    /** @var Request|AccessibleMockObjectInterface|MockObject */
    protected $request;
    /** @var Response */
    protected $response;
    /** @var ChangeEmailConfirmationProcess|AccessibleMockObjectInterface|MockObject */
    protected $changeEmailProcessTest;
    /** @var TokenAuthenticationInterface|AccessibleMockObjectInterface|MockObject */
    protected $authenticationToken;
    /** @var FrontendUserRepository|MockObject */
    protected $frontendUserRepository;

    protected function setUp()
    {
        $this->request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(['hasArgument', 'getArgument'])
            ->getMock();
        $this->response = new Response();
        $this->response->setContent('dummyContent');
        $persistenceManager = $this->getMockBuilder(PersistenceManager::class)
            ->setMethods(['persistAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUserFromTokenEmail'])
            ->getMock();
        $this->authenticationToken = $this->getMockForAbstractClass(TokenAuthenticationInterface::class, [], '', true, true, true, ['isValid']);
        $this->changeEmailProcessTest = $this->getAccessibleMock(ChangeEmailConfirmationProcess::class, [
            'redirect',
            'forward',
            'canGetUserFromLogged',
            'getValidationRuleManager',
            'isDisableAnnotationValidation',
            'emitMethodSignal',
            'startChangeEmailMailProcess'
        ], [], '', false);
        $this->changeEmailProcessTest->_set('processResult', new Result());
        $this->changeEmailProcessTest->_set('persistenceManager', $persistenceManager);
        $this->changeEmailProcessTest->_set('tokenAuthentication', $this->authenticationToken);
        $this->changeEmailProcessTest->_set('frontendUserRepository', $this->frontendUserRepository);
    }

    public function testTokenIsMissed()
    {
        $this->request->method('hasArgument')
            ->will(self::returnValue(false));
        $this->changeEmailProcessTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'));
        $this->assertFalse($this->changeEmailProcessTest->process($this->request, $this->response));
        $this->changeEmailProcessTest->close();
    }

    public function testTokenIsNotValid()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::INVALID));
        $this->changeEmailProcessTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'), self::equalTo(['token' => $token]));
        $this->assertFalse($this->changeEmailProcessTest->process($this->request, $this->response));
        $this->changeEmailProcessTest->close();
    }

    public function testTokenIsValidButUserNotFound()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::VALID));
        $userFromPersistence = null;
        $this->frontendUserRepository->expects(self::any())
            ->method('findUserFromTokenEmail')
            ->will(self::returnValue($userFromPersistence));
        $this->changeEmailProcessTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'), self::equalTo(['token' => $token]));
        $this->assertFalse($this->changeEmailProcessTest->process($this->request, $this->response));
        $this->changeEmailProcessTest->close();
    }

    public function testTokenIsValid()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::VALID));
        $userFromPersistence = new FrontendUser('dummyUsername');
        $userFromPersistence->setTemporaryEmailChanged('dummyUsernameChanged');
        $userFromPersistence->setEmailChangeProcessToken($token);
        $userExpected = new FrontendUser('dummyUsernameChanged');
        $userExpected->setEmail($userExpected->getUsername());
        $userExpected->setTemporaryEmailChanged('');
        $userExpected->setEmailChangeProcessToken('');
        $this->frontendUserRepository->expects(self::any())
            ->method('findUserFromTokenEmail')
            ->will(self::returnValue($userFromPersistence));
        $this->changeEmailProcessTest->method('redirect')
            ->with(self::equalTo('changeEmailProcessComplete'), self::equalTo(['user' => $userExpected]));
        $this->assertTrue($this->changeEmailProcessTest->process($this->request, $this->response));
        $this->changeEmailProcessTest->close();
        $this->assertEquals(
            $userExpected->getTemporaryEmailChanged(),
            $this->changeEmailProcessTest->getUser()->getTemporaryEmailChanged()
        );
        $this->assertEquals(
            $userExpected->getEmailChangeProcessToken(),
            $this->changeEmailProcessTest->getUser()->getEmailChangeProcessToken()
        );
        $this->assertEquals($userExpected->getUsername(), $this->changeEmailProcessTest->getUser()->getUsername());
        $this->assertEquals($userExpected->getEmail(), $this->changeEmailProcessTest->getUser()->getEmail());
    }
}
