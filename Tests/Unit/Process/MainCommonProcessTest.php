<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Pallino\T3registration\Process\CreateProcess;
use Pallino\T3registration\Process\MainCommonProcess;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Extbase\Mvc\Controller\Arguments;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\Web\Request;

class MainCommonProcessTest extends TestCase
{
    public function testGetProcessReturnCreateProcessObject()
    {
        $request = new Request();
        $request->setControllerActionName('create');
        /** @var ControllerContext|MockObject $controllerContext */
        $controllerContext = $this->getMockBuilder(ControllerContext::class)
            ->disableOriginalConstructor()
            ->setMethods(['getRequest', 'getArguments'])
            ->getMock();
        $arguments = $this->getMockBuilder(Arguments::class)
            ->setMethods(['hasArgument'])
            ->getMock();
        $arguments->expects(self::once())
            ->method('hasArgument')
            ->will(self::returnValue(false));
        $controllerContext->expects(self::any())
            ->method('getRequest')
            ->will(self::returnValue($request));
        $controllerContext->expects(self::any())
            ->method('getArguments')
            ->will(self::returnValue($arguments));
        /** @var MainCommonProcess|MockObject $mainCommonProcess */
        $mainCommonProcess = $this->getMockBuilder(MainCommonProcess::class)
            ->setMethods(['getProcessObject'])
            ->setConstructorArgs([$controllerContext, []])
            ->getMock();
        $expectedObject = new CreateProcess($controllerContext, []);
        $mainCommonProcess->expects(self::once())
            ->method('getProcessObject')
            ->with(self::equalTo('Pallino\\T3registration\\Process\\CreateProcess'))
            ->will(self::returnValue($expectedObject));
        $this->assertEquals($expectedObject, $mainCommonProcess->getProcess());
    }
}
