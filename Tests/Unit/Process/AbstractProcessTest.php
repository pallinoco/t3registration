<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Process\AbstractProcess;
use Pallino\T3registration\Process\ProcessInterface;
use Pallino\T3registration\Utility\DeciderRuleManager;
use Pallino\T3registration\Utility\ValidationRuleManager;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;

class AbstractProcessTest extends UnitTestCase
{
    public function testProcessValidationResult()
    {
        /** -------------- MOCKING --------- */
        $resultValidation = new Result();
        $resultValidation->forProperty('dummy')->addError(new \TYPO3\CMS\Extbase\Validation\Error(
            'dummy error message',
            101010,
            [],
            'dummyTitle'
        ));
        $resultDecider = new Result();
        $resultDecider->forProperty('generic')->addError(new \TYPO3\CMS\Extbase\Validation\Error(
            'dummy error message',
            101011,
            [],
            'dummyTitle'
        ));
        /** @var AbstractProcess|MockObject|AccessibleMockObjectInterface $process */
        $process = $this->getAccessibleMockForAbstractClass(
            AbstractProcess::class,
            [],
            '',
            false,
            false,
            true,
            ['getValidationRuleManager', 'isDisableAnnotationValidation', 'getDeciderRuleManager']
        );
        $user = new FrontendUser();
        $process->_set('user', $user);
        $validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $process->expects(self::once())
            ->method('isDisableAnnotationValidation')
            ->will(self::returnValue(true));
        $process->expects(self::once())
            ->method('getValidationRuleManager')
            ->will(self::returnValue($validationRuleManager));
        $process->expects(self::once())
            ->method('getDeciderRuleManager')
            ->will(self::returnValue($deciderRuleManager));
        $validationRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($resultValidation));
        $deciderRuleManager->expects(self::once())
            ->method('process')
            ->will(self::returnValue($resultDecider));

        /** --------TEST--------- */
        $validationResult = $process->getValidationResults();
        $validationExpected = new Result();
        $validationExpected->forProperty('user')->merge($resultValidation);
        $validationExpected->forProperty('user')->merge($resultDecider);
        $this->assertEquals($validationExpected, $validationResult);
    }

    public function testValidationResultIsNeverCalledIfAlreadyValidated()
    {
        /** -------------- MOCKING --------- */
        $result = new Result();
        $result->forProperty('dummy')->addError(new \TYPO3\CMS\Extbase\Validation\Error(
            'dummy error message',
            101010,
            [],
            'dummyTitle'
        ));
        /** @var ProcessInterface|MockObject|AccessibleMockObjectInterface $process */
        $process = $this->getAccessibleMockForAbstractClass(
            AbstractProcess::class,
            [],
            '',
            false,
            false,
            true,
            ['getValidationRuleManager', 'isDisableAnnotationValidation']
        );
        $process->_set('validationResult', $result);
        $process->expects(self::never())
            ->method('isDisableAnnotationValidation');
        $process->expects(self::never())
            ->method('getValidationRuleManager');

        /** --------TEST--------- */
        $validationResult = $process->getValidationResults();
        $this->assertEquals($result, $validationResult);
    }

    public function testProcessArgumentsSetCorrectly()
    {
        /** -------------- MOCKING --------- */
        /** @var ProcessInterface|MockObject|AccessibleMockObjectInterface $process */
        $process = $this->getAccessibleMockForAbstractClass(
            AbstractProcess::class,
            [],
            '',
            false,
            false,
            true,
            ['getValidationRuleManager', 'isDisableAnnotationValidation']
        );
        $request = new Request();
        $request->setControllerName('Registration');
        $response = new Response();
        $response->setContent('dummyContent');

        /** --------TEST--------- */
        $this->assertTrue($process->process($request, $response));
        $this->assertEquals($request, $process->_get('request'));
        $this->assertEquals($response, $process->_get('response'));
    }
}
