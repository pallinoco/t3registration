<?php

namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Mail\UserNotifyConfirmModerationProcess;
use Pallino\T3registration\Process\ModerationConfirmProcess;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Mvc\Web\Response;

class ModerationConfirmProcessTest extends UnitTestCase
{


    public function testProcessWithUserEmailAndSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationConfirmProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyConfirmModerationProcess = $this->getAccessibleMock(UserNotifyConfirmModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(true);
        $registrationForm->setAdminAuthEnableSignals(true);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyConfirmModerationProcess);
        $userNotifyConfirmModerationProcess->expects(self::once())->method('process');
        $moderationConfirmProcess->expects(self::once())->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

    public function testProcessWithUserEmailButNoSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationConfirmProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyConfirmModerationProcess = $this->getAccessibleMock(UserNotifyConfirmModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(true);
        $registrationForm->setAdminAuthEnableSignals(false);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyConfirmModerationProcess);
        $userNotifyConfirmModerationProcess->expects(self::once())->method('process');
        $moderationConfirmProcess->expects($this->exactly(0))->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

    public function testProcessWithNoUserEmailButWithSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationConfirmProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyConfirmModerationProcess = $this->getAccessibleMock(UserNotifyConfirmModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(false);
        $registrationForm->setAdminAuthEnableSignals(true);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyConfirmModerationProcess);
        $userNotifyConfirmModerationProcess->expects($this->exactly(0))->method('process');
        $moderationConfirmProcess->expects(self::once())->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

    public function testProcessWithNoUserEmailAndNoSignal(){
        $moderationConfirmProcess = $this->getAccessibleMock(ModerationConfirmProcess::class, ['verifyTokenInProcess','getUserModerationObject','emitMethodSignal'], [], '', false);
        $userNotifyConfirmModerationProcess = $this->getAccessibleMock(UserNotifyConfirmModerationProcess::class, ['process'], [], '', false);
        $registrationForm = new RegistrationForm();
        $registrationForm->setEnableEmailModerationResultToUser(false);
        $registrationForm->setAdminAuthEnableSignals(false);
        $user = new FrontendUser();
        $moderationConfirmProcess->method('verifyTokenInProcess')->willReturn($user);
        $moderationConfirmProcess->method('getUserModerationObject')->willReturn($userNotifyConfirmModerationProcess);
        $userNotifyConfirmModerationProcess->expects($this->exactly(0))->method('process');
        $moderationConfirmProcess->expects($this->exactly(0))->method('emitMethodSignal');
        $moderationConfirmProcess->_set('registrationForm',$registrationForm);
        $fakeRequest = new Request();
        $fakeResponse = new Response();
        $moderationConfirmProcess->process($fakeRequest, $fakeResponse);
    }

}
