<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 03/12/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Process;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use Pallino\T3registration\Authentication\TokenAuthenticationInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Process\DeleteProcessConfirmationProcess;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class DeleteProcessConfirmationTest extends UnitTestCase
{
    /** @var Request|AccessibleMockObjectInterface|MockObject */
    protected $request;
    /** @var Response */
    protected $response;
    /** @var DeleteProcessConfirmationProcess|AccessibleMockObjectInterface|MockObject */
    protected $deleteProcessConfirmationTest;
    /** @var TokenAuthenticationInterface|AccessibleMockObjectInterface|MockObject */
    protected $authenticationToken;
    /** @var FrontendUserRepository|MockObject */
    protected $frontendUserRepository;

    protected function setUp()
    {
        $this->request = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(['hasArgument', 'getArgument'])
            ->getMock();
        $this->response = new Response();
        $this->response->setContent('dummyContent');
        $persistenceManager = $this->getMockBuilder(PersistenceManager::class)
            ->setMethods(['persistAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findUserFromToken'])
            ->getMock();
        $this->authenticationToken = $this->getMockForAbstractClass(TokenAuthenticationInterface::class, [], '', true, true, true, ['isValid']);
        $this->deleteProcessConfirmationTest = $this->getAccessibleMock(DeleteProcessConfirmationProcess::class, [
            'redirect',
            'forward',
            'getValidationRuleManager',
            'isDisableAnnotationValidation',
            'emitMethodSignal'
        ], [], '', false);
        $this->deleteProcessConfirmationTest->_set('processResult', new Result());
        $this->deleteProcessConfirmationTest->_set('persistenceManager', $persistenceManager);
        $this->deleteProcessConfirmationTest->_set('tokenAuthentication', $this->authenticationToken);
        $this->deleteProcessConfirmationTest->_set('frontendUserRepository', $this->frontendUserRepository);
    }

    public function testTokenIsMissed()
    {
        $this->request->method('hasArgument')
            ->will(self::returnValue(false));
        $this->deleteProcessConfirmationTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'));
        $this->assertFalse($this->deleteProcessConfirmationTest->process($this->request, $this->response));
        $this->deleteProcessConfirmationTest->close();
    }

    public function testTokenIsNotValid()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::INVALID));
        $this->deleteProcessConfirmationTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'), self::equalTo(['token' => $token]));
        $this->assertFalse($this->deleteProcessConfirmationTest->process($this->request, $this->response));
        $this->deleteProcessConfirmationTest->close();
    }

    public function testTokenIsValidButUserNotFound()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::VALID));
        $userFromPersistence = null;
        $this->frontendUserRepository->expects(self::any())
            ->method('findUserFromToken')
            ->will(self::returnValue($userFromPersistence));
        $this->deleteProcessConfirmationTest->method('redirect')
            ->with(self::equalTo('tokenNotValid'), self::equalTo(['token' => $token]));
        $this->assertFalse($this->deleteProcessConfirmationTest->process($this->request, $this->response));
        $this->deleteProcessConfirmationTest->close();
    }

    public function testTokenIsValid()
    {
        $token = '11111-22222-33333-44444';
        $this->request->method('hasArgument')
            ->will(self::returnValue(true));
        $this->request->method('getArgument')
            ->will(self::returnValue($token));
        $this->authenticationToken->method('isValid')
            ->with(self::equalTo($token))
            ->will(self::returnValue(TokenAuthenticationInterface::VALID));
        $userFromPersistence = new FrontendUser('dummyUsername');
        $userFromPersistence->setUserAuthenticationToken($token);
        $userFromPersistence->setDelete(false);
        $userExpected = new FrontendUser('dummyUsername');
        $userExpected->setUserAuthenticationToken('');
        $userExpected->setDelete(true);
        $this->frontendUserRepository->expects(self::any())
            ->method('findUserFromToken')
            ->will(self::returnValue($userFromPersistence));
        $this->deleteProcessConfirmationTest->method('redirect')
            ->with(self::equalTo('deleteProcessConfirmationComplete'), self::equalTo(['user' => $userExpected]));
        $this->assertTrue($this->deleteProcessConfirmationTest->process($this->request, $this->response));
        $this->deleteProcessConfirmationTest->close();
        $this->assertEquals(
            $userExpected->getUserAuthenticationToken(),
            $this->deleteProcessConfirmationTest->getUser()->getUserAuthenticationToken()
        );
        $this->assertTrue($this->deleteProcessConfirmationTest->getUser()->isDeleted());
    }
}
