<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Tests\Unit\View;

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 18/11/2018$
 */
use Pallino\T3registration\View\BasicView;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

class BasicViewTest extends TestCase
{
    public function testEmailTemplateExistsAndContentIsCorrect()
    {
        /** @var ConfigurationManager|\PHPUnit_Framework_MockObject_MockObject $configurationManager */
        $configurationManager = $this->getMockBuilder(ConfigurationManager::class)
            ->setMethods(['getConfiguration'])
            ->disableOriginalConstructor()
            ->getMock();
        $configurationManager->expects($this->once())
            ->method('getConfiguration')
            ->will(
                $this->returnValue([
                    'view' => [
                        'layoutRootPaths' => [],
                        'partialRootPaths' => [],
                        'templateRootPaths' => [
                            '10' => 'EXT:t3registration/Tests/Unit/Fixtures'
                        ]
                    ]
                ])
            );
        /** @var BasicView|\PHPUnit_Framework_MockObject_MockObject $basicView */
        $basicView = $this->getMockBuilder(BasicView::class)
            ->setMethods(['setRootPaths', 'getTemplateFilePath', 'render', 'setTemplatePathAndFilename'])
            ->disableOriginalConstructor()
            ->getMock();
        $basicView->expects($this->once())
            ->method('setRootPaths')->with($this->equalTo(
                [
                    'layoutRootPaths' => [],
                    'partialRootPaths' => [],
                    'templateRootPaths' => [
                        '10' => 'EXT:t3registration/Tests/Unit/Fixtures'
                    ]
                ]
            ));
        $basicView->expects($this->once())
            ->method('getTemplateFilePath')
            ->with(
                $this->equalTo(
                    [
                        'controller' => 'Templates',
                        'action' => 'DummyTemplateView',
                        'format' => 'html'
                    ]
                )
            );
        $basicView->injectConfigurationManager($configurationManager);
        $basicView->setContentTemplate('Templates/DummyTemplateView.html');
    }

    /**
     * @expectedException Exception
     */
    public function testEmailTemplateThrowException()
    {
        /** @var ConfigurationManager|\PHPUnit_Framework_MockObject_MockObject $configurationManager */
        $configurationManager = $this->getMockBuilder(ConfigurationManager::class)
            ->setMethods(['getConfiguration'])
            ->disableOriginalConstructor()
            ->getMock();
        $configurationManager->expects($this->once())
            ->method('getConfiguration')
            ->will(
                $this->returnValue([
                    'view' => [
                        'layoutRootPaths' => [],
                        'partialRootPaths' => [],
                        'templateRootPaths' => [
                            '10' => 'EXT:t3registration/Tests/Unit/Fixtures'
                        ]
                    ]
                ])
            );
        /** @var BasicView|\PHPUnit_Framework_MockObject_MockObject $basicView */
        $basicView = $this->getMockBuilder(BasicView::class)
            ->setMethods(['setRootPaths', 'getTemplateFilePath', 'render', 'setTemplatePathAndFilename'])
            ->disableOriginalConstructor()
            ->getMock();
        $basicView->expects($this->once())
            ->method('setRootPaths')->with($this->equalTo(
                [
                    'layoutRootPaths' => [],
                    'partialRootPaths' => [],
                    'templateRootPaths' => [
                        '10' => 'EXT:t3registration/Tests/Unit/Fixtures'
                    ]
                ]
            ));
        $basicView->injectConfigurationManager($configurationManager);
        $basicView->setContentTemplate('Templates/html');
    }
}
