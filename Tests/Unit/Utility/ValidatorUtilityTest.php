<?php

namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Tests\Unit\Fixtures\DummyIncorrectValidator;
use Pallino\T3registration\Tests\Unit\Fixtures\DummyValidator;
use Pallino\T3registration\Utility\ValidatorUtility;
use TYPO3\CMS\Extbase\Validation\Validator\NumberRangeValidator;

class ValidatorUtilityTest extends TranslationUnitTestCase
{
    protected function setUp(): void
    {
        $this->languageFilePath = 'EXT:t3registration/Resources/Private/Language/locallang_db.xlf';
        $this->LOCAL_LANG = [
            $this->languageFilePath => [
                'default' => [
                    'validator.NumberRangeValidator.label' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.label',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.label',
                        ]
                    ],
                    'validator.NumberRangeValidator.minimum.label' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.minimum.label',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.minimum.label',
                        ]
                    ],
                    'validator.NumberRangeValidator.maximum.label' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.maximum.label',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.maximum.label',
                        ]
                    ]
                ]
            ]
        ];
        parent::setUp();
    }

    public function tearDown()
    {
        ValidatorUtility::removeAll();
    }

    /**
     * @test
     */
    public function testAddValidatorImplementedInterface()
    {
        ValidatorUtility::removeAll();
        ValidatorUtility::addValidator(DummyValidator::class);
        $this->assertEquals(1, count(ValidatorUtility::getValidators()));
    }

    /**
     * @test
     */
    public function testRemoveValidator()
    {
        ValidatorUtility::removeAll();
        ValidatorUtility::addValidator(DummyValidator::class);
        ValidatorUtility::removeValidator(DummyValidator::class);
        $this->assertEquals(0, count(ValidatorUtility::getValidators()));
    }

    /**
     * @test
     */
    public function testAddValidatorNotImplementedInterface()
    {
        ValidatorUtility::removeAll();
        ValidatorUtility::addValidator(DummyIncorrectValidator::class);
        $this->assertEquals(0, count(ValidatorUtility::getValidators()));
    }

    /**
     * @test
     */
    public function testReturnValidatorStructure()
    {
        $this->setUpTranslationUtility();

        ValidatorUtility::removeAll();
        ValidatorUtility::addValidator(NumberRangeValidator::class, ['minimum', 'maximum']);
        $this->assertEquals(
            [
            [
                'id' => NumberRangeValidator::class,
                'name' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.label',
                'params' => [
                    [
                        'id' => 'minimum',
                        'name' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.minimum.label'
                    ],
                    [
                        'id' => 'maximum',
                        'name' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:validator.NumberRangeValidator.maximum.label'
                    ]
                ]
            ]
        ],
            ValidatorUtility::getValidatorsDefinitionStructure()
        );
    }
}
