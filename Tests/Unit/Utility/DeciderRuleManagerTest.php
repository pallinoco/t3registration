<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 05/12/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Decider\PasswordTwiceDecider;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Tests\Unit\Fixtures\DummyDecider;
use Pallino\T3registration\Utility\DeciderRuleManager;
use Pallino\T3registration\Utility\DeciderUtility;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Mvc\Web\Request;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DeciderRuleManagerTest extends TestCase
{
    /** @var array */
    protected $validators;

    /** @var \PHPUnit_Framework_MockObject_MockObject|RegistrationForm */
    protected $registrationFrom;

    /** @var \PHPUnit_Framework_MockObject_MockObject|DeciderRuleManager */
    protected $deciderRuleManager;

    /** @var Request|MockObject */
    protected $request;

    public function testValidateBaseUserWithoutError()
    {
        $dummyUser = new FrontendUser('', '');
        $dummyUser->setPassword('mario');

        $this->prepareMocks($dummyUser);

        $this->request->method('getArguments')
            ->will(self::returnValue(['passwordTwice' => 'mario']));
        /** @var Result $validationResult */
        $validationResult = $this->deciderRuleManager->process();

        $this->assertFalse(
            $validationResult->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('password')->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('generic')->hasErrors()
        );
    }

    public function testValidateBaseUserWithError()
    {
        $dummyUser = new FrontendUser('', 'mario');

        $this->prepareMocks($dummyUser);

        $this->request->method('getArguments')
            ->will(self::returnValue(['passwordTwice' => 'mario_error']));
        /** @var Result $validationResult */
        $validationResult = $this->deciderRuleManager->process();
        $this->assertTrue(
            $validationResult->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('generic')->hasErrors()
        );
        $this->assertTrue(
            $validationResult->forProperty('password')->hasErrors()
        );
    }

    public function objectManagerCallback()
    {
        $args = func_get_args();
        $class = $args[0];
        if ($args[1] == null) {
            $args[1] = [];
        }
        $decider = $this->getMockBuilder($class)
            ->setMethods(['translateErrorMessage'])
            ->setConstructorArgs([$args[1]])
            ->getMock();
        $decider->method('translateErrorMessage')
            ->will(
                $this->returnValue('validation error')
            );
        return $decider;
    }

    protected function prepareMocks(FrontendUser $user): void
    {
        $this->registrationFrom = $this->getMockBuilder(RegistrationForm::class)
            ->setMethods(['getDeciders'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->registrationFrom->method('getDeciders')
            ->will(self::returnValue($this->getDeciderJson()));

        $this->request = $this->getMockBuilder(Request::class)
            ->setMethods(['getArguments'])
            ->disableOriginalConstructor()
            ->getMock();

        /** @var ObjectManager|MockObject $objectManager */
        $objectManager = $this->getMockBuilder(ObjectManager::class)
            ->setMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager->method('get')
            ->will(self::returnCallback([$this, 'objectManagerCallback']));

        /** @var \PHPUnit_Framework_MockObject_MockObject|DeciderRuleManager $validationRuleManager */
        $this->deciderRuleManager = $this->getMockBuilder(DeciderRuleManager::class)
            ->setConstructorArgs([$user, $this->registrationFrom, $this->request])
            ->setMethods(null)
            ->getMock();

        $this->deciderRuleManager->injectObjectManager($objectManager);
    }

    protected function setUp()
    {
        DeciderUtility::addDecider(PasswordTwiceDecider::class, []);
        DeciderUtility::addDecider(DummyDecider::class, []);
    }

    protected function getDeciderJson()
    {
        return file_get_contents(__DIR__ . '/../Fixtures/deciders.json');
    }

    protected function tearDown()
    {
        $this->deciderRuleManager = null;
    }
}
