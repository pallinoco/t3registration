<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2019 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 2019-06-05$
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use Pallino\T3registration\Utility\UserEnableResolver;

class UserEnableResolverTest extends \Nimut\TestingFramework\TestCase\UnitTestCase
{

    /** @var RegistrationForm */
    protected $formRegistration;
    /** @var FrontendUser */
    protected $user;

    protected function setUp()
    {
        parent::setUp();
        $this->formRegistration = new RegistrationForm();
        $this->user = new FrontendUser();
    }

    /**
     * @test
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function testAdminConfirmWithDoubleOptinAndAlreadyConfirmed()
    {
        $this->user->setUserAuthenticationToken('');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL));
    }

    public function testAdminConfirmWithDoubleOptinAndNotYetConfirmedAndFlagEnableWithoutDoubleOptinActivated()
    {
        $this->user->setUserAuthenticationToken('DummyDoubleOptinToken');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $this->formRegistration->setUserEnableWithoutEmailVerification(true);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL));
    }

    public function testAdminConfirmWithDoubleOptinAndNotYetConfirmedAndFlagEnableWithoutDoubleOptinNotActivated()
    {
        $this->user->setUserAuthenticationToken('DummyDoubleOptinToken');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $this->formRegistration->setUserEnableWithoutEmailVerification(false);
        $testClass = new UserEnableResolver();
        $this->assertFalse($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL));
    }

    public function testAdminConfirmWithoutDoubleOptin()
    {
        $this->user->setUserAuthenticationToken('');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(false);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL));
    }

    public function testAdminNotConfirmed()
    {
        $this->user->setUserAuthenticationToken('');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(false);
        $testClass = new UserEnableResolver();
        $this->assertFalse($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL, false));
    }

    public function testDoubleOptinConfirmationWithAdminNotConfirmed()
    {
        $this->user->setUserAuthenticationToken('');
        $this->user->setAdminModerationToken('');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(false);
        $testClass = new UserEnableResolver();
        $this->assertFalse($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::ADMIN_APPROVAL, false));
    }

    public function testDoubleOptinConfirmationWithAdminAuthorizationAlreadyConfirmed()
    {
        $this->user->setUserAuthenticationToken('DummyDoubleOptinToken');
        $this->user->setAdminModerationToken('');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::DOUBLEOPTIN_APPROVAL));
    }

    public function testDoubleOptinConfirmationWithAdminAuthorizationAndNotYetConfirmedAndFlagEnableWithoutAdminActivated()
    {
        $this->user->setUserAuthenticationToken('DummyDoubleOptinToken');
        $this->user->setAdminModerationToken('');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $this->formRegistration->setUserEnableWithoutAdminAuth(true);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::DOUBLEOPTIN_APPROVAL));
    }

    public function testDoubleOptinConfirmationWithAdminAuthorizationAndNotYetConfirmedAndFlagEnableWithoutAdminNotActivated()
    {
        $this->user->setUserAuthenticationToken('DummyDoubleOptinToken');
        $this->user->setAdminModerationToken('DummyAdminModerationToken');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(true);
        $this->formRegistration->setUserEnableWithoutAdminAuth(false);
        $testClass = new UserEnableResolver();
        $this->assertFalse($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::DOUBLEOPTIN_APPROVAL));
    }

    public function testDoubleOptinConfirmationWithoutAdminAuthorization()
    {
        $this->user->setUserAuthenticationToken('');
        $this->user->setAdminModerationToken('');
        $this->formRegistration->setAdminAuth(true);
        $this->formRegistration->setDoubleOptin(false);
        $testClass = new UserEnableResolver();
        $this->assertTrue($testClass->isUserToEnable($this->user, $this->formRegistration, UserEnableResolver::DOUBLEOPTIN_APPROVAL));
    }
}
