<?php

namespace Pallino\T3registration\Tests\Unit\Utility;

use TYPO3\CMS\Core\Localization\LocalizationFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

class TranslationUnitTestCase extends UnitTestCase
{
    /**
     * Instance of configurationManagerInterface, injected to subject
     *
     * @var ConfigurationManagerInterface
     */
    protected $configurationManagerInterfaceProphecy;
    /**
     * LOCAL_LANG array fixture
     *
     * @var array
     */
    protected $LOCAL_LANG = [];
    /**
     * File path of locallang for extension "core"
     * @var string
     */
    protected $languageFilePath = '';

    protected function setUp(): void
    {
        parent::setUp();
        $reflectionClass = new \ReflectionClass(LocalizationUtility::class);
        $this->configurationManagerInterfaceProphecy = $this->prophesize(ConfigurationManagerInterface::class);
        $property = $reflectionClass->getProperty('configurationManager');
        $property->setAccessible(true);
        $property->setValue($this->configurationManagerInterfaceProphecy->reveal());
        $localizationFactoryProphecy = $this->prophesize(LocalizationFactory::class);
        GeneralUtility::setSingletonInstance(LocalizationFactory::class, $localizationFactoryProphecy->reveal());
        $localizationFactoryProphecy->getParsedData(
            'EXT:t3registration/Resources/Private/Language/locallang_db.xlf',
            'default'
        )->willReturn($this->LOCAL_LANG[$this->languageFilePath]['default']);
    }

    protected function setUpTranslationUtility()
    {
        $reflectionClass = new \ReflectionClass(LocalizationUtility::class);
        $property = $reflectionClass->getProperty('LOCAL_LANG');
        $property->setAccessible(true);
        $property->setValue($this->LOCAL_LANG);

        $backendUserAuthenticationProphecy = $this->prophesize(BackendUserAuthentication::class);
        $GLOBALS['BE_USER'] = $backendUserAuthenticationProphecy->reveal();
        $backendUserAuthenticationProphecy->uc = [
            'lang' => 'default',
        ];
        $GLOBALS['LANG'] = $this->LOCAL_LANG;
    }
}
