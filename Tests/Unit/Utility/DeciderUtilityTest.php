<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Tests\Unit\Fixtures\DummyDecider;
use Pallino\T3registration\Tests\Unit\Fixtures\DummyIncorrectDecider;
use Pallino\T3registration\Tests\Unit\Fixtures\DummyRangeDecider;
use Pallino\T3registration\Utility\DeciderUtility;

class DeciderUtilityTest extends TranslationUnitTestCase
{
    protected function setUp(): void
    {
        $this->languageFilePath = 'EXT:t3registration/Resources/Private/Language/locallang_db.xlf';
        $this->LOCAL_LANG = [
            $this->languageFilePath => [
                'default' => [
                    'decider.DummyRangeDecider.label' => [
                        [
                            'source' => 'label',
                            'target' => 'label',
                        ]
                    ],
                    'decider.DummyRangeDecider.minimum.label' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.minimum.label',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.minimum.label',
                        ]
                    ],
                    'decider.DummyRangeDecider.maximum.label' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.maximum.label',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.maximum.label',
                        ]
                    ]
                ]
            ]
        ];
        parent::setUp();
    }

    public function tearDown()
    {
        DeciderUtility::removeAll();
    }

    /**
     * @test
     */
    public function testAddDeciderImplementedInterface()
    {
        DeciderUtility::removeAll();
        DeciderUtility::addDecider(DummyDecider::class);
        $this->assertEquals(1, count(DeciderUtility::getDeciders()));
    }

    /**
     * @test
     */
    public function testRemoveDecider()
    {
        DeciderUtility::removeAll();
        DeciderUtility::addDecider(DummyDecider::class);
        DeciderUtility::removeDecider(DummyDecider::class);
        $this->assertEquals(0, count(DeciderUtility::getDeciders()));
    }

    /**
     * @test
     */
    public function testAddDeciderNotImplementedInterface()
    {
        DeciderUtility::removeAll();
        DeciderUtility::addDecider(DummyIncorrectDecider::class);
        $this->assertEquals(0, count(DeciderUtility::getDeciders()));
    }

    /**
     * @test
     */
    public function testReturnDeciderStructure()
    {
        $this->setUpTranslationUtility();
        DeciderUtility::removeAll();
        DeciderUtility::addDecider(DummyRangeDecider::class, ['minimum', 'maximum']);

        $this->assertEquals(
            [
            [
                'id' => DummyRangeDecider::class,
                'name' => 'label',
                'params' => [
                    [
                        'id' => 'minimum',
                        'name' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.minimum.label'
                    ],
                    [
                        'id' => 'maximum',
                        'name' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:decider.DummyRangeDecider.maximum.label'
                    ]
                ]
            ]
        ],
            DeciderUtility::getDecidersDefinitionStructure()
        );
    }
}
