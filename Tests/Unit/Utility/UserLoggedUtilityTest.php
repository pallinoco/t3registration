<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 29/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Repository\FrontendUserRepository;
use Pallino\T3registration\Utility\UserLoggedUtility;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class UserLoggedUtilityTest extends \Nimut\TestingFramework\TestCase\UnitTestCase
{
    /** @var UserLoggedUtility|MockObject */
    protected $userLoggedUtility;

    public function testGetLoggedExistentUser()
    {
        $user['username'] = 'dummyUsername';
        $user['user'] = 1;
        $frontendUser = new FrontendUser('dummyUsername');
        $frontendUser->_setProperty('uid', 1);
        $this->prepareConfigurationForLoggedUser($user, $frontendUser);
        $this->assertEquals(1, $this->userLoggedUtility->getLoggedUser()->getUid());
    }

    public function testGetLoggedNotExistentUser()
    {
        $this->prepareConfigurationForLoggedUser(null, null);
        $this->assertEquals(null, $this->userLoggedUtility->getLoggedUser());
    }

    public function testUserLoggedEqualToPassedArgument()
    {
        $this->prepareConfigurationForLoggedUser(['username' => 'dummyUsername', 'uid' => 1], null);
        $this->assertTrue($this->userLoggedUtility->isUserLoggedEqualTo(1));
        $this->assertTrue($this->userLoggedUtility->isUserLoggedEqualTo(null, 'dummyUsername'));
        $this->assertFalse($this->userLoggedUtility->isUserLoggedEqualTo(null, ''));
    }

    public function testUserLoggedEqualToPassedArgumentUserNotLogged()
    {
        $this->prepareConfigurationForLoggedUser(null, null);
        $this->assertFalse($this->userLoggedUtility->isUserLoggedEqualTo(1));
        $this->assertFalse($this->userLoggedUtility->isUserLoggedEqualTo(null, 'dummyUsername'));
        $this->assertFalse($this->userLoggedUtility->isUserLoggedEqualTo(null, ''));
    }

    protected function prepareConfigurationForLoggedUser(array $user = null, FrontendUser $frontendUser = null)
    {
        /** @var FrontendUserAuthentication|AccessibleMockObjectInterface|MockObject $userAuthentication */
        $userAuthentication = $this->getAccessibleMock(FrontendUserAuthentication::class, null, [], '', false);
        $userAuthentication->_set('user', $user);
        /** @var TypoScriptFrontendController|AccessibleMockObjectInterface|MockObject $typoscriptFrontendController */
        $typoscriptFrontendController = $this->getAccessibleMock(
            TypoScriptFrontendController::class,
            null,
            [],
            '',
            false
        );
        if ($user == null) {
            $typoscriptFrontendController->_set('loginUser', false);
        } else {
            $typoscriptFrontendController->_set('loginUser', true);
        }
        $typoscriptFrontendController->_set('fe_user', $userAuthentication);
        $this->userLoggedUtility = $this->getMockBuilder(UserLoggedUtility::class)
            ->setMethods(['getTyposcriptFrontendController'])
            ->getMock();
        $this->userLoggedUtility->expects(self::any())
            ->method('getTyposcriptFrontendController')
            ->will(self::returnValue($typoscriptFrontendController));

        $frontendUserRepository = $this->getMockBuilder(FrontendUserRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findByUid'])
            ->getMock();
        $frontendUserRepository->method('findByUid')
            ->will(self::returnValue($frontendUser));
        $this->userLoggedUtility->injectFrontendUserRepository($frontendUserRepository);
    }
}
