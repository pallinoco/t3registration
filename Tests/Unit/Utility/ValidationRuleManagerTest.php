<?php
/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 09/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Domain\Model\FrontendUser;
use Pallino\T3registration\Domain\Model\RegistrationFormFields;
use Pallino\T3registration\Utility\ValidationRuleManager;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Extbase\Error\Result;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\ColumnMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMap;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator;

class ValidationRuleManagerTest extends TestCase
{
    /** @var array */
    protected $validators;

    /** @var \PHPUnit_Framework_MockObject_MockObject|DataMapper */
    protected $dataMapper;

    /** @var \PHPUnit_Framework_MockObject_MockObject|RegistrationFormFields */
    protected $registrationFromFields;

    /** @var \PHPUnit_Framework_MockObject_MockObject|ValidationRuleManager */
    protected $validationRuleManager;

    public function testValidateBaseUserWithoutError()
    {
        $dummyUser = new FrontendUser('', '');
        $dummyUser->setUsername('mario.rossi');
        $dummyUser->setEmail('mario@rossi.it');
        $dummyUser->setFirstName('Mario');
        $dummyUser->setLastName('55');

        $this->prepareMocks($dummyUser);
        /** @var Result $validationResult */
        $validationResult = $this->validationRuleManager->process();

        $this->assertFalse(
            $validationResult->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('email')->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('lastName')->hasErrors()
        );
    }

    public function testValidateBaseUserWithError()
    {
        $dummyUser = new FrontendUser('', '');
        $dummyUser->setUsername('mario.rossi');
        $dummyUser->setEmail('mario@rossi.it');
        $dummyUser->setFirstName('Mario');
        $dummyUser->setLastName('553344444');

        $this->prepareMocks($dummyUser);
        /** @var Result $validationResult */
        $validationResult = $this->validationRuleManager->process();
        $this->assertTrue(
            $validationResult->hasErrors()
        );
        $this->assertFalse(
            $validationResult->forProperty('email')->hasErrors()
        );
        $this->assertTrue(
            $validationResult->forProperty('lastName')->hasErrors()
        );
        $this->assertEquals(
            '1221561046',
            $validationResult->forProperty('lastName')->getFirstError()->getCode()
        );
        $this->assertEquals(
            'validation error',
            $validationResult->forProperty('lastName')->getFirstError()->getMessage()
        );
    }

    public function testValidateBaseUserWithMultipleError()
    {
        $dummyUser = new FrontendUser('', '');
        $dummyUser->setUsername('mario.rossi');
        $dummyUser->setEmail('mario');
        $dummyUser->setFirstName('Mario');
        $dummyUser->setLastName('5533rrr44444tttt');

        $this->prepareMocks($dummyUser);
        /** @var Result $validationResult */
        $validationResult = $this->validationRuleManager->process();
        $this->assertTrue(
            $validationResult->hasErrors()
        );
        $this->assertTrue(
            $validationResult->forProperty('email')->hasErrors()
        );
        $this->assertTrue(
            $validationResult->forProperty('lastName')->hasErrors()
        );
        $this->assertEquals(
            '1221563685',
            $validationResult->forProperty('lastName')->getFirstError()->getCode()
        );
        $this->assertEquals(
            'validation error',
            $validationResult->forProperty('lastName')->getFirstError()->getMessage()
        );
    }

    public function columnMapCallback()
    {
        $args = func_get_args();
        $columnMapper = [
            'lastName' => 'last_name',
            'email' => 'email'
        ];
        if (array_key_exists($args[0], $columnMapper)) {
            $columnMap = new ColumnMap($columnMapper[$args[0]], $args[0]);
        } else {
            $columnMap = new ColumnMap($args[0], $args[0]);
        }

        return $columnMap;
    }

    public function createValidatorCallback()
    {
        $args = func_get_args();
        $class = $args[0];
        if ($args[1] == null) {
            $args[1] = [];
        }
        $validator = $this->getMockBuilder($class)
            ->setMethods(['translateErrorMessage'])
            ->setConstructorArgs([$args[1]])
            ->getMock();
        $validator->method('translateErrorMessage')
            ->will(
                $this->returnValue('validation error')
            );
        return $validator;
    }

    public function validatorsArrayCallback()
    {
        $args = func_get_args();
        if (array_key_exists($args[0], $this->validators)) {
            return $this->validators[$args[0]];
        } else {
            return [];
        }
    }

    protected function prepareMocks(FrontendUser $user): void
    {
        $this->validators = require __DIR__ . '/../Fixtures/RegistrationFormFieldsArrayResult.php';
        /** @var RegistrationFormFields|\PHPUnit_Framework_MockObject_MockObject $registrationFromFields */
        $this->registrationFromFields = $this->getMockBuilder(RegistrationFormFields::class)
            ->setMethods(['getFieldsList', 'getValidatorsForField'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->registrationFromFields->method('getFieldsList')
            ->will(
                $this->returnValue(['last_name', 'email'])
            );

        $this->registrationFromFields->method('getValidatorsForField')
            ->will(
                $this->returnCallback([$this, 'validatorsArrayCallback'])
            );
        /** @var \PHPUnit_Framework_MockObject_MockObject|DataMap $dataMap */
        $dataMap = $this->getMockBuilder(DataMap::class)
            ->setMethods(['getColumnMap'])
            ->disableOriginalConstructor()
            ->getMock();
        $dataMap->method('getColumnMap')
            ->will(
                $this->returnCallback([$this, 'columnMapCallback'])
            );
        /** @var \PHPUnit_Framework_MockObject_MockObject|DataMapper $dataMapper */
        $this->dataMapper = $this->getMockBuilder(DataMapper::class)
            ->setMethods(['getDataMap'])
            ->getMock();
        $this->dataMapper->method('getDataMap')
            ->will(
                $this->returnValue($dataMap)
            );
        /** @var \PHPUnit_Framework_MockObject_MockObject|ValidationRuleManager $validationRuleManager */
        $this->validationRuleManager = $this->getMockBuilder(ValidationRuleManager::class)
            ->setConstructorArgs([$user, $this->registrationFromFields])
            ->setMethods(['getResult', 'getConjunctionValidator', 'createValidator'])
            ->getMock();

        $this->validationRuleManager->method('getResult')
            ->will(
                $this->returnValue(
                    new Result()
                )
            );

        $this->validationRuleManager->method('getConjunctionValidator')
            ->will(
                $this->returnCallback(function () {
                    return new ConjunctionValidator();
                })
            );
        $this->validationRuleManager->method('createValidator')
            ->will(
                $this->returnCallback([$this, 'createValidatorCallback'])
            );
        //  = new ValidationRuleManager($dummyUser, $registrationFromFields);
        $this->validationRuleManager->injectDataMapper($this->dataMapper);
    }

    protected function setUp()
    {
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\IntegerValidator::class);

        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(
            \TYPO3\CMS\Extbase\Validation\Validator\NumberRangeValidator::class,
            ['minimum', 'maximum']
        );
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\AlphanumericValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\DateTimeValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\TextValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(
            \TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator::class,
            ['minimum', 'maximum']
        );
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\FloatValidator::class);
        \Pallino\T3registration\Utility\ValidatorUtility::addValidator(
            \TYPO3\CMS\Extbase\Validation\Validator\RegularExpressionValidator::class,
            ['regularExpression']
        );
    }

    protected function tearDown()
    {
        $this->validationRuleManager = null;
        $this->dataMapper = null;
        parent::tearDown();
    }
}
