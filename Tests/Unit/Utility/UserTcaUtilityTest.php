<?php
/**
 * Created by PhpStorm.
 * User: alessandro.filira
 * Date: 08/11/18
 * Time: 10:47
 */
namespace Pallino\T3registration\Tests\Unit\Utility;

use Pallino\T3registration\Utility\UserTcaUtility;

class UserTcaUtilityTest extends TranslationUnitTestCase
{
    protected function setUp(): void
    {
        $this->languageFilePath = 'EXT:lang/Resources/Private/Language/locallang_general.xlf';
        $this->LOCAL_LANG = [
            $this->languageFilePath => [
                'default' => [
                    'LGL.address' => [
                        [
                            'source' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.address',
                            'target' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.address',
                        ]
                    ],
                    'LGL.first_name' => [
                        [
                            'source' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.first_name',
                            'target' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.first_name',
                        ]
                    ],
                    'LGL.last_name' => [
                        [
                            'source' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.last_name',
                            'target' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.last_name',
                        ]
                    ],
                    'LGL.hidden' => [
                        [
                            'source' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                            'target' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                        ]
                    ],
                    'frontenduser.field.tx_t3registration_user_authentication_token' => [
                        [
                            'source' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_user_authentication_token',
                            'target' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_user_authentication_token',
                        ]
                    ]
                ]
            ]
        ];
        parent::setUp();
    }

    protected function tearDown()
    {
    }

    public function testGetFields()
    {
        $this->setUpTranslationUtility();
        $GLOBALS['TCA']['fe_users']['columns'] = $this->getTCAExpectation();
        $fields = UserTcaUtility::getFields();
        $this->assertEquals($this->getExpectationStructuredArray(), $fields);
    }

    protected function getExpectationStructuredArray()
    {
        return [
            [
                'id' => 'address',
                'name' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.address'
            ],
            [
                'id' => 'first_name',
                'name' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.first_name'
            ],
            [
                'id' => 'last_name',
                'name' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.last_name'
            ],
        ];
    }

    protected function getTCAExpectation()
    {
        $TcaExpectation = [
            'address' => [
                'exclude' => true,
                'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.address',
                'config' =>
                    [
                        'type' => 'text',
                        'cols' => 20,
                        'rows' => 3,
                    ],
            ],
            'hidden' => [
                'exclude' => true,
                'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                'config' =>
                    [
                        'type' => 'text',
                        'cols' => 20,
                        'rows' => 3,
                    ],
            ],
            'first_name' => [
                'exclude' => true,
                'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.first_name',
                'config' =>
                    [
                        'type' => 'text',
                        'cols' => 20,
                        'rows' => 3,
                    ],
            ],
            'last_name' => [
                'exclude' => true,
                'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_general.xlf:LGL.last_name',
                'config' =>
                    [
                        'type' => 'text',
                        'cols' => 20,
                        'rows' => 3,
                    ],
            ],
            'tx_t3registration_user_authentication_token' => [
                'exclude' => 1,
                'label' => 'LLL:EXT:t3registration/Resources/Private/Language/locallang_db.xlf:frontenduser.field.tx_t3registration_user_authentication_token',
                'excludeFromRegistrationForm' => true,
                'config' => [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim'
                ],
            ],
        ];
        return $TcaExpectation;
    }
}
