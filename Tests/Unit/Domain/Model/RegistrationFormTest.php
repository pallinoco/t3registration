<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

namespace Pallino\T3registration\Tests\Unit\Domain\Model;

use Pallino\T3registration\Domain\Model\RegistrationForm;
use PHPUnit\Framework\TestCase;

class RegistrationFormTest extends TestCase
{
    public function testIfIsJson()
    {
        $objTest = new RegistrationForm();
        $objTest->setAdminAuth(true);
        $objTest->setAdminEmails('nome.congnome@pallino.it');
        $this->assertEquals(self::isJson($objTest->toJson())->toString(), 'is valid JSON');
    }

    public function testJsonFields()
    {
        $objTest = new RegistrationForm();
        $objTest->setAdminAuth(true);
        $objTest->setAdminEmails('nome.congnome@pallino.it');
        $objTest->setTitle('This is title field');
        $json = $objTest->toJson();
        $reverseObject = json_decode($json);
        $this->assertEquals($objTest->getAdminEmails(), $reverseObject->config->adminEmails);
        $this->assertEquals($objTest->getTitle(), $reverseObject->config->title);
    }
}
