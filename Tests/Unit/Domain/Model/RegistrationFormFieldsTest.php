<?php
/*************************************************************************
 *
 * PALLINO COPYRIGHT SOFTWARE
 * __________________
 *
 *  [20010] - [2017] Pallino & Co. Srl
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Pallino & Co. Srl  and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Pallino & Co. Srl
 * and its suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Pallino & Co. Srl .
 */

/**
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 08/11/2018$
 */
namespace Pallino\T3registration\Tests\Unit\Domain\Model;

use Pallino\T3registration\Domain\Model\RegistrationFormFields;
use PHPUnit\Framework\TestCase;

class RegistrationFormFieldsTest extends TestCase
{
    public function testGetValidatorsForField()
    {
        $registrationFieldsString = require __DIR__ . '/../../Fixtures/RegistrationFormFieldsData.php';
        $registrationFormFieldArrayResult = require __DIR__ . '/../../Fixtures/RegistrationFormFieldsArrayResult.php';
        /** @var RegistrationFormFields $registrationFormField */
        $registrationFormField = new RegistrationFormFields($registrationFieldsString);
        $this->assertEquals(
            $registrationFormFieldArrayResult['last_name'],
            $registrationFormField->getValidatorsForField('last_name')
        );
    }

    public function testReturnFieldsList()
    {
        $registrationFieldsString = require __DIR__ . '/../../Fixtures/RegistrationFormFieldsData.php';
        /** @var RegistrationFormFields $registrationFormField */
        $registrationFormField = new RegistrationFormFields($registrationFieldsString);
        $this->assertEquals(
            [
                'last_name',
                'email'
            ],
            $registrationFormField->getFieldsList()
        );
    }
}
