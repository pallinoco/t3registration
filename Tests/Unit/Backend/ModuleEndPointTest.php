<?php
/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Created by PhpStorm.
 * User: alessandro.filira
 * Date: 10/12/18
 * Time: 14:55
 */

namespace Pallino\T3registration\Tests\Unit\Backend;

use GuzzleHttp\Psr7\ServerRequest;
use Pallino\T3registration\Backend\ModuleEndPoint;
use Pallino\T3registration\Domain\Model\RegistrationForm;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Http\Response;

class ModuleEndPointTest extends TestCase
{
    protected $requestJson = '';
    protected $responseJson = '';

    protected function getRequestJson()
    {
        return file_get_contents(__DIR__ . '/request.json');
    }

    protected function getExpectedResponse()
    {
        $response = file_get_contents(__DIR__ . '/response.json');
        $response = json_decode($response);
        return $response;
    }

    protected function getRegistrationFrom($request)
    {
        $requestArray = json_decode($request);
        $registrationForm = new RegistrationForm();
        $registrationForm->_setProperty('uid', $requestArray->config->uid);
        $registrationForm->setTitle($requestArray->config->title);
        $registrationForm->setDoubleOptin($requestArray->config->doubleOptin);
        $registrationForm->setAdminAuth($requestArray->config->adminAuth);
        $registrationForm->setAdminEmails($requestArray->config->adminEmails);
        $registrationForm->setAfterAdminConfirmGroups($requestArray->config->afterAdminConfirmGroups);
        $registrationForm->setUserEnableWithoutAdminAuth($requestArray->config->userEnableWithoutAdminAuth);
        $registrationForm->setAdminAuthEnableSignals($requestArray->config->adminAuthEnableSignals);
        $registrationForm->setEnableEmailModerationResultToUser($requestArray->config->enableEmailModerationResultToUser);
        $registrationForm->setUserEnableWithoutEmailVerification($requestArray->config->userEnableWithoutEmailVerification);
        $registrationForm->setBeforeUserConfirmGroups($requestArray->config->beforeUserConfirmGroups);
        $registrationForm->setAfterUserConfirmationGroups($requestArray->config->afterUserConfirmationGroups);
        $registrationForm->setGeneralSenderEmailAddress($requestArray->config->generalSenderEmailAddress);
        $registrationForm->setGeneralSenderEmailName($requestArray->config->generalSenderEmailName);
        $registrationForm->setUseEmailAsUsername($requestArray->config->useEmailAsUsername);
        $registrationForm->setEnableChangeUsername($requestArray->config->enableChangeUsername);
        $registrationForm->setChangeUsernameAction($requestArray->config->changeUsernameAction);
        $registrationForm->setDeciders(json_encode($requestArray->config->deciders));
        $registrationForm->setEnableApi($requestArray->config->enableApi);
        $registrationForm->setEnableFeReport($requestArray->config->enableFeReport);
        $registrationForm->setTestingEmail($requestArray->config->testingEmail);
        $registrationForm->setEnableTestEnvironment($requestArray->config->enableTestEnvironment);
        $registrationForm->setEnableModelAnnotationValidation($requestArray->config->enableModelAnnotationValidation);
        $registrationForm->setFields(json_encode($requestArray->fields));
        return $registrationForm;
    }

    /**
     * @test
     */
    public function testSave()
    {
        $fakeRequestObj = new ServerRequest('POST', 'fakeUrl');
        $fakeResponseObj = new Response();
        $requestJson = $this->getRequestJson();
        $expectedResponse = $this->getExpectedResponse();
        $registrationForm = $this->getRegistrationFrom($requestJson);

        /** @var \PHPUnit_Framework_MockObject_MockObject|CacheBuilder $cacheBuilder */
        $moduleEndPoint = $this->getMockBuilder(ModuleEndPoint::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'getJsonContent',
                    'saveRegistrationForm',
                    'getTranslation',
                    'getRegistrationFormObject'
                ]
            )
            ->getMock();
        $moduleEndPoint->expects($this->exactly(1))->method('getJsonContent')->willReturn($requestJson);
        $moduleEndPoint->expects($this->exactly(1))->method('getTranslation')->willReturn('Form Saved');
        $moduleEndPoint->expects($this->exactly(1))->method('getRegistrationFormObject')->willReturn($registrationForm);
        $moduleEndPoint->expects($this->once())->method('saveRegistrationForm');
        $response = $moduleEndPoint->save($fakeRequestObj, $fakeResponseObj);
        $jsonResponse = json_decode($response->getBody()->__toString());
        $this->assertEquals($expectedResponse, $jsonResponse);
    }

    /**
     * @test
     */
    public function testException()
    {
        $fakeRequestObj = new ServerRequest('POST', 'fakeUrl');
        $fakeResponseObj = new Response();
        $requestJson = $this->getRequestJson();
        $moduleEndPoint = $this->getMockBuilder(ModuleEndPoint::class)
            ->disableOriginalConstructor()
            ->setMethods(
                [
                    'getJsonContent',
                    'saveRegistrationForm',
                    'getTranslation',
                    'getRegistrationFormObject',
                    'setResponse'
                ]
            )
            ->getMock();
        $exception = new \TYPO3\CMS\Extbase\Property\Exception('error');
        $moduleEndPoint->expects($this->exactly(1))->method('getJsonContent')->willReturn($requestJson);
        $moduleEndPoint->expects($this->exactly(1))->method('getRegistrationFormObject')->willThrowException($exception);
        $moduleEndPoint->expects($this->exactly(0))->method('getTranslation')->willReturn('string');
        $moduleEndPoint->expects($this->exactly(0))->method('saveRegistrationForm');
        $moduleEndPoint->expects($this->exactly(1))->method('setResponse');
        $response = $moduleEndPoint->save($fakeRequestObj, $fakeResponseObj);
    }
}
