<?php
/**
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Pallino & Co. Srl, http://www.pallino.it
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

/** @var $extbaseObjectContainer \TYPO3\CMS\Extbase\Object\Container\Container */
$extbaseObjectContainer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class);
// Singleton
$extbaseObjectContainer->registerImplementation(\Pallino\T3registration\Authentication\TokenAuthenticationInterface::class,
    \Pallino\T3registration\Authentication\SimpleTokenFactory::class);
unset($extbaseObjectContainer);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Pallino.T3registration',
    'Form',
	array(
        'Registration' => 'new,create,preview,registrationComplete,doubleOptinConfirmation,edit,update,accessDenied,delete,updateComplete,deleteProcessEmailSent,deleteProcessCancellation,deleteProcessConfirmation,tokenNotValid,doubleOptinProcessComplete,changeEmailProcessComplete,changeEmailConfirmation,deleteProcessCancellationComplete,deleteProcessConfirmationComplete,moderationConfirm,moderationReject'

	),
	// non-cacheable actions
	array(
        'Registration' => 'new,create,preview,registrationComplete,doubleoptinConfirmation,edit,update,accessDenied,delete,updateComplete,deleteProcessEmailSent,deleteProcessCancellation,deleteProcessConfirmation,tokenNotValid,deleteProcessCancellationComplete,deleteProcessConfirmationComplete,moderationConfirm,moderationReject'

	)
);

// Register cache frontend for proxy class generation
$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['t3registration'] = [
    'frontend' => \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend::class,
    'backend' => \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
    'groups' => [
        'all',
        'system',
    ],
    'options' => [
        'defaultLifetime' => 0,
    ]
];

\Pallino\T3registration\Utility\ClassLoader::registerAutoloader();


\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\IntegerValidator::class);

\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\NumberRangeValidator::class,
    ['minimum', 'maximum']);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\AlphanumericValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\DateTimeValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\TextValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator::class,
    ['minimum', 'maximum']);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\FloatValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Extbase\Validation\Validator\RegularExpressionValidator::class,
    ['regularExpression']);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\Pallino\T3registration\Validator\UsernameUniqueValidator::class);
\Pallino\T3registration\Utility\ValidatorUtility::addValidator(\Pallino\T3registration\Validator\UsernameUniqueInPidValidator::class,
    ['pid']);
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('form')) {
    \Pallino\T3registration\Utility\ValidatorUtility::addValidator(\TYPO3\CMS\Form\Mvc\Validation\CountValidator::class,
        ['minimum', 'maximum']);
}

$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::
makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
$signalSlotDispatcher->connect(
    \Pallino\T3registration\Controller\RegistrationController::class,  // Signal class name
    'beforeUserPersisted',                                  // Signal name
    \Pallino\T3registration\Controller\RegistrationController::class,        // Slot class name
    'dummy'                               // Slot name
);

\Pallino\T3registration\Utility\DeciderUtility::addDecider(\Pallino\T3registration\Decider\PasswordTwiceDecider::class,
    []);
