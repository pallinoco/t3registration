Extension Manual
=================

This is a TYPO3 extension to create a manage user registration process inside a website built with TYPO3.
For information and documentation please go to `Official Documentation <https://docs.typo3.org/p/pallino/t3registration/master/en-us/>`_.
