.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt


.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
T3registration
=============================================================

:Version:
   |release|

:Language:
   en

:Authors:
   Pallino & Co. Srl

:Email:
   t3registration@pallino.it

:License:
   This extension documentation is published under the
   `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
   license

This extension helps user to create a completely customizable registration process for frontend users.

.. tip::

   This version works only in TYPO3 9.5 or above, please check the composer.json file.

**TYPO3**

   The content of this document is related to TYPO3 CMS,
   a GNU/GPL CMS/Framework available from `typo3.org
   <https://typo3.org/>`_ .

**Community Documentation:**

   This documentation is community documentation for the TYPO3 extension t3registration

   It is maintained as part of this third party extension.

   If you find an error or something is missing, please:
   `Report a Problem <https://bitbucket.org/pallinoco/t3registration/issues?status=new&status=open>`__

**Extension Manual**

   This documentation is for the TYPO3 extension t3registration.



**For Contributors**

   You are welcome to help improve this guide. Just click on "Edit me on GitHub"
   on the top right to submit your change request.


**Sitemap:**

   :ref:`sitemap`


.. toctree::
   :maxdepth: 3
   :hidden:


   Introduction/Index
   Administrators/Index
   Editor/Index
   Configuration/Index
   Developer/Index
   Sitemap

