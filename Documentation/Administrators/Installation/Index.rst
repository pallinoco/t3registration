.. include:: ../../Includes.txt



.. _installation:

============
Installation
============

Target group: **Administrators**

The extension needs to be installed as any other extension of TYPO3 CMS:

#. Switch to the module “Extension Manager”.

#. Get the extension

   #. **Get it from the Extension Manager:** Press the “Retrieve/Update”
      button and search for the extension key *news* and import the
      extension from the repository.

   #. **Use composer**: Use `composer require pallino/t3registration`.

You can also refer to general TYPO3 documentation, for example the
:ref:`t3install:start`.
