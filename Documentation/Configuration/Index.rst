.. include:: ../Includes.txt


.. _configuration:

=============
Configuration
=============

Target group: **Editors, Developers, Integrators**

T3registration is a flexible and complete registration system and it has many features and configurations to customize the registration behaviour.

The most important configuration features are available in the backend module in the form settings.

Name
----

In this field you can fill the name of the form, this name is used inside the select in the plugin. This field is compulsory.

Admin Authorization/Moderation
------------------------------

If you check this field you are enabling the moderation process. After user will be saved into TYPO3 an email will be sent to a specific email address with two links: one to authorize user and another to decline the registration user process.

Admin moderation email (comma separated) [enabled wih Admin moderation check]
-----------------------------------------------------------------------------

List the emails used to received moderated email (see above).

User Groups after admin confirmation [enabled wih Admin moderation check]
-------------------------------------------------------------------------

Select the group to associate to the user after the process of authorization.

User is enable without admin confirmation [enabled wih Admin moderation check]
------------------------------------------------------------------------------

If use enable the check after moderator authorizes user (also in case user hasn't yet confirmed email) will be activated.

Enable signal for admin moderation [enabled wih Admin moderation check]
-----------------------------------------------------------------------

With this check you can disable the signal bind with moderation process.

Enable email with confirmation/reject message [enabled wih Admin moderation check]
----------------------------------------------------------------------------------

If you check after authorization process an email with resul will be sent to user, otherwise the process is muted.

Double opt-in
-------------

Checking this box you activate the process to send an email to user with a link to confirm his registration.

User is enable without admin confirmation [enabled wih Double opt-in]
---------------------------------------------------------------------

If you have activated the moderation process the user will be activated also before admin authorization.

User Groups before double Optin [enabled wih Double opt-in]
-----------------------------------------------------------

When double opt-in is activated before user confirm the email this groups will be associated.

User Groups after double opt-in (or after registration if it is disabled)
-------------------------------------------------------------------------

At the end of the process of registration (in case of double opt-in or admin authorization) these groups will be associated to the user.

Sender Email Address
--------------------

Email used to send email from the website.

Sender Email name
-----------------

From field in the email sent from the website.

Enable email as username
------------------------

If you want use the email field as username you have to check this box.

Enable change username feature
------------------------------

If this feature is enabled the user can change his username and the process is based on a double opt-in.

New username verification action
--------------------------------

This feature is bind with the previous one: you should choose what behaviour you prefer when user change the username: send a confirmation email to the old email address, to the new email address, no email will be sent.

Deciders
--------

Deciders are similar to validators. The difference is validators act on single field, deciders act on multiple fields, for example repeat password is a validation rule execute by decider because it will verify two fields: password and password repeated. In developer section you can find the explanation about decider creation.

Enable model annotation validation
----------------------------------

Normally extension verify only for validator rules, if you enable this check, the system does validation for model field vaidations.

