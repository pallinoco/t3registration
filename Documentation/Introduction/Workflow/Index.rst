.. include:: ../../Includes.txt


.. _workflow:

========
Workflow
========

One of the most complicated part in this docuemntation was the regsitration possible workflow. It sound weird but the configuration is very easy if you understand the many features of this extension.

Different types of registration
===============================

The common part of the process is the filling form, user fills the registration form with information some compulsory.

During the process user can preview filled form data before submitting them to the system, and in some cases he can modify data. This feature is called "**preview**" in the configuration of the extension, so if you want enable this behaviour you can activate preview.

At the end of filling process (included optionally preview) the data are sent to the system. Some registration process are complete and user can log in with submitted credentials, in other situations system requires the user confirm his email (filled in the form) to confirm his identity, this process is called "double opt-in". If you want enable this behaviour you must activate "**double opt-in**" configuration feature.

In some other cases, particularly in protected website intranet, administrators of the website have to confirm your profile before you can access all the website reserved contents, this process is called "moderation". If you want enable this behaviour you must activate "**moderation**" configuration feature.
