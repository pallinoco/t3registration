.. include:: ../Includes.txt


.. _introduction:

============
Introduction
============


Many times I was looking for an extension to create a private access for some pages with user registration form. I didn't find an extension to achieve this task very easily. The aim of this extension is to help you in create a complete frontend registration process. The extension has many level of authorization during registration process from the simpler to the more complicated.

In the following documentation you can find help steps for beginner to advanced user.


.. toctree::
	:maxdepth: 5
	:titlesonly:

	Workflow/Index
