.. include:: ../Includes.txt


.. _developer:

=============
For Developer
=============

Target group: **Developers**

T3registration was born to became a very easy to use extension, but you can extend behaviour this some signals (`learn signals <https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ApiOverview/Hooks/Concept/Index.html>`_) or extending user model. T3registration uses the MVC construct to manage the registration process so if you extend the model with property, set and get the persistence of data will be automatically.

Here you can find the list of signals and an example to extend the user model.

.. _developer-signals:

Signals
=======

How to use signal
^^^^^^^^^^^^^^^^^

In this example you see usage of signal, the first part of code i the class slot that will be called when signal is triggered.

.. code-block:: php
   :linenos:

   <?php
   namespace YourNameSpace\YourExtension\Slot;

   class MySlotClass
   {
      public function doSomeThingFunction($customValue1, $customValue2)
      {
         /* Do something */
      }
   }
The $customValue1 and $customValue2 are the variables passed from the signal.

Here the example of code you can use to attach to the signal

.. code-block:: php
   :linenos:

   $signalSlotDispatcher = TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(Dispatcher::class);
   $signalSlotDispatcher->connect(
    \Name\Space\Signal\SignalClassName::class,
    'DoSomeThing',
    YourNameSpace\YourExtension\Slot\MySlotClass::class,
    'doSomeThingFunction'
   );

User creation
^^^^^^^^^^^^^

During the final operation of persisting user, t3registration launch 2 different signals, one before user persisted and one after user persisted, the parameter is the user saved.

**BeforeUserCreate**, **AfterUserCreate**

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

User updating
^^^^^^^^^^^^^

During the final operation of updating user, t3registration launch 2 different signals, one before user updated is persisted and one after user updated is persisted, the parameter is the user saved.

**BeforeUserUpdate**, **AfterUserUpdate**

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

User deleting
^^^^^^^^^^^^^

During the final operation of deleting user, t3registration launch 2 different signals, one before user deleted is persisted and one after user deleted is persisted, the parameter is the user saved.

**BeforeUserDelete**, **AfterUserDelete**

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

Processes
^^^^^^^^^

**afterChangeEmailConfirmation**

This signal is raised after user clicking in the email link for change username (email) and before user updated will be persisted. So you can change data of the user.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**prepareUserConfirmation**

This signal is raised after user clicking in the double opt-in email link and before user will be persisted. So you can change data of the user.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**prepareUserBeforeEmailModeration**

This signal is raised before the system begins the moderation process. So you can change data of the user.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**afterDeleteCancellationConfirmation**

This signal is raised after the user click on then cancel action in the email, so he has decided to decline the cancellation action. You can update user before token will be empty and user updated.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**afterDeleteConfirmation**

Questo signal è lanciato quando l'utente clicca sul link per confermare la cancellazione, subito dopo il sistema aggiornerà l'utente conle eventuali modifiche che arriveranno dallo slot. In pratica con questo signal è possibile cambiare dei dati dell'utente prima della cancellazione.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**afterDoubleOptinConfirmation**

Questo signal è lanciato quando l'utente clicca sul link per confermare il double opt-in, quindi è possibile aggiornare i dati dell'utente.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**afterModerationConfirmation**

Questo signal è lanciato quando l'utente clicca sul link per confermare la moderazione, quindi è possibile aggiornare i dati dell'utente.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**afterModerationReject**

Questo signal è lanciato quando l'utente clicca sul link per negare l'autorizzazione durante il processo di moderazione, quindi è possibile aggiornare i dati dell'utente.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**setChangedEmailConfirmation**

This signal is raised before the system sending email to user for the email modification process. So you can update user data used both persisting information and email compiling.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

**setChangedEmailWithoutEmailProcessConfirmation**

This signal is raised before the system updating user's email. So you can update user data used for persisting information.

+----------------+-------------------------------------------------------------+---------------------------------+
| Parameter      | Data type                                                   | Description                     |
+================+=============================================================+=================================+
| $user          | Pallino\\T3registration\\Domain\\Model\\FrontendUser        | user                            |
+----------------+-------------------------------------------------------------+---------------------------------+

Use parameter :code:`$user` to modify user information

.. _developer-extension:

Extending user model
====================

Follow this chapter to learn how to add new fields or actions.

It is important to know how this concept is implemented. If a class should be extended, EXT:t3registration will generate
a new file containing the original class of the extension itself and all other classes which should extended it.

.. warning:: The drawbacks are easy to identify:

 	- Don't use any use statements as those are currently ignored!
 	- It is not possible to override an actual method or property!

The files are saved by using the Caching Framework in the directory ``typo3temp/Cache/Code/t3registration``.

.. only:: html

	.. contents::
		:local:
		:depth: 1

1) Add a new field in the backend
---------------------------------
To add new fields, use either the extension **extension_builder** (http://typo3.org/extensions/repository/view/extension_builder) or create the extension from scratch.

The extension key used in this examples is ``t3registration_marketing``.

Create the fields
^^^^^^^^^^^^^^^^^
3 files are basically all what you need:

ext_emconf.php
""""""""""""""
The file  ``ext_emconf.php`` holds all basic information about the extension like the title, description and version number.

.. code-block:: php

	<?php

	$EM_CONF[$_EXTKEY] = array(
		'title' => 't3registration privacy permissions',
		'description' => 'Set privacy permission for t3regsitration',
		'category' => 'plugin',
		'author' => 'Pallino & Co. srl',
		'author_email' => '',
		'state' => 'alpha',
		'uploadfolder' => FALSE,
		'createDirs' => '',
		'clearCacheOnLoad' => TRUE,
		'version' => '1.0.0',
		'constraints' => array(
			'depends' => array(
				'typo3' => '9.5.0-9.5.99',
				't3registration' => '3.0.0-3.0.99',
			),
			'conflicts' => array(),
			'suggests' => array(),
		),
	);

SQL definition
""""""""""""""
Create the file ``ext_tables.sql`` in the root of the extension directory with the following content:

.. code-block:: sql


	# Table structure for table 'fe_users'
	#
	CREATE TABLE fe_users (
         tx_t3registration_marketing tinyint(4) unsigned DEFAULT 0 NOT NULL,
         tx_t3registration_profile tinyint(4) unsigned DEFAULT 0 NOT NULL
	);


TCA definition
""""""""""""""
The TCA defines which tables and fields are available in the backend and how those are rendered (e.g. as input field, textarea, select field, ...).

In this example, the table ``fe_users`` will be extended by a simple input field.
Therefore, create the file ``Configuration/TCA/Overrides/fe_users.php``.

.. code-block:: php

	<?php
	defined('TYPO3_MODE') or die();
	$fields = array(
		'tx_t3registration_marketing' => array(
			'exclude' => 1,
			'label' => 'Marketing permission',
			'config' => [
				'type' => 'check',
				'items' => [
					['marketing', ''],
				],
			]
		),
		'tx_t3registration_profile' => array(
			'exclude' => 1,
			'label' => 'Profiling permission',
			'config' => [
				'type' => 'check',
				'items' => [
					['profile', ''],
				],
			]
		)
	);

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $fields);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'tx_t3registration_marketing');
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'tx_t3registration_profile');


Install the extension
^^^^^^^^^^^^^^^^^^^^^
Now you should be able to install the extension and if you open a website user record, you should see the new field in the last tab.

.. TODO: what if something wrong


2) Register the class
---------------------

Until now, EXT:t3registration won't use the new field because it doesn't know about it. To change that, you need to register your new model.

Create the extending class
^^^^^^^^^^^^^^^^^^^^^^^^^^

As the class ``Domain/Model/FrontendUser`` should be extended, create a file at the same path in the own extension which is
``typo3conf/ext/t3registration_marketing/Classes/Domain/Model/FrontendUser.php``:

.. code-block:: php
   :linenos:

	<?php

	namespace Pallino\T3registrationMarketing\Domain\Model;

	/**
	* FrontendUser
	*/
	class FrontendUser extends \Pallino\T3registration\Domain\Model\FrontendUser {

		/**
		* @var bool
		*/
		protected $marketing;

		/**
		* @var bool
		*/
		protected $profile;

		/**
		* @return bool
		*/
		public function isMarketing(): bool
		{
			return $this->marketing;
		}

		/**
		* @param bool $marketing
		*/
		public function setMarketing(bool $marketing): void
		{
			$this->marketing = $marketing;
		}

		/**
		* @return bool
		*/
		public function isProfile(): bool
		{
			return $this->profile;
		}

		/**
		* @param bool $profile
		*/
		public function setProfile(bool $profile): void
		{
			$this->profile = $profile;
		}
	}

Register yor class
^^^^^^^^^^^^^^^^^^

Create the file ``ext_localconf.php`` in the root of the extension:

.. code-block:: php

	<?php
	defined('TYPO3_MODE') or die();

	$GLOBALS['TYPO3_CONF_VARS']['EXT']['t3registration']['classes']['Domain/Model/FrontendUser'][] = 't3registration_marketing';

**Domain/Model/FrontendUser** have to be the namespace of your extended class and **t3registration_marketing** is the extension key.
